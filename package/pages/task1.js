scopeFunctionTaskJK();

function scopeFunctionTaskJK() {

    let canvas = document.getElementById("taskSim");
    let context = canvas.getContext("2d");

    let stateJK = {
        simName: "Bistabil JK\nBez kašnjenja",
        J: 0,
        K: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        outF: 0,
        msDelay: 1000,
        userInput: [0, 0, 0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [14, 6],
        impl: 1
    };

    let stateT = {
        simName: "Implementacija bistabila JK\nBez kašnjenja",
        T: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        animationOffset: 0,
        animationPattern: [14, 10],
    };

    Object.defineProperties(stateJK, {
        "setQ": {
            set: function (newVal) {
                stateJK.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateJK.nQ = newVal;
            }
        },
    });

    stateJK.QnNext = (stateJK.Q === 1) ? 0 : 1;
    stateJK.currentRow = 1;

    let zoomHotsJK = {
        hots: [],
        min: 1.9,
        max: 2.3
    };

    let scaleFactor = 1.9;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        stateJK.CP = (stateJK.CP === 1) ? 0 : 1;
        if (stateJK.CP === 1) {
            stateJK.CPEdge = 1;
        } else {
            stateJK.CPEdge = 0;
        }
        if (stateJK.CPEdge === 1) {
            if (stateJK.userInput[0] === 1 &&
                stateJK.userInput[1] === 1) {
                // Comb(1, 1)
                stateJK.Q = 1 - stateJK.Q;
                stateJK.nQ = (stateJK.Q === 1) ? 0 : 1;
            } else if (stateJK.userInput[0] === 1) {
                // Comb(1, 0)
                stateJK.Q = 1;
                stateJK.nQ = 0;
            } else if (stateJK.userInput[1] === 1) {
                // Comb(0, 1)
                stateJK.nQ = 1;
                stateJK.Q = 0;
            } else {
                // Comb(0, 0)
            }
        }
        stateJK.CPEdge = 0;
    }


    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateJK.hots.length; i < l; i++) {
            let h = stateJK.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                let u = stateJK.userInput[h.row];
                stateJK.userInput[h.row] = (u === 0) ? 1 : 0;
                if (h.row === 3) {
                    // Right Click, see context event function
                    break;
                } else if (h.row === 2) {
                    // CP changed, make appropriate changes
                    animateRaisingCP();
                }
                reInit(canvas, context);
                break;
            }
        }
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
    }

    function reInit(canvas, context) {
        if (stateJK.impl === 0) {
            drawJK(canvas, context, stateJK, constJK);
        } else {
            drawInnerJK(canvas, context, stateJK, constJK);
        }
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);
    }

    function startSimulation() {
        stateJK.animationOffset++;
        stateT.animationOffset++;
        if (stateJK.animationOffset > 2000) {
            stateJK.animationOffset = 0;

        }
        if (stateT.animationOffset > 2000) {
            stateT.animationOffset = 0;
        }
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }


    function propRightClick(e, state, h) {
        if (h.row === 3) {
            state.impl = (state.impl === 1) ? 0 : 1;
            setScaleFactor(scaleFactor);
            e.preventDefault();
        }
    }

    function rightClickEvent(canvas, state) {
        return function (e) {
            clickHotEvent2(e, canvas, state, propRightClick);
        }
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas, state) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);
        canvas.addEventListener('contextmenu',
            rightClickEvent(canvas, state), false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);
    }


    function drawJK(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateJK.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[2] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let J = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let K = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [J, K, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.emerald, clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["J", "K", "CP"],
            outText: ["Q", "Q'"],
            flipFlopType: 3,
            addClickBox: [true, true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: state
        };

        // Save clickHots
        saveClickHots(stateJK, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 3);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                reInit(canvas, context);
            }
        };
    }

    function drawMissingLogic(context, innerBox, box, constants) {

        let NQ = (box.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let J = (box.state.userInput[0] === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let K = (box.state.userInput[1] === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let Q = (box.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (box.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        let firstANDResult = (box.state.nQ === 1 && box.state.userInput[0] === 1)
            ? 1 : 0;
        let secondANDResult = (box.state.Q === 1 && box.state.userInput[1] === 1) ?
            1 : 0;
        let out1 = (firstANDResult === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let out2 = (secondANDResult === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        let yLinesOff = getFlipFlopYLinesOff(box.boxHeight, 3);
        let yLinesOffTwo = getFlipFlopYLinesOff(box.boxHeight, 2);
        let lineDistance = box.boxHeight / 3;

        let boxWidth = constants.scaleFactor * 30;
        let boxHeight = constants.scaleFactor * 30;
        let inputLinesYOff = 8 * constants.scaleFactor;
        let yOff = box.startY + yLinesOff - boxHeight + inputLinesYOff;

        let mainANDOff = 25 * constants.scaleFactor;
        let AND1 = {
            startX: box.startX + mainANDOff,
            startY: yOff,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            color: constants.frameBlacked,
            fillStyle: constants.colors.darkRed,
            textFont: constants.IECTextFont,
            complementList: [0, 0, 0],
            lineColorsIO: [NQ, J, out1],
        };

        let AND2 = {
            startX: AND1.startX,
            startY: box.startY + yLinesOff + lineDistance - inputLinesYOff,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            color: constants.frameBlacked,
            fillStyle: constants.colors.darkRed,
            textFont: constants.IECTextFont,
            complementList: [0, 0, 0],
            lineColorsIO: [NQ, J, out1]
        };

        let xOffOR = 40 * constants.scaleFactor;
        let OR1 = {
            startX: AND1.startX + AND1.boxWidth + xOffOR,
            startY: AND1.startY + AND1.boxHeight +
            ((AND2.startY - (AND1.startY + AND1.boxHeight)) / 2) - boxHeight / 2,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            color: constants.frameBlacked,
            fillStyle: constants.colors.dodgerBlue,
            textFont: constants.IECTextFont,
            complementList: [0, 0, 0],
            lineColorsIO: [NQ, J, out1]
        };

        let gates = new LogicGates();
        drawRectangularBox(context, AND1, gates.ANDSIGN, constants);
        drawRectangularBox(context, AND2, gates.ANDSIGN, constants);
        drawRectangularBox(context, OR1, gates.ORSIGN, constants);


        let lineInJ = getLineParams(
            box.startX,
            box.startY + yLinesOff,
            AND1.startX,
            box.startY + yLinesOff,
            constants.passiveStateColor
        );

        let lineInK = getLineParams(
            box.startX,
            box.startY + yLinesOff + lineDistance,
            AND2.startX,
            box.startY + yLinesOff + lineDistance,
            constants.passiveStateColor
        );
        drawComplexLine(context, lineInJ, J, box.state, constants, false, false);
        drawComplexLine(context, lineInK, K, box.state, constants, false, false);

        // Draw connect lines to OR
        let outLineLength = constants.inputLineLengthNew;
        let inOR1 = getLineParams(
            AND1.startX + AND1.boxWidth,
            AND1.startY + AND1.boxHeight / 2,
            AND1.startX + AND1.boxWidth + outLineLength,
            AND1.startY + AND1.boxHeight / 2,
            constants.passiveStateColor
        );
        let inORDown = getLineParams(
            inOR1.endX,
            inOR1.endY,
            inOR1.endX,
            OR1.startY + inputLinesYOff,
            constants.passiveStateColor
        );
        let inORRight = getLineParams(
            inORDown.endX,
            inORDown.endY,
            OR1.startX,
            inORDown.endY,
            constants.passiveStateColor
        );

        drawComplexLine(context, inOR1, out1, box.state, constants, false, false);
        drawComplexLine(context, inORDown, out1, box.state, constants, false, false);
        drawComplexLine(context, inORRight, out1, box.state, constants,
            false, false);

        let inOR2 = getLineParams(
            AND2.startX + AND2.boxWidth,
            AND2.startY + AND2.boxHeight / 2,
            AND2.startX + AND2.boxWidth + outLineLength,
            AND2.startY + AND2.boxHeight / 2,
            constants.passiveStateColor
        );
        let inORUp = getLineParams(
            inOR2.endX,
            inOR2.endY,
            inOR2.endX,
            OR1.startY + OR1.boxHeight - inputLinesYOff,
            constants.passiveStateColor
        );
        let inOR2Right = getLineParams(
            inORUp.endX,
            inORUp.endY,
            OR1.startX,
            inORUp.endY,
            constants.passiveStateColor
        );

        drawComplexLine(context, inOR2, out2, box.state, constants, false, false);
        drawComplexLine(context, inORUp, out2, box.state, constants, false, false);
        drawComplexLine(context, inOR2Right, out2, box.state, constants,
            false, false);

        let yLinesOffInner = getFlipFlopYLinesOff(innerBox.boxHeight, 2);
        let outOR = (firstANDResult === 1 || secondANDResult === 1) ? 1 : 0;
        let out3 = (outOR === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        // Change T state;
        box.state.outF = outOR;

        let OROut = getLineParams(
            OR1.startX + OR1.boxWidth,
            OR1.startY + OR1.boxHeight / 2,
            OR1.startX + OR1.boxWidth + outLineLength,
            OR1.startY + OR1.boxHeight / 2,
            constants.passiveStateColor
        );

        let outORDown = getLineParams(
            OROut.endX,
            OROut.endY,
            OROut.endX,
            innerBox.startY + yLinesOffInner,
            constants.passiveStateColor
        );

        let outORRight = getLineParams(
            outORDown.endX,
            outORDown.endY,
            innerBox.startX,
            outORDown.endY,
            constants.passiveStateColor
        );
        drawComplexLine(context, OROut, out3, box.state, constants, false, false);
        drawComplexLine(context, outORDown, out3, box.state, constants,
            false, false);
        drawComplexLine(context, outORRight, out3, box.state, constants,
            false, false);


        let outLineOff = 20 * constants.scaleFactor;
        let outNQLine = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + innerBox.boxHeight - yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew
            + outLineOff,
            innerBox.startY + innerBox.boxHeight - yLinesOffInner,
            constants.passiveStateColor
        );

        let andYOff = 10 * constants.scaleFactor;
        let extendLineLength = 6 * constants.scaleFactor;
        let outNQUp = getLineParams(
            outNQLine.startX + extendLineLength,
            outNQLine.startY,
            outNQLine.startX + extendLineLength,
            AND1.startY - andYOff,
            constants.passiveStateColor
        );

        let outNQLeft = getLineParams(
            outNQUp.endX,
            outNQUp.endY,
            AND1.startX - outLineLength / 2,
            outNQUp.endY,
            constants.passiveStateColor
        );

        let outNQDown = getLineParams(
            outNQLeft.endX,
            outNQLeft.endY,
            outNQLeft.endX,
            AND1.startY + inputLinesYOff,
            constants.passiveStateColor
        );

        let outNQRight = getLineParams(
            outNQDown.endX,
            outNQDown.endY,
            AND1.startX,
            outNQDown.endY,
            constants.passiveStateColor
        );

        let outQLine = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew
            + outLineOff,
            innerBox.startY + yLinesOffInner,
            constants.passiveStateColor
        );

        let outQDown = getLineParams(
            outQLine.startX,
            outQLine.startY,
            outQLine.startX,
            innerBox.startY + innerBox.boxHeight + andYOff,
            constants.passiveStateColor
        );

        let outQLeft = getLineParams(
            outQDown.endX,
            outQDown.endY,
            AND2.startX - outLineLength / 2,
            outQDown.endY,
            constants.passiveStateColor
        );

        let outQUp = getLineParams(
            outQLeft.endX,
            outQLeft.endY,
            outQLeft.endX,
            AND2.startY + AND2.boxHeight - inputLinesYOff,
            constants.passiveStateColor
        );

        let outQRight = getLineParams(
            outQUp.endX,
            outQUp.endY,
            AND2.startX,
            outQUp.endY,
            constants.passiveStateColor
        );

        let lineQOut = getLineParams(
            outQLine.endX,
            outQLine.endY,
            outQLine.endX,
            box.startY + yLinesOffTwo,
            constants.passiveStateColor
        );
        let lineNQOut = getLineParams(
            outNQLine.endX,
            outNQLine.endY,
            outNQLine.endX,
            box.startY + box.boxHeight - yLinesOffTwo,
            constants.passiveStateColor
        );

        let lineNQRight = getLineParams(
            lineNQOut.endX,
            lineNQOut.endY,
            box.startX + box.boxWidth,
            box.startY + box.boxHeight - yLinesOffTwo,
            constants.passiveStateColor
        );

        let lineQRight = getLineParams(
            lineQOut.endX,
            lineQOut.endY,
            box.startX + box.boxWidth,
            box.startY + yLinesOffTwo,
            constants.passiveStateColor
        );

        drawComplexLine(context, outNQRight, nQ, box.state, constants, false, false);
        drawComplexLine(context, outNQDown, nQ, box.state, constants, false, false);
        drawComplexLine(context, outNQLeft, nQ, box.state, constants, false, false);
        drawComplexLine(context, outNQUp, nQ, box.state, constants, false, false);
        drawComplexLine(context, outNQLine, nQ, box.state, constants, false, false);
        drawComplexLine(context, outQLine, Q, box.state, constants, false, false);
        drawComplexLine(context, outQDown, Q, box.state, constants, false, false);
        drawComplexLine(context, outQLeft, Q, box.state, constants, false, false);
        drawComplexLine(context, outQUp, Q, box.state, constants, false, false);
        drawComplexLine(context, outQRight, Q, box.state, constants, false, false);
        drawComplexLine(context, lineQOut, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQOut, nQ, box.state, constants, false, false);
        drawComplexLine(context, lineQRight, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQRight, nQ, box.state, constants,
            false, false);


        // Draw CP Connections
        let CP = (box.state.userInput[2] === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let CPIn = getLineParams(
            box.startX,
            box.startY + box.boxHeight - yLinesOff,
            outORDown.startX,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let CPInUp = getLineParams(
            CPIn.endX,
            CPIn.endY,
            CPIn.endX,
            innerBox.startY + innerBox.boxHeight - yLinesOffInner,
            constants.passiveStateColor
        );

        let CPInRight = getLineParams(
            CPInUp.endX,
            CPInUp.endY,
            innerBox.startX,
            CPInUp.endY,
            constants.passiveStateColor
        );

        drawComplexLine(context, CPIn, CP, box.state, constants, false, false);
        drawComplexLine(context, CPInUp, CP, box.state, constants, false, false);
        drawComplexLine(context, CPInRight, CP, box.state, constants, false, false);


        let connectDotQ =
            {
                centerX: outQDown.startX,
                centerY: outQDown.startY,
                radius: constants.IEEEConnectDotRadius,
                startAngle: toRadians(constants.zeroAngle),
                endAngle: toRadians(constants.fullCircle),
                rotation: false,
                color: constants.passiveStateColor,
                fillStyle: constants.passiveStateColor,
                lineWidth: constants.connectDotLineWidth
            };

        let connectDotNQ =
            {
                centerX: outNQUp.startX,
                centerY: outNQUp.startY,
                radius: constants.IEEEConnectDotRadius,
                startAngle: toRadians(constants.zeroAngle),
                endAngle: toRadians(constants.fullCircle),
                rotation: false,
                color: constants.passiveStateColor,
                fillStyle: constants.passiveStateColor,
                lineWidth: constants.connectDotLineWidth
            };
        drawCircle(context, connectDotQ);
        drawCircle(context, connectDotNQ);
    }

    function drawInnerJK(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateT.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);

        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let scaleFactorJK = constants.scaleFactor - 0.5;
        let constJK = new Constants(scaleFactorJK);
        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let J = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let K = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[2] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [J, K, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: "#CB3A34", clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 3 * 85 * constants.scaleFactor;
        let boxHeight = 180 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = -10 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 + xOff,
            startY: (canvas.height - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1, 2],
            textIn: ["J", "K", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [0, 0, 0],
            textPosOut: [0, 0],
            flipFlopType: 1,
            addClickBox: [true, true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: constants.colors.white,
            state: state
        };

        // Save clickHots
        saveClickHots(stateJK, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 3);
        // Draw flip flop. Draw IO lines

        passiveStyle(context);
        context.lineWidth = 1 * constants.scaleFactor;
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length,
            colorsOut.length, constants);
        drawInnerT(context, genericBox, stateT, constJK);

        // drawCloud(context, genericBox, constJK);
        passiveStyle(context);
    }

    function getFlipFlopYLinesOff(height, numberOfIn) {
        let h = height;
        let lineDistanceIn = (h / numberOfIn);
        return ((h - (numberOfIn - 1) * lineDistanceIn) / 2);
    }

    function drawInnerT(context, outerBox, stateT, constants) {
        // Clear rect
        context.save();

        // init current values, passive and active states.
        let T = (outerBox.state.outF === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let q = (outerBox.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (outerBox.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let CP = (outerBox.state.userInput[2] === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        // Set inner CP
        stateT.CP = stateJK.CP;

        let colorsIn = [T, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.flipT};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 70 * constants.scaleFactor;
        let boxHeight = 90 * constants.scaleFactor;
        let xOff = 50 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: outerBox.startX + (outerBox.boxWidth - boxWidth) / 2 + xOff,
            startY: outerBox.startY + (outerBox.boxHeight - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["T", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 3,
            addClickBox: [false, false],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: constants.colors.white,
            fillStyle: colorsBox.box,
            state: stateT
        };

        // Save clickHots
        saveClickHots(stateJK, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        drawMissingLogic(context, genericBox, outerBox, constants);
        passiveStyle(context);
    }

    function drawComplexLine(context, line, cond, state,
                             constants, clearCanvas = false, direction = true) {
        passiveStyle(context);
        drawConnectLine(context, line,
            constants.tubeLineWidth, clearCanvas, direction);
        if (cond === constants.greenLineActiveColor) {
            activeStyle(context, state);
            line.color = constants.greenLineActiveColor;
            drawConnectLine(context, line,
                constants.currentLineWidth, clearCanvas, direction);
        }
        passiveStyle(context);
    }

    function getLineParams(x1, y1, x2, y2, c) {
        return {
            startX: x1,
            startY: y1,
            endX: x2,
            endY: y2,
            color: c
        };
    }

    function init(canvas, context, state, constants) {
        if (stateJK.impl === 0) {
            drawJK(canvas, context, constants, constants);
        } else {
            drawInnerJK(canvas, context, state, constants);

        }
    }

    init(canvas, context, stateJK, constJK);
    eventListeners(canvas, stateJK);
    startSimulation();
}
