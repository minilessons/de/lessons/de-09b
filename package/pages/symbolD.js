/**
 * Created by Komediruzecki on 13.12.2016..
 */


/**
 * Created by Komediruzecki on 7.12.2016..
 */

scopeFunctionSymbolD();

function scopeFunctionSymbolD() {

    let canvas = document.getElementById("symbolD");
    let context = canvas.getContext("2d");

    let stateD = {
        simName: "Bistabil D\nKašnjenje bistabila: 1000 ms",
        D: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        msDelay: 1000,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let u = new usedUnicode();

    stateD.QnNext = (stateD.Q === 1) ? 0 : 1;
    stateD.currentRow = 1;

    stateD.elems = [
        ["D", "Q" + u.subN + u.subPlus + u.sub1],
        [0, 0],
        [1, 1]
    ];

    let zoomHotsJK = {
        hots: [],
        min: 1.5,
        max: 2
    };

    Object.defineProperties(stateD, {
        "setQ": {
            set: function (newVal) {
                stateD.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateD.nQ = newVal;
            }
        },
    });

    let scaleFactor = 1.75;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.D];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = state.D;
        } else if (state.currentRow === 2) {
            state.QnNext = state.D
        } else {
            state.QnNext = state.elems[state.currentRow][1];
        }
    }

    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateD.hots.length; i < l; i++) {
            let h = stateD.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                let u = stateD.userInput[h.row];
                stateD.userInput[h.row] = (u === 0) ? 1 : 0;
                stateD.D = (h.row === 0) ? (stateD.D === 1) ? 0 : 1 : stateD.D;
                if (h.row === 1) {
                    stateD.CP = 1 - stateD.CP;
                    // CP changed, call the appropriate function
                    animateRaisingCP();

                } else {
                    setCurrentState(stateD);
                    reInit(canvas, context);
                }
                break;
            }
        }
    }

    function reInit(canvas, context) {
        drawD(canvas, context, stateD, constJK);
        drawDiagram(context, canvas, stateD, constJK);
        drawTable(context, stateD, constJK);
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);
    }

    function startSimulation() {
        stateD.animationOffset++;
        if (stateD.animationOffset > 2000) {
            stateD.animationOffset = 0;
        }
        setCurrentState(stateD);
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);
    }


    function drawD(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let D = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [D, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.dodgerBlue, clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 75 * constants.scaleFactor;
        let yOff = 85 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["D", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 3,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: state
        };

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);
    }


    function drawDiagram(context, canvas, state, constants) {
        let yOff = 10;
        let y = canvas.height - (canvas.height - 200) / 2 + yOff;
        let distanceFromCircles = 150 * constants.scaleFactor;
        let sketchLength = distanceFromCircles + constants.IEEEComplementDiameter;
        let x = (canvas.width - sketchLength) / 2;
        let circleRadius = constants.IEEEArcRadius;

        let transitions = {
            lineSet: "1",
            lineReset: "0",
            lineStay0: "0",
            lineStay1: "1"
        };
        drawDiagramGeneric002(context, state,
            x, y, distanceFromCircles, circleRadius, transitions, constants)
    }

    function drawTable(context, state, constants) {
        let x = 250 * 2;
        let y = 15 * 2;
        let rowHeight = 23 * 2;
        let columnWidth = 28 * 2;
        let fontSize = rowHeight / 2;
        drawTableGeneric2(context, x, y, rowHeight, columnWidth, fontSize, state, constants)
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                reInit(canvas, context);
            }
        };
    }


    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        if (stateD.CP === 1) {
            stateD.CPEdge = 1;
        } else {
            stateD.CPEdge = 0;
        }
        if (stateD.CPEdge === 1) {
            let newQ = stateD.D;
            let newNQ = (newQ === 1) ? 0 : 1;
            setTimeout(simulationCallback(stateD, "Q", newQ), stateD.msDelay);
            setTimeout(simulationCallback(stateD, "NQ", newNQ), stateD.msDelay);
        }
        stateD.CPEdge = 0;
    }

    function init() {
        drawD(canvas, context, stateD, constJK);
        drawDiagram(context, canvas, stateD, constJK);
        drawTable(context, stateD, constJK);


    }

    init();
    eventListeners(canvas);
    startSimulation();
}
