/**
 * Created by Komediruzecki on 13.12.2016..
 */


/**
 * Created by Komediruzecki on 7.12.2016..
 */

scopeFunctionSymbolJK();

function scopeFunctionSymbolJK() {

    let canvas = document.getElementById("symbolJK");
    let context = canvas.getContext("2d");

    let stateJK = {
        simName: "Bistabil JK s tablicom promjene stanja i dijagramom\n" +
        "Kašnjenje bistabila: 2000 ms",
        J: 1,
        K: 1,
        CP: 0,
        Q: 1,
        nQ: 0,
        msDelay: 2000,
        userInput: [1, 0, 1],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let u = new usedUnicode();

    stateJK.QnNext = (stateJK.Q === 1) ? 0 : 1;
    stateJK.currentRow = 4;

    stateJK.elems = [
        ["J", "K", "Q" + u.subN + u.subPlus + u.sub1],
        [0, 0, "Q" + u.subN],
        [0, 1, 0],
        [1, 0, 1],
        [1, 1, "Q" + u.subN + "'"]
    ];

    let zoomHotsJK = {
        hots: [],
        min: 1.5,
        max: 2
    };

    let scaleFactor = 1.75;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    setStateProperties(stateJK);
    function setStateProperties(s0) {
        Object.defineProperties(s0, {
            "getQ": {
                get: function () {
                    return s0.Q
                }
            },
            "setQ": {
                set: function (newVal) {
                    s0.Q = newVal;
                }
            },
            "getNQ": {
                get: function () {
                    return s0.nQ
                }
            },
            "setNQ": {
                set: function (newVal) {
                    s0.nQ = newVal;
                }
            }
        });
    }

    function setNextState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.J, state.K];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = state.Q;
        } else if (state.currentRow === 4) {
            state.QnNext = (state.Q === 1) ? 0 : 1;
        } else {
            state.QnNext = state.elems[state.currentRow][2];
        }
    }

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.J, state.K];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = state.Q;
        } else if (state.currentRow === 4) {
            state.QnNext = (state.Q === 1) ? 0 : 1;
        } else {
            state.QnNext = state.elems[state.currentRow][2];
        }
    }

    let intervalId = 0;

    function startOscilation() {
        // set current
        // Change state
        stateJK.Q = (stateJK.Q === 1) ? 0 : 1;
        stateJK.nQ = (stateJK.nQ === 1) ? 0 : 1;
        setCurrentState(stateJK);
        reInit(canvas, context);
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            state[set + item] = value;
            reInit(canvas, context);
        }
    }

    function calcState(state) {
        let newVals = {Q: state.Q, nQ: state.nQ};
        if (state.userInput[1] === 1) {
            if (state.userInput[0] === 1) {
                // Comb(1, 0)
                newVals.Q = 1;
                newVals.nQ = 0;
            } else if (state.userInput[2] === 1) {
                // Comb(0, 1)
                newVals.nQ = 1;
                newVals.Q = 0;
            } else {
                // Comb(0, 0)
            }
        }
        return newVals;
    }

    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateJK.hots.length; i < l; i++) {
            let h = stateJK.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                if (intervalId != 0) {
                    clearInterval(intervalId);
                }

                let u = stateJK.userInput[h.row];
                stateJK.userInput[h.row] = (u === 0) ? 1 : 0;
                stateJK.J = (h.row === 0) ? (stateJK.J === 1) ? 0 : 1 : stateJK.J;
                stateJK.K = (h.row === 2) ? (stateJK.K === 1) ? 0 : 1 : stateJK.K;
                setCurrentState(stateJK);
                reInit(canvas, context);

                if (stateJK.userInput[1] === 1 &&
                    stateJK.userInput[2] === 1 && stateJK.userInput[0] === 1) {
                    // Comb(1, 1)
                    intervalId = setInterval(startOscilation, stateJK.msDelay);
                    break;
                }

                let newVals = calcState(stateJK);
                setTimeout(simulationCallback(stateJK, "Q", newVals.Q),
                    stateJK.msDelay);
                setTimeout(simulationCallback(stateJK, "NQ", newVals.nQ),
                    stateJK.msDelay);

                break;
            }
        }
    }

    function reInit(canvas, context) {
        drawJK(canvas, context, stateJK, constJK);
        drawDiagram(context, canvas, stateJK, constJK);
        drawTable(context, stateJK, constJK);
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);

    }

    function startSimulation() {
        stateJK.animationOffset++;
        if (stateJK.animationOffset > 2000) {
            stateJK.animationOffset = 0;
        }
        //setCurrentState(stateJK);
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);
    }


    function drawJK(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let J = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let K = (state.userInput[2] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [J, CP, K];
        let colorsOut = [q, nQ];
        let colorsBox = {box: "#CB3A34", clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 75 * constants.scaleFactor;
        let yOff = 85 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1, 2],
            textIn: ["J", "CP", "K"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 1,
            addClickBox: [true, true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: constants.colors.emerald,
            state: state

        };

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);


        if (state.CP != undefined) {
            // This goes only if cant do with draw lines...
        }
    }

    function drawDiagramJK(context, state, x, y, cDistance, cRadius, transitions, constants) {
        let stayInZero;
        let stayInOne;
        let set, reset;

        if (state.Q === 0 && state.QnNext === 0) {
            stayInZero = constants.colors.diagramArrow;
        } else {
            stayInZero = constants.colors.black;
        }
        if (state.Q === 0 && state.QnNext === 1) {
            set = constants.colors.diagramArrow;
        } else {
            set = constants.colors.black;
        }
        if (state.Q === 1 && state.QnNext === 0) {
            reset = constants.colors.diagramArrow;
        } else {
            reset = constants.colors.black;
        }
        if (state.Q === 1 && state.QnNext === 1) {
            stayInOne = constants.colors.diagramArrow;
        } else {
            stayInOne = constants.colors.black;
        }

        let stateZero = (state.Q === 1)
            ? constants.passiveStateColor : constants.activeStateColor;
        let stateOne = (state.Q === 0)
            ? constants.passiveStateColor : constants.activeStateColor;


        let xOff = x;
        let yOff = y;
        let state0 = {
            centerX: xOff,
            centerY: yOff,
            radius: cRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            rotation: false,
            color: stateZero,
            fillStyle: "white",
            lineWidth: constants.connectLinesWidth + 2
        };

        let circleDistance = cDistance;
        let state1 = {
            centerX: xOff + circleDistance,
            centerY: yOff,
            radius: cRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            rotation: false,
            color: stateOne,
            fillStyle: "white",
            lineWidth: constants.connectLinesWidth + 2,
        };
        drawCircle(context, state0);
        drawCircle(context, state1);

        // Draw Text for circle states
        let textS0 = {
            leftVertexX: (state0.centerX) - (constants.textBoxWidth / 2),
            leftVertexY: (state0.centerY) - (constants.textBoxHeight / 2),
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: (constants.varTextSize + 5) + "px Courier",
            color: constants.standardLineColor,
            lineWidth: constants.textLineWidth
        };
        let textS1 = {
            leftVertexX: (state1.centerX) - (constants.textBoxWidth / 2),
            leftVertexY: (state1.centerY) - (constants.textBoxHeight / 2),
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: (constants.varTextSize + 5) + "px Courier",
            color: constants.standardLineColor,
            lineWidth: constants.textLineWidth
        };

        drawText(context, textS0, "0");
        drawText(context, textS1, "1");

        // Draw connect lines
        let yLineOff = cRadius - Math.PI - 2;
        let distX = Math.sqrt(Math.pow(cRadius, 2) -
            Math.pow(yLineOff, 2));

        let lineSet = {
            startX: xOff + distX,
            startY: yOff - yLineOff,
            controlPointX: xOff + (circleDistance - 2 * distX) / 2,
            controlPointY: yOff - cRadius * 2,
            endPointX: xOff - distX + circleDistance - 5,
            endPointY: yOff - yLineOff,
            color: set,
            lineWidth: constants.connectLinesWidth
        };

        let lineReset = {
            startX: xOff + distX,
            startY: yOff + yLineOff,
            controlPointX: xOff + (circleDistance - 2 * distX) / 2,
            controlPointY: yOff + cRadius * 2,
            endPointX: xOff - distX + circleDistance,
            endPointY: yOff + yLineOff,
            color: reset,
            lineWidth: constants.connectLinesWidth
        };

        let yCenterOff = 45;
        let lineStayZero = {
            startX: xOff - distX,
            startY: yOff - yLineOff,
            controlPointX: xOff - 3 * cRadius,
            controlPointY: yOff - yCenterOff,
            endPointX: xOff - cRadius,
            endPointY: yOff,
            color: stayInZero,
            lineWidth: constants.connectLinesWidth
        };


        let lineStayOne = {
            startX: xOff + circleDistance + distX,
            startY: yOff - yLineOff,
            controlPointX: xOff + circleDistance + 3 * cRadius,
            controlPointY: yOff - yCenterOff,
            endPointX: xOff + circleDistance + cRadius,
            endPointY: yOff,
            color: stayInOne,
            lineWidth: constants.connectLinesWidth
        };

        let rows = transitions.lineSet.split("\n").length;
        let textYOff = rows * 24;
        let textForLineSet = {
            leftVertexX: lineSet.controlPointX,
            leftVertexY: lineSet.controlPointY - textYOff,
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: constants.varTextSize + "px Courier",
            color: "black",
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.textLineWidth

        };

        let textYDown = 5;
        let textForLineReset = {
            leftVertexX: lineReset.controlPointX,
            leftVertexY: lineReset.controlPointY - textYDown,
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: constants.varTextSize + "px Courier",
            color: "black",
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.textLineWidth

        };

        let smallInc = 17 * constants.scaleFactor;
        let twoLineOff = 20 * constants.scaleFactor;
        let textForLineStay0 = {
            leftVertexX: lineStayZero.controlPointX + smallInc,
            leftVertexY: lineStayZero.controlPointY - smallInc - twoLineOff,
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: constants.varTextSize + "px Courier",
            color: "black",
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.textLineWidth

        };

        /** Constant is used for leftVertexX because of zooming makes change to it and it must be fixed (y is only moving axis). */
        let textForLineStay1 = {
            leftVertexX: state1.centerX + state1.radius,
            leftVertexY: lineStayOne.controlPointY - smallInc - twoLineOff,
            boxWidth: constants.textBoxWidth,
            boxHeight: constants.textBoxHeight,
            textFont: constants.varTextSize + "px Courier",
            color: "black",
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.textLineWidth

        };

        let arrowWidth = 15;
        let lineHeight = 30;
        drawCurvedConnectLine(context, lineSet);
        drawCurvedConnectLine(context, lineReset);
        drawCurvedConnectLine(context, lineStayZero);
        drawCurvedConnectLine(context, lineStayOne);
        drawArrow(context, lineSet, arrowWidth, 1);
        drawArrow(context, lineReset, arrowWidth, 0);
        drawArrow(context, lineStayZero, arrowWidth, 1);
        drawArrow(context, lineStayOne, arrowWidth, 1);

        // Draw text on lines
        writeMultilineText2(context, textForLineSet,
            lineHeight, transitions.lineSet);
        writeMultilineText2(context, textForLineReset,
            lineHeight, transitions.lineReset);
        writeMultilineText2(context, textForLineStay0,
            lineHeight, transitions.lineStay0);
        writeMultilineText2(context, textForLineStay1,
            lineHeight, transitions.lineStay1);
    }

    function drawDiagram(context, canvas, state, constants) {
        let yOff = -10;
        let y = canvas.height - (canvas.height - 200) / 2 + yOff;
        let distanceFromCircles = 125 * constants.scaleFactor;
        let sketchLength = distanceFromCircles + constants.IEEEComplementDiameter;
        let x = (canvas.width - sketchLength) / 2;
        let circleRadius = constants.IEEEArcRadius;


        let transitions = {
            lineSet: "(1, 0)\n(1, 1)",
            lineReset: "(0, 1)\n(1, 1)",
            lineStay0: "(0, 0)\n(0, 1)",
            lineStay1: "(0, 0)\n(1, 0)"
        };
        drawDiagramJK(context, state,
            x, y, distanceFromCircles, circleRadius, transitions, constants)
    }


    function drawTable(context, state, constants) {
        let x = 220 * 2;
        let y = 15 * 2;
        let rowHeight = 23 * 2;
        let columnWidth = 28 * 2;
        let fontSize = rowHeight / 2;
        drawTableGeneric2(context, x, y, rowHeight, columnWidth, fontSize, state, constants)
    }

    function init() {
        drawJK(canvas, context, stateJK, constJK);
        drawDiagram(context, canvas, stateJK, constJK);
        drawTable(context, stateJK, constJK);


    }

    init();
    eventListeners(canvas);
    startSimulation();
}