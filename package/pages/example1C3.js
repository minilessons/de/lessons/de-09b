/**
 * Created by Komediruzecki on 7.12.2016..
 */

scopeFunctionJK3();

function scopeFunctionJK3() {

    let canvas1 = document.getElementById("jkFlipFlop3");
    let context1 = canvas1.getContext("2d");
    let scaleFactor = 1.7;
    let constJK = new Constants(scaleFactor);

    let simParams = {canvas: canvas1, context: context1, constants: constJK};
    let stateJK1 = createState({}, simulationCallback, simParams);
    let zoomHotsJK = {
        hots: [],
        min: 1.5,
        max: 2
    };


    saveClickHots(zoomHotsJK, 0, 0, canvas1.width, canvas1.height);

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                updateOther(canvas1, context1, true);
            }
        };
    }

    function clickEventHandler(canvas, context, state, prop) {
        return function (e) {
            clickHotEvent(e, canvas, state, prop);
            update(canvas, context, false, state, constJK);
        }
    }

    function update(canvas, context, clear = false, state, constants) {
        drawJK(canvas, context, clear, state, constants);
    }

    function updateOther(canvas, context, clear = false) {
        drawJK(canvas, context, clear, stateJK1, constJK);
    }

    function processZoom(newScaleFactor, canvas, context) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        updateOther(canvas, context, true);
    }

    function startSimulationCallback(canvas, context, state) {
        return function () {
            startSimulation(canvas, context, state);
        }
    }

    function startSimulation(canvas, context, state) {
        state.animationOffset++;
        if (state.animationOffset > 2000) {
            state.animationOffset = 0;
        }
        updateOther(canvas, context);
        setTimeout(startSimulationCallback(canvas, context, state), 40);
    }

    function wheelCollector(canvas, context) {
        return function (e) {
            MouseWheelEvent1(e, canvas, context);
        }
    }

    function MouseWheelEvent1(e, canvas, context) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas, context, state, prop) {
        canvas.addEventListener('mousedown',
            clickEventHandler(canvas, context, state, prop), false);
        canvas.addEventListener('mousewheel', wheelCollector(canvas, context), false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', wheelCollector(canvas, context), false);
    }

    function init(canvas, context, state, prop, constants) {
        let needClear = true;
        drawJK(canvas, context, needClear, stateJK1, constants);
        eventListeners(canvas, context, state, prop);
    }

    init(canvas1, context1, stateJK1, propClickHotEvent, constJK);
    startSimulation(canvas1, context1, stateJK1);
}
