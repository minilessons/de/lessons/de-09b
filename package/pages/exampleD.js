scopeFunctionD();

function scopeFunctionD() {

    let canvas = document.getElementById("flipFlopD");
    let context = canvas.getContext("2d");

    let stateD = {
        simName: "Bistabil D\nKašnjenje bistabila: 1000 ms",
        D: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        msDelay: 1000,
        userInput: [0, 0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [14, 6],
        impl: 0
    };

    let stateSR = {
        simName: "Implementacija bistabila D\nKašnjenje bistabila: 1000 ms",
        S: 0,
        R: 1,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        animationOffset: 0,
        animationPattern: [14, 10],
    };

    Object.defineProperties(stateD, {
        "setQ": {
            set: function (newVal) {
                stateD.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateD.nQ = newVal;
            }
        },
    });


    let u = new usedUnicode();

    stateD.QnNext = (stateD.Q === 1) ? 0 : 1;
    stateD.currentRow = 1;

    stateD.elems = [
        ["D", "Q" + u.subN + u.subPlus + u.sub1],
        [0, 0],
        [1, 1]
    ];

    let zoomHotsJK = {
        hots: [],
        min: 1.9,
        max: 2.5
    };

    let scaleFactor = 1.9;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.D];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = state.D;
        } else if (state.currentRow === 2) {
            state.QnNext = state.D
        } else {
            state.QnNext = state.elems[state.currentRow][1];
        }
    }

    function propRightClick(e, state, h) {
        if (h.row === 2) {
            state.impl = (state.impl === 1) ? 0 : 1;
            setScaleFactor(scaleFactor);
            e.preventDefault();
        }
    }

    function rightClickEvent(canvas, state) {
        return function (e) {
            clickHotEvent2(e, canvas, state, propRightClick);
        }
    }

    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateD.hots.length; i < l; i++) {
            let h = stateD.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                let u = stateD.userInput[h.row];
                stateD.userInput[h.row] = (u === 0) ? 1 : 0;
                stateD.D = (h.row === 0) ? (stateD.D === 1) ? 0 : 1 : stateD.D;

                switch (h.row) {
                    case 0:
                        setCurrentState(stateD);
                        reInit(canvas, context);
                        break;
                    case 1:

                        // CP changed, call the appropriate function
                        stateD.CP = 1 - stateD.CP;
                        animateRaisingCP();
                        break;
                    case 2:
                        // Right Click, see context event function
                        break;
                    default:
                        alert("Impossible click!");
                }
                break;
            }
        }
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
    }

    function reInit(canvas, context) {
        if (stateD.impl === 0) {
            drawD(canvas, context, stateD, constJK);
        } else {
            drawInnerD(canvas, context, stateD, constJK);
        }
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);
    }

    function startSimulation() {
        stateD.animationOffset++;
        stateSR.animationOffset++;
        if (stateD.animationOffset > 2000) {
            stateD.animationOffset = 0;

        }
        if (stateSR.animationOffset > 2000) {
            stateSR.animationOffset = 0;
        }
        setCurrentState(stateD);
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas, state) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('contextmenu',
            rightClickEvent(canvas, state), false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);

    }


    function drawD(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateD.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let D = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [D, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.flipSR, clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["D", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 3,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: constants.colors.dodgerBlue,
            state: state
        };

        // Save clickHots
        saveClickHots(stateD, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                reInit(canvas, context);
            }
        };
    }


    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        if (stateD.CP === 1) {
            stateD.CPEdge = 1;
        } else {
            stateD.CPEdge = 0;
        }
        if (stateD.CPEdge === 1) {
            let newQ = stateD.D;
            let newNQ = (newQ === 1) ? 0 : 1;
            setTimeout(simulationCallback(stateD, "Q", newQ), stateD.msDelay);
            setTimeout(simulationCallback(stateD, "NQ", newNQ), stateD.msDelay);
        }
        stateD.CPEdge = 0;
    }


    function drawInnerD(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateSR.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);

        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let scaleFactorSR = constants.scaleFactor - 0.5;
        let constSR = new Constants(scaleFactorSR);
        let q = (state.Q === 1) ?
            constSR.greenLineActiveColor : constSR.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constSR.greenLineActiveColor : constSR.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constSR.greenLineActiveColor : constSR.passiveStateColor;
        let D = (state.userInput[0] === 1) ?
            constSR.greenLineActiveColor : constSR.passiveStateColor;

        let colorsIn = [D, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: "#CB3A34", clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 2 * 75 * constants.scaleFactor;
        let boxHeight = 120 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["D", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [0, 0],
            textPosOut: [0, 0],
            flipFlopType: 1,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: constants.colors.white,
            state: state
        };

        // Save clickHots
        saveClickHots(stateD, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        drawInnerSR(context, genericBox, stateSR, constSR);
        //drawOuterTextBoxes(context, genericBox, constants);
        passiveStyle(context);
    }

    function getFlipFlopYLinesOff(height, numberOfIn) {
        let h = height;
        let lineDistanceIn = (h / numberOfIn);
        return ((h - (numberOfIn - 1) * lineDistanceIn) / 2);
    }


    function drawInnerSR(context, outerBox, stateSR, constants) {
        // Clear rect
        context.save();

        // init current values, passive and active states.
        let S = (outerBox.state.D === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let R = (outerBox.state.D === 0) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let q = (outerBox.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (outerBox.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        // Set inner CP
        stateSR.CP = stateD.CP;

        let colorsIn = [S, R];
        let colorsOut = [q, nQ];
        let colorsBox = {box: "#CB3A34"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 70 * constants.scaleFactor;
        let boxHeight = 90 * constants.scaleFactor;
        let xOff = 30 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: outerBox.startX + (outerBox.boxWidth - boxWidth) / 2 + xOff,
            startY: outerBox.startY + (outerBox.boxHeight - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["S", "R"],
            outText: ["Q", "Q'"],
            addClickBox: [false, false],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: constants.colors.white,
            fillStyle: colorsBox.box,
            state: stateSR
        };

        // Save clickHots
        saveClickHots(stateD, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);

        drawCPBelow(context, genericBox, constants, 3);
        drawInnerLines(context, outerBox, genericBox, constants);
        passiveStyle(context);
    }

    function drawInnerLines(context, box, innerBox, constants) {
        // Active and passive colours
        let S = (box.state.D === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let R = (box.state.D === 0) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let CP = (box.state.CP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let Q = (box.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (box.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        let numberOfIn = 2;
        let h = box.boxHeight;
        let hInner = innerBox.boxHeight;
        let lineDistanceIn = (h / numberOfIn);
        let yLinesOff = (h - (numberOfIn - 1) * lineDistanceIn) / 2;

        let lineDistanceInInner = (hInner / numberOfIn);
        let yLinesOffInner = (hInner - (numberOfIn - 1) * lineDistanceInInner) / 2;
        let xIn = 20 * constants.scaleFactor;
        let yDown = (innerBox.startY + yLinesOffInner) - (box.startY + yLinesOff);

        // Line from outer to inner
        // Line to S
        let lineOutToInS = {
            startX: box.startX,
            startY: box.startY + yLinesOff,
            color: constants.passiveStateColor
        };
        lineOutToInS.endX = lineOutToInS.startX + xIn;
        lineOutToInS.endY = lineOutToInS.startY;
        let lineToS = {
            startX: lineOutToInS.endX,
            startY: lineOutToInS.endY,
            color: constants.passiveStateColor
        };
        lineToS.endX = lineToS.startX;
        lineToS.endY = lineToS.startY + yDown;

        let lineToSLeft = {
            startX: lineToS.endX,
            startY: lineToS.endY,
            color: constants.passiveStateColor
        };
        lineToSLeft.endX = innerBox.startX;
        lineToSLeft.endY = lineToSLeft.startY;

        passiveStyle(context);
        drawConnectLine(context, lineOutToInS, constants.tubeLineWidth);
        drawConnectLine(context, lineToS, constants.tubeLineWidth);
        drawConnectLine(context, lineToSLeft, constants.tubeLineWidth);
        if (S === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineOutToInS.color = constants.greenLineActiveColor;
            lineToS.color = constants.greenLineActiveColor;
            lineToSLeft.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineOutToInS, constants.currentLineWidth);
            drawConnectLine(context, lineToS, constants.currentLineWidth);
            drawConnectLine(context, lineToSLeft, constants.currentLineWidth);
        }
        passiveStyle(context);

        // Line from S to inv
        // Line to R

        let lineInvIn = {
            startX: lineToS.endX,
            startY: lineToS.endY,
            color: constants.passiveStateColor
        };
        lineInvIn.endX = lineInvIn.startX;
        lineInvIn.endY = innerBox.startY + innerBox.boxHeight - yLinesOffInner;

        let xInvOff = 20 * constants.scaleFactor;
        let lineInvInLeft = {
            startX: lineInvIn.endX,
            startY: lineInvIn.endY,
            color: constants.passiveStateColor
        };
        lineInvInLeft.endX = lineInvInLeft.startX + xInvOff;
        lineInvInLeft.endY = lineInvInLeft.startY;

        passiveStyle(context);
        drawConnectLine(context, lineInvIn, constants.tubeLineWidth);
        drawConnectLine(context, lineInvInLeft, constants.tubeLineWidth);
        if (S === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineInvIn.color = constants.greenLineActiveColor;
            lineInvInLeft.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineInvIn, constants.currentLineWidth);
            drawConnectLine(context, lineInvInLeft, constants.currentLineWidth);
        }
        passiveStyle(context);

        let invertorHeight = 19 * constants.scaleFactor;
        let baseLength = 18 * constants.scaleFactor;
        let invCP = {
            startX: lineInvInLeft.endX,
            h: invertorHeight,
            baseLength: baseLength,
            color: constants.passiveStateColor,
            lineWidth: constants.connectLinesWidth
        };
        invCP.startY = lineInvInLeft.startY - invCP.baseLength / 2;

        let circleInv = {
            centerX: invCP.startX + invCP.h + constants.IEEEComplementRadius,
            centerY: lineInvInLeft.startY,
            radius: constants.inverterComplementRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            lineWidth: constants.connectLinesWidth,
            fillStyle: "white"
        };

        passiveStyle(context);
        drawCircle(context, circleInv);
        drawTriangle(context, invCP);

        let lineInvOut = {
            startX: circleInv.centerX + constants.IEEEComplementRadius,
            startY: circleInv.centerY,
            color: constants.passiveStateColor
        };

        lineInvOut.endX = innerBox.startX;
        lineInvOut.endY = lineInvOut.startY;

        passiveStyle(context);
        drawConnectLine(context, lineInvOut, constants.tubeLineWidth);
        if (R === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineInvOut.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineInvOut, constants.currentLineWidth);
        }
        passiveStyle(context);

        let connectDot =
            {
                centerX: lineToS.endX,
                centerY: lineToS.endY,
                radius: constants.IEEEConnectDotRadius,
                startAngle: toRadians(constants.zeroAngle),
                endAngle: toRadians(constants.fullCircle),
                rotation: false,
                color: constants.passiveStateColor,
                fillStyle: constants.passiveStateColor,
                lineWidth: constants.connectDotLineWidth
            };
        drawCircle(context, connectDot);

        let lineCPIn = getLineParams(
            box.startX,
            box.startY + box.boxHeight - yLinesOff,
            box.startX + xIn,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let downCP = 25 * constants.scaleFactor;
        let lineCPDown = getLineParams(
            lineCPIn.endX,
            lineCPIn.endY,
            lineCPIn.endX,
            lineCPIn.endY + downCP,
            constants.passiveStateColor
        );

        let rightCP = getLineParams(
            lineCPDown.endX,
            lineCPDown.endY,
            innerBox.startX + (innerBox.boxWidth / 2),
            lineCPDown.endY,
            constants.passiveStateColor
        );

        let lineCPUp = getLineParams(
            rightCP.endX,
            rightCP.endY,
            rightCP.endX,
            innerBox.startY + innerBox.boxHeight,
            constants.passiveStateColor
        );
        drawComplexLine(context, lineCPIn, CP, box.state, constants);
        drawComplexLine(context, lineCPDown, CP, box.state, constants);
        drawComplexLine(context, rightCP, CP, box.state, constants);
        drawComplexLine(context, lineCPUp, CP, box.state, constants, false, false);


        let lineQOut = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            box.startY + yLinesOff,
            constants.passiveStateColor
        );
        let lineNQOut = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + innerBox.boxHeight - yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let lineNQRight = getLineParams(
            lineNQOut.endX,
            lineNQOut.endY,
            box.startX + box.boxWidth,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let lineQRight = getLineParams(
            lineQOut.endX,
            lineQOut.endY,
            box.startX + box.boxWidth,
            box.startY + yLinesOff,
            constants.passiveStateColor
        );
        drawComplexLine(context, lineQOut, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQOut, nQ, box.state, constants, false, false);
        drawComplexLine(context, lineQRight, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQRight, nQ, box.state, constants, false, false);
    }

    function drawComplexLine(context, line, cond, state,
                             constants, clearCanvas = false, direction = true) {
        passiveStyle(context);
        drawConnectLine(context, line,
            constants.tubeLineWidth, clearCanvas, direction);
        if (cond === constants.greenLineActiveColor) {
            activeStyle(context, state);
            line.color = constants.greenLineActiveColor;
            drawConnectLine(context, line,
                constants.currentLineWidth, clearCanvas, direction);
        }
        passiveStyle(context);
    }

    function getLineParams(x1, y1, x2, y2, c) {
        return {
            startX: x1,
            startY: y1,
            endX: x2,
            endY: y2,
            color: c
        };
    }

    function init() {
        if (stateD.impl === 0) {
            drawD(canvas, context, stateD, constJK);
        } else {
            drawInnerD(canvas, context, stateD, constJK);
        }
    }

    init();
    eventListeners(canvas, stateD);
    startSimulation();
}
