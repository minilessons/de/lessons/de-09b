/* ChangeLog */
// Chaned how gradients calculates,
// if lib is used elsewhere change the code accordingli


/**
 * Created by Komediruzecki on 1.11.2016..
 */

/* Comments on more refactoring...
 1. Refactor scale factor to be class which can every sketch use**
 2. Add functions for IEEE-OR logic gate
 3. Make functions for Flip Flop type connect *return line to start pos*
 */

/**
 * Constants which can be scaled with scale factor.
 * This constructor holds default values for all constants, but all can change if user wants.
 */
class Constants {
    constructor(/**
                 * @param scaleFactor scale factor to scale all components
                 * @param boxHeight standard box height (default=50)
                 * @param boxWidth standard box width
                 * @param lineLength length of AND logic gate lines
                 * @param IEEEArcRadius radius for AND logic gate
                 * @param inputLineLength length of input lines for logic gates
                 * @param lineWidthIO width of input and output lines for logic gates
                 * @param inputLineDistance distance from input lines on logic gates
                 * @param IEEEComplementRadius complement radius for complementing inputs and outputs
                 * @param IEEEComplementDiameter complement diameter
                 * @param IEEEConnectDotRadius radius for little black circle for joining wires
                 * @param IEEEConnectDiameter connect dot diameter
                 * @param clickBoxWidth width of a click box
                 * @param clickBoxHeight height of a click box
                 * @param constBoxWidth width for constant boxes
                 * @param constBoxHeight height for constants boxes
                 * @param outLineLength length of line that every logic gate extends after output line
                 * @param verticalUpLength length of offset from logic gates
                 * @param zeroAngle standard angle for drawing full circle (value=0)
                 * @param fullCircle standard full angle for drawing circles (value=360)
                 * @param varTextSize size of text for commenting inputs and outputs of logic gates
                 */
                scaleFactor = 1,
                boxHeight = 50,
                boxWidth = 50,
                lineLength = 20,
                IEEEArcRadius = 20,
                inputLineLength = 15,
                lineWidthIO = 2,
                inputLineDistance = 20,
                IEEEComplementRadius = (Math.PI + 0.5),
                IEEEConnectDotRadius = (Math.PI - 1),
                clickBoxWidth = 15,
                clickBoxHeight = 15,
                constBoxWidth = 15,
                constBoxHeight = 15,
                outLineLength = 50,
                verticalUpLength = 50,
                zeroAngle = 0,
                fullCircle = 360,
                varTextSize = 24,
                CPLineWidth = 4,
                textLineWidth = 2,
                connectDotLineWidth = 1,
                complementLineWidth = 1,
                constBoxLineWidth = 1,
                connectLinesWidth = 2,
                clickBoxLineWidth = 1,
                complementColor = "black",
                IECFontSize = 21,
                standardLineColor = "black",
                textBoxWidth = 20,
                textBoxHeight = 20,
                blueTextColor = "blue",
                commonFillStyle = "#83A59E",
                commonTextOffset = 5,
                universalBoxWidth = 40,
                universalBoxHeight = 40,
                currentStateColor = "#FF1953",
                activeStateColor = "#962D8D",
                passiveStateColor = "black",
                redLineActiveColor = "red",
                simStyle = "#000000",
                simTextSize = "bold 16px Courier",
                simTextFont = "19px Courier",
                schemaComplementRadius = (Math.PI + 0.5) * 1.3,
                overLineLength = 10,
                clickHotsOffset = 10,
                currentLineWidth = 1.5,
                currentColor = "#41f547",
                complementFill = "#ffffff",
                frameColor = "#319666",
                frameBlacked = "#131212",
                backgroundFrameWidth = "2px solid black",
                stateDiagramFont = "12px Courier",
                IECTextFont = 21,
                timeTextFont = "14px Courier",
                delayTextColor = "#96396a",
                timeTextSize = 14,
                tubeLineWidth = 2,
                greenLineActiveColor = "#41f547",
                fontSizeIOValues = "12px",
                standardFont = " Courier",
                valuesBoxWidth = 5,
                valuesBoxHeight = 7,
                inverterComplementRadius = (Math.PI - 1),
                colors = {
                    black: "black",
                    yellow: "yellow",
                    red: "red",
                    white: "white",
                    orange: "orange",
                    awesome: "#8836ba",
                    gray: "#AF1E37",
                    darkRed: "#d02b2a",
                    andColor: "#ABB7B7",
                    purple: "#8e44ad",
                    emerald: "#2ecc71",
                    midnight: "#2c3e50",
                    dodgerBlue: "#1E90FF",
                    flipD: "#1E90FF",
                    flipT: "#886F98",
                    diagramArrow: "#da0000",
                    flipSR: "#CB3A34",
                    flipJK: "#2ecc71"
                },
                flipFontSize = 10,
                flipFontSizeInner = 14,
                flipFont = "px Courier",
                inputLineLengthNew = 21) {
        /** Defines scale to use on all constants. */
        this.scaleFactor = scaleFactor;
        /**
         * Standard IEEE box height
         * @type {number} value of boxHeight
         */
        this.boxHeight = boxHeight * scaleFactor;
        /**
         * Standard IEEE box width
         * @type {number} value of boxWidth
         */
        this.boxWidth = boxWidth * scaleFactor;

        /**
         * Length of line that extend arc in IEEE AND box
         * @type {number}
         */
        this.lineLength = lineLength * scaleFactor;
        /**
         * Standard radius of arc that is used for AND logic gates
         * @type {number} value of arc radius
         */
        this.IEEEArcRadius = IEEEArcRadius * scaleFactor;
        /**
         * Height of AND logic gate box
         * @type {number} value of box Height
         */
        this.NANDBoxHeight = 2 * this.IEEEArcRadius;
        /**
         * Width of AND logic gate box
         * @type {number} value of box width
         */
        this.NANDBoxWidth = this.IEEEArcRadius + this.lineLength;

        /**
         * Line length of inputs in logic gate box
         * @type {number} value of length
         */
        this.inputLineLength = inputLineLength * scaleFactor;

        /**
         * Line width for input and output logic boxes
         * @type {number} value of line width
         */
        this.lineWidthIO = lineWidthIO * scaleFactor;

        /**
         *
         * @type {number}
         */
        this.inputLineDistance = inputLineDistance * scaleFactor;

        /**
         * Standard IEEE radius of a complement circle
         * @type {number} value of radius
         */
        this.IEEEComplementRadius = IEEEComplementRadius * scaleFactor;
        /**
         * Standard IEEE diameter for complement circle
         * @type {number} value of diameter
         */
        this.IEEEComplementDiameter = 2 * this.IEEEComplementRadius;

        /**
         * Standard IEEE radius of connect dot for joining lines
         * @type {number} value of radius
         */
        this.IEEEConnectDotRadius = IEEEConnectDotRadius * scaleFactor;

        /**
         * Standard click box height for clickable boxes beside logic gates
         * @type {number} value of height of a click box
         */
        this.clickBoxHeight = clickBoxHeight * scaleFactor;
        /**
         * Standard click box width for clickable boxes beside logic gates
         * @type {number} value of width of a click box
         */
        this.clickBoxWidth = clickBoxWidth * scaleFactor;

        /**
         * Standard constant box height for constants on input of logic gates
         * @type {number} value of height of a constant box
         */
        this.constBoxWidth = constBoxWidth * scaleFactor;

        /**
         * Standard constant box width for constants on input of logic gates
         * @type {number} value of width of a constant box
         */
        this.constBoxHeight = constBoxHeight * scaleFactor;

        /**
         * Length of a line that goes out of logic gate box
         * @type {number} value of length
         */
        this.outLineLength = outLineLength * scaleFactor;

        /**
         * Length of vertical line that connects returning line
         * and middle of logic gate
         * @type {number} line length
         */
        this.verticalUpLength = verticalUpLength * scaleFactor;

        /**
         * Constant angle for drawing circle
         * @type {number} value: 0
         */
        this.zeroAngle = zeroAngle;

        /**
         * Constant angle for drawing full circle
         * @type {number} value: 360
         */
        this.fullCircle = fullCircle;

        /**
         * Text size standard for writing beside logic gate boxes
         * @type {number}
         */
        this.varTextSize = varTextSize * scaleFactor * 0.7;

        this.textBoxWidth = textBoxWidth * scaleFactor;
        this.textBoxHeight = textBoxHeight * scaleFactor;
        this.complementColor = complementColor;
        this.CPLineWidth = CPLineWidth;
        this.textLineWidth = textLineWidth;
        this.connectDotLineWidth = connectDotLineWidth;
        this.complementLineWidth = complementLineWidth;
        this.constBoxLineWidth = constBoxLineWidth;
        this.connectLinesWidth = connectLinesWidth;
        this.clickBoxLineWidth = clickBoxLineWidth;
        this.IECFontSize = IECFontSize * scaleFactor;
        this.standardLineColor = standardLineColor;
        this.blueTextColor = blueTextColor;
        this.commonFillStyle = commonFillStyle;
        this.commonTextOffset = commonTextOffset;

        this.universalBoxWidth = universalBoxWidth * scaleFactor;
        this.universalBoxHeight = universalBoxHeight * scaleFactor;
        this.currentStateColor = currentStateColor;
        this.activeStateColor = activeStateColor;
        this.redLineActiveColor = redLineActiveColor;
        this.passiveStateColor = passiveStateColor;

        this.simStyle = simStyle;
        this.simTextSize = simTextSize;
        this.simTextFont = simTextFont;

        this.schemaComplementRadius = schemaComplementRadius * scaleFactor;
        this.overLineLength = overLineLength * scaleFactor;
        this.clickHotsOffset = clickHotsOffset;
        this.currentLineWidth = currentLineWidth * scaleFactor;
        this.currentColor = currentColor;
        this.complementFill = complementFill;
        this.frameColor = frameColor;
        this.frameBlacked = frameBlacked;
        this.backgroundFrameWidth = backgroundFrameWidth;
        this.stateDiagramFont = stateDiagramFont;
        this.IECTextFont = (IECTextFont * scaleFactor) + "px Courier";
        this.timeTextFont = timeTextFont;
        this.delayTextColor = delayTextColor;
        this.timeTextSize = timeTextSize;
        this.tubeLineWidth = tubeLineWidth * scaleFactor;
        this.greenLineActiveColor = greenLineActiveColor;
        this.fontSizeIOValues = fontSizeIOValues;
        this.standardFont = standardFont;
        this.valuesBoxWidth = valuesBoxWidth * scaleFactor;
        this.valuesBoxHeight = valuesBoxHeight * scaleFactor;
        this.inverterComplementRadius = inverterComplementRadius * scaleFactor;
        this.colors = colors;
        this.flipFontSize = flipFontSize * scaleFactor;
        this.flipFont = flipFont;
        this.flipFontSizeInner = flipFontSizeInner * scaleFactor;
        this.inputLineLengthNew = inputLineLengthNew * scaleFactor;
    }
}

/**
 * Defines global constants with scale factor 1.
 * @type {Constants}
 */
let globalConstants = new Constants(1);

class SRTypes {
    constructor() {
        this.RAISINGEDGE = "raisingEdge";
        this.FALLINGEDGE = "failingEdge";
        this.ACTIVEHIGH = "activeHigh";
        this.ACTIVELOW = "activeLow";
    }
}

class LogicGates {
    constructor() {
        this.AND = "AND";
        this.OR = "OR";
        this.XOR = "XOR";
        this.NAND = "NAND";
        this.NOR = "NOR";
        this.XNOR = "XNOR";
        this.ANDSIGN = "&";
        this.ORSIGN = '\u2265';
        this.XORSIGN = "=1";
    }
}

class usedUnicode {
    constructor() {
        /* Subscript <sub> x </sub> numbers */
        this.sup0 = '\u2070';
        this.sup1 = '\u00B9';
        this.sup2 = '\u00B2';
        this.sup3 = '\u00B3';
        this.sup4 = '\u2074';
        this.sup5 = '\u2075';
        this.sup6 = '\u2076';
        this.sup7 = '\u2077';
        this.sup8 = '\u2078';
        this.sup9 = '\u2079';

        /* Superscript <sup> x </sup> numbers */
        this.sub0 = '\u2080';
        this.sub1 = '\u2081';
        this.sub2 = '\u2082';
        this.sub3 = '\u2083';
        this.sub4 = '\u2084';
        this.sub5 = '\u2085';
        this.sub6 = '\u2086';
        this.sub7 = '\u2087';
        this.sub8 = '\u2088';
        this.sub9 = '\u2089';

        // Letters
        this.subA = '\u2090';
        this.subE = '\u2091';
        this.subO = '\u2092';
        this.subM = '\u2098';
        this.subN = '\u2099';

        this.supN = '\u207F';

        // Other signs
        this.subPlus = '\u208A';
        this.subMinus = '\u208B';
        this.subBracketLeft = '\u208F';
        this.supBracketLeft = '\u207D';
        this.subBracketRight = '\u208E';
        this.supBracketRight = '\u207E';
    }
}

/**
 * Defines Box class which can be used as template for other elements.
 * @Note Only for learning purposes...
 */
class Box {
    constructor(x, y, w, h) {
        this.boxXOff = x;
        this.boxYOff = y;
        this.boxWidth = w;
        this.boxHeight = h;
    }
}

/**
 * Defines BoxHandler template which uses Box as base class and adds color.
 * @Note Only for learning purposes...
 */
class BoxHandler extends Box {
    constructor(x, y, w, h, color) {
        super(x, y, w, h);
        this.color = color;
    }

    get getColor() {
        return this.color;
    }
}

/**
 * Defines LineHandler template which uses different set of colors.
 * @Note Only for learning purposes...
 */
class IEEELineHandler extends BoxHandler {
    constructor(x, y, w, h, complementList, complementDiameter, lineWidth,
                lineLength, inputLineDistance,
                color = [globalConstants.passiveStateColor,
                    globalConstants.passiveStateColor,
                    globalConstants.passiveStateColor], state = undefined) {
        super(x, y, w, h, "black");
        this.lineLength = lineLength;
        this.inputLineDistance = inputLineDistance;
        this.lineWidth = lineWidth;
        this.complementDiameter = complementDiameter;
        this.complementList = complementList;
        this.colorIn1 = color[0];
        this.colorIn2 = color[1];
        this.colorOut = color[2];
    }

    get getColorIn1() {
        return this.colorIn1;
    }

    get getColorIn2() {
        return this.colorIn2;
    }

    get getColorOut() {
        return this.colorOut;
    }
}

/**
 *
 * @param x coordinate for clickBox
 * @param y coordinate for clickBox
 * @param w width of clickBox
 * @param h height of clickBox
 * @param row index for userInput
 * @constructor
 */
function Handler(x, y, w, h, row) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.row = row;
}

function getInputLinesYOff(constants) {
    return ((constants.universalBoxHeight - constants.inputLineDistance) / 2);
}

function setGradient(context, box) {
    for (let i = 0, l = box.length; i < l; i++) {
        box[i].fillStyle = calcGradient(context, box[i].startX,
            box[i].startY, box[i].startX + box[i].boxWidth,
            box[i].startY);
    }
}

function setGradientGeneric(box, gradient) {
    box.fillStyle = gradient;
}
function setGradient2(context, box) {
    for (let i = 0, l = box.length; i < l; i++) {
        box[i].fillStyle = calcGradient2(context, box[i].startX,
            box[i].startY, box[i].startX + box[i].boxWidth,
            box[i].startY);
    }
}

function getGradientParams(box) {
    return {
        x1: box.startX,
        y1: box.startY,
        x2: box.startX + box.boxWidth,
        y2: box.startY
    }
}

function calcGradient2(context, args) {
    let startX = Math.round(args.x1);
    let startY = Math.round(args.y1);
    let endX = Math.round(args.x2);
    let endY = Math.round(args.y2);
    let gradient = context.createLinearGradient(startX, startY, endX, endY);
    gradient.addColorStop(0, "#3CA55C");
    gradient.addColorStop(1, "#B5AC49");

    return gradient;
}

function calcGradient3(context, args) {
    let startX = Math.round(args.x1);
    let startY = Math.round(args.y1);
    let endX = Math.round(args.x2);
    let endY = Math.round(args.y2);
    let gradient = context.createLinearGradient(startX, startY, endX, endY);
    gradient.addColorStop(0, "#348F50");
    gradient.addColorStop(1, "#56B4D3");

    return gradient;
}

function calcGradientCurrent(context, args) {
    let startX = Math.round(args.x1);
    let startY = Math.round(args.y1);
    let endX = Math.round(args.x2);
    let endY = Math.round(args.y2);
    let gradient = context.createLinearGradient(startX, startY, endX, endY);
    gradient.addColorStop(0, "#248e61");
    gradient.addColorStop(1, "#0bc2cd");

    return gradient;
}


function calcGradient(context, args) {
    let startX = Math.round(args.x1);
    let startY = Math.round(args.y1);
    let endX = Math.round(args.x2);
    let endY = Math.round(args.y2);
    let gradient = context.createLinearGradient(startX, startY, endX, endY);    //
    gradient.addColorStop(0, "#115c4e");
    gradient.addColorStop(1, "#237A57");

    gradient.addColorStop(0, "#3a6186");
    gradient.addColorStop(1, "#89253e");


    //gradient.addColorStop(1, "#3FC380");
    return gradient;
}

function binaryToDecimal(sequenceBits) {
    let num = 0;
    for (let c = 0, l = sequenceBits.length; c < l; c++) {
        num += sequenceBits[c] * Math.pow(2, l - c - 1);
    }
    return num;
}

function writeMultilineText(canvas, context, multiLineText) {
    let rect = canvas.getBoundingClientRect();
    let lineHeight = 20;
    let lines = multiLineText.split("\n");

    let yOffsetFromBoundingRect = 5;
    let textOff = lines.length * lineHeight + yOffsetFromBoundingRect;
    let startX = rect.width / 2;
    let startY = rect.height - textOff;

    context.beginPath();
    context.strokeStyle = globalConstants.simStyle;
    context.font = globalConstants.simTextFont;
    context.fillStyle = globalConstants.simStyle;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.clearRect(1, startY - 20, canvas.width - 2, canvas.height - startY + 19);
    for (let i = 0, l = lines.length; i < l; i++) {
        context.fillText(lines[i], startX, startY + (i * lineHeight));
        //context.strokeText(lines[i], startX, startY + (i * lineHeight));
    }
}

function writeMultilineText2(context, args, lineHeight, multiLineText, centered = true) {
    let lines = multiLineText.split("\n");

    let x1 = args.leftVertexX;
    let y1 = args.leftVertexY;
    let w = args.boxWidth;
    let h = args.boxHeight;

    // Set text in the center
    let textX = (centered === true) ? x1 + (w / 2) : x1;
    let textY = (centered === true) ? y1 + (h / 2) : y1;

    context.beginPath();
    context.font = args.textFont;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = args.fillStyle;
    context.strokeStyle = args.color;
    context.lineWidth = args.lineWidth;
    for (let i = 0, l = lines.length; i < l; i++) {
        context.fillText(lines[i], textX, textY + (i * lineHeight));
        //context.strokeText(lines[i], textX, textY + (i * lineHeight));
    }
}


function writeSimulationName(canvas, context, name) {
    writeMultilineText(canvas, context, name);
}

function toDegrees(radian) {
    return (radian * 180) / Math.PI;
}
function toRadians(degree) {
    return (Math.PI / 180) * degree;
}

/**
 * This function saves hot spots for <code>mousedown</code> and  <code>mouseup</code> events.
 * @param state box state which will be used to save hots
 * @param x coordinate of clickBox
 * @param y coordinate of clickBox
 * @param w width of clickBox
 * @param h height of clickBox
 * @param row index for userInput
 */
function saveClickHots(state, x, y, w, h, row) {
    state.hots.push(new Handler(x, y, w, h, row));
}

function genericCalcOut(state) {
    return state.curOut;
}

function setInputState(userInput, index) {
    for (let i = 0, l = userInput.length; i < l; i++) {
        userInput[i] = (i === index) ? 1 : 0;
    }
}

function resetInputState(userInput, index) {
    for (let i = 0, l = userInput.length; i < l; i++) {
        userInput[i] = (i === index) ? 0 : 1;
    }
}

function drawSR(canvas, context, state, constants) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    writeSimulationName(canvas, context, state.simName);
    setBackgroundFrame(context, canvas.width, canvas.height);

    let lineColorOut = state.curOut === 0 ? "black" : "red";
    let lineColorIn1 = state.userInput[0] === 0 ? "black" : "red";
    let lineColorIn2 = state.userInput[1] === 0 ? "black" : "red";
    let lineColorCp = state.cp === 0 ? "black" : "red";
    let colors = [lineColorIn1, lineColorIn2, lineColorOut, lineColorCp];
    let colorsBs = {boxColor: "#CB3A34", lineColor: colors, cBoxColor: "#746dc5"};

    // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
    // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
    let boxWidth = 150;
    let boxHeight = 150;
    let cpLength = 30;
    let boxXOffset = (canvas.width - boxWidth) / 2;
    let boxYOffset = (canvas.height - boxHeight - cpLength) / 2;
    let lineLength = 40;
    let inputLineDistance = 70;
    let clickBoxWidth = 35;
    let clickBoxHeight = 35;
    let complementList = [0, 0, 0];
    let boxLineWidth = constants.clickBoxLineWidth + 1;
    let optsBs = setOptions(boxXOffset, boxYOffset, boxWidth, boxHeight,
        boxLineWidth, lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight,
        complementList, colorsBs);

    // Init classes
    let boxBs = new BoxHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.boxColor);

    /*let boxHandler = {
     boxXOff: optsBs.boxXOff,
     boxYOff: optsBs.boxYOff,
     boxWidth: optsBs.boxWidth,
     boxHeight: optsBs.boxHeight,
     color: optsBs.boxColor
     };
     */
    let linesIEEE = new IEEELineHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.complementList,
        constants.IEEEComplementDiameter,
        optsBs.lineWidth, optsBs.lineLength,
        optsBs.inputLineDistance, optsBs.lineColor);

    let lineHandlerBs = new LineHandler(optsBs.boxXOff, optsBs.boxYOff,
        optsBs.boxWidth, optsBs.boxHeight, optsBs.lineWidth, optsBs.lineLength,
        optsBs.inputLineDistance, optsBs.lineColor);

    let clickBoxHandler = {
        boxXOff: optsBs.boxXOff,
        boxYOff: optsBs.boxYOff,
        w: optsBs.boxWidth,
        h: optsBs.boxHeight,
        lineWidth: optsBs.lineWidth,
        lineLength: optsBs.lineLength,
        inputLineDistance: optsBs.inputLineDistance,
        boxWidth: optsBs.boxWidth,
        boxHeight: optsBs.boxHeight,
        clickBoxWidth: optsBs.clickBoxWidth,
        clickBoxHeight: optsBs.clickBoxHeight,
        color: optsBs.clickBoxColor
    };

    // Draw first Black Box
    let states = [0, 1];
    drawBox(context, boxBs);
    drawLines(context, linesIEEE, constants);
    drawClickBoxes(context, clickBoxHandler, state, states, genericCalcOut(state));
    drawTextForBox(context, lineHandlerBs, constants);

    if (state.cp != undefined) {
        let typeSpec = (state.typeSpec != undefined) ? state.typeSpec : 1;
        drawCP(context, lineHandlerBs, constants, typeSpec);
    }
}


/**
 * This function draws clickBoxes.
 * @param context context in which clickBox is drawn
 * @param params parameters which are used for calculating clickBox position
 * @param state box state variable which is used for saving clickHots
 * @param states list of indexes for userInput
 * @param output function which calculates box output value
 */
function drawClickBoxes(context, params, state, states, output) {
    // Input first box
    let w = params.clickBoxWidth;
    let h = params.clickBoxHeight;
    let inputLinesYOff = (params.boxHeight - params.inputLineDistance) / 2;
    let x1 = params.boxXOff - params.lineLength - w;
    let y1 = params.boxYOff + inputLinesYOff - (h / 2);

    context.beginPath();
    context.rect(x1, y1, w, h);
    context.fillStyle = "#83A59E";
    context.fill();
    saveClickHots(state, x1, y1, w, h, states[0]);

    context.font = "24px Courier";
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = params.color;
    context.strokeStyle = "black";
    context.lineWidth = params.lineWidth;

    // Set text in the center
    let textX = x1 + (w / 2);
    let textY = y1 + (h / 2);

    context.strokeText(state.userInput[states[0]], textX, textY);
    context.stroke();
    // Input Second box
    let clickBoxDistance = params.boxHeight - 2 * inputLinesYOff;
    let x2 = x1;
    let y2 = y1 + clickBoxDistance;
    context.beginPath();
    context.rect(x2, y2, w, h);
    context.fillStyle = "#83A59E";
    context.fill();
    saveClickHots(state, x2, y2, w, h, states[1]);

    // Text
    // Set text in the center
    let textX2 = x2 + (w / 2);
    let textY2 = y2 + (h / 2);
    context.strokeText(state.userInput[states[1]], textX2, textY2);
    context.stroke();

    // Output box
    let x3 = params.boxXOff + params.boxWidth + params.lineLength;
    let y3 = params.boxYOff + (params.boxHeight / 2) - (h / 2);
    context.beginPath();
    context.rect(x3, y3, w, h);
    context.fillStyle = "#83A59E";
    context.fill();

    let outText = output;
    // Set text in the center
    let textX3 = x3 + (w / 2);
    let textY3 = y3 + (h / 2);
    context.strokeText(outText.toString(), textX3, textY3);
    context.stroke();
}

function drawClickBoxesNew(context, args, line, constants) {
    // Draw box for given line
    let w = args.clickBoxWidth;
    let h = args.clickBoxHeight;
    let x1 = line.endX - w;
    let y1 = line.startY - (h / 2);

    context.beginPath();
    context.rect(x1, y1, w, h);
    context.fillStyle = constants.commonFillStyle;
    context.fill();

    saveClickHots(args.state, x1, y1, w, h, line.clickId);

    context.font = constants.flipFontSize + constants.flipFont;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = constants.colors.black;
    context.strokeStyle = constants.passiveStateColor;
    context.lineWidth = constants.textLineWidth;

    // Set text in the center
    let textX = x1 + (w / 2);
    let textY = y1 + (h / 2);

    context.strokeText(args.state.userInput[line.clickId], textX, textY);
    context.stroke();
}

function mouseZoomer(e, canvas, context, zoomHots, oldScaleFactor, functionToCall) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
    // Check if mouse is in area which we consider..
    for (let i = 0, l = zoomHots.hots.length; i < l; i++) {
        let h = zoomHots.hots[i];
        if (pos.x >= h.x && pos.x < h.x + h.w &&
            pos.y >= h.y && pos.y < h.y + h.h) {
            // cross-browser wheel delta
            e = window.event || e; // old IE support
            let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
            let newScaleFactor = Math.max(zoomHots.min, Math.min(zoomHots.max,
                oldScaleFactor + (0.1 * delta)));
            functionToCall(newScaleFactor, canvas, context);
            e.returnValue = false;
            break;
        }
    }
}

function drawComplements(context, arrayOfLines, complementList = [0, 0, 0], constants) {
    let line = [
        {
            centerX: arrayOfLines[0].startX + constants.IEEEComplementRadius,
            centerY: arrayOfLines[0].startY,
            radius: constants.IEEEComplementRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            color: constants.complementColor,
            fillStyle: constants.complementFill,
            lineWidth: constants.complementLineWidth
        },
        {
            centerX: arrayOfLines[1].startX + constants.IEEEComplementRadius,
            centerY: arrayOfLines[1].startY,
            radius: constants.IEEEComplementRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            color: constants.complementColor,
            fillStyle: constants.complementFill,
            lineWidth: constants.complementLineWidth
        },
        {
            centerX: arrayOfLines[2].startX - constants.IEEEComplementRadius,
            centerY: arrayOfLines[2].startY,
            radius: constants.IEEEComplementRadius,
            startAngle: toRadians(0),
            endAngle: toRadians(360),
            color: constants.complementColor,
            fillStyle: constants.complementFill,
            lineWidth: constants.complementLineWidth
        }
    ];
    for (let c = 0, l = complementList.length; c < l; c++) {
        if (complementList[c] === 1) {
            drawCircle(context, line[c]);
        }
    }
}

/**
 * Function draws complement on lines that needs it.
 * @param context context to draw complement to
 * @param line line which have starting point for complement
 * @param switchIO set this to 0 if line is OUTPUT line, or 1 if it is INPUT line
 * @param constants used constants
 */
function drawComplementGeneric(context, line, switchIO, constants) {
    let sign = (switchIO === 0) ? 1 : -1;
    let complement = {
        centerX: line.startX + (sign * constants.IEEEComplementRadius),
        centerY: line.startY,
        radius: constants.IEEEComplementRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        color: constants.complementColor,
        fillStyle: constants.complementFill,
        lineWidth: constants.complementLineWidth
    };
    drawCircle(context, complement);
}

function clearStateHots(state) {
    state.hots = [];
}

/**
 * Function which draws lines at given context.
 * Three lines are drawn, input1, input2 & output.
 * @param context context in which lines are drawn
 * @param params parameters that are used for calculating position for lines
 * @param constants constants for IEEE boxes
 * @param state state for clickHots saving
 * @param clickHots click hots on IO lines
 * @param clearCanvas clears canvas before drawing line
 * @param lineLengths lengths of IO lines
 */
function drawLines(context, params, constants, state = undefined,
                   clickHots = [false, false, false], clearCanvas = false,
                   lineLengths = [constants.inputLineLength,
                       constants.inputLineLength,
                       constants.inputLineLength]) {
    let yLinesOff = (params.boxHeight - params.inputLineDistance) / 2;
    // Input line1 and line2, outputLine
    let lines = [
        {
            startX: params.boxXOff,
            startY: params.boxYOff + yLinesOff,
            endX: params.boxXOff - lineLengths[0],
            endY: params.boxYOff + yLinesOff,
            color: params.getColorIn1,
            lineWidth: params.lineWidth,
            dashed: params.currentFlow
        },
        {
            startX: params.boxXOff,
            startY: params.boxYOff + params.boxHeight - yLinesOff,
            endX: params.boxXOff - lineLengths[1],
            endY: params.boxYOff + params.boxHeight - yLinesOff,
            color: params.getColorIn2,
            lineWidth: params.lineWidth,
            dashed: params.currentFlow
        },
        {
            startX: params.boxXOff + params.boxWidth,
            startY: params.boxYOff + (params.boxHeight / 2),
            endX: params.boxXOff + params.boxWidth + lineLengths[2],
            endY: params.boxYOff + (params.boxHeight / 2),
            color: params.getColorOut,
            lineWidth: params.lineWidth,
            dashed: params.currentFlow
        }
    ];
    let clickHotsOffset = 10;
    for (let c = 0, l = lines.length; c < l; c++) {
        if (params.complementList != undefined && params.complementList[c] === 1) {
            if (c === 2) {
                lines[c].startX += params.complementDiameter;
                lines[c].endX += params.complementDiameter;
            } else {
                lines[c].startX -= params.complementDiameter;
                lines[c].endX -= params.complementDiameter;
            }
        }
        if (state != undefined && clickHots[c] === true) {
            saveConnectLineClickHots(context, state,
                lines[c], clickHotsOffset);
        }
        context.save();
        passiveStyle(context);
        let value = (lines[c].dashed != undefined && lines[c].dashed[c] === true) ? 1 : 0;
        if (c != 2) {
            drawValues(context, lines[c], value, constants);
        }
        activeStyle(context, state);
        context.restore();

        if (lines[c].dashed === undefined || lines[c].dashed[c] === true) {
            let startX = (lines[c].startX > lines[c].endX) ?
                lines[c].endX : lines[c].startX;
            let startY = lines[c].startY;
            let endX = (lines[c].endX === startX) ?
                lines[c].startX : lines[c].endX;
            if (clearCanvas === true) {
                clearLineCanvas(context, lines[c],
                    globalConstants.currentLineWidth);
            }
            context.beginPath();
            context.strokeStyle = lines[c].color;
            context.lineWidth = lines[c].lineWidth;
            context.moveTo(startX, startY);
            context.lineTo(endX, lines[c].endY);
            context.stroke();
        }
    }
    if (lines[0].dashed == undefined) {
        drawComplements(context, lines, params.complementList, constants);
    }
}

/**
 * Draws flip flop text for general flip flop box
 * @param context context in which text is drawn
 * @param line line from which starting points are used
 * @param switchIO defines left or right offset depending on line specification
 * @param centered true specifies centered text (default), otherwise text is not centered
 * @param toggleFill true if fill is needed, otherwise stroke is used
 * @param typeSpec specifies FL type: 0 - Low level active, 2 - Falling edge active
 * 3 - Raising edge active, otherwise High logical level active
 * @param constants used constants
 */
function drawFlipFlopText(context, line, switchIO, constants, centered = true,
                          toggleFill = true, typeSpec = 0) {
    let sign = (switchIO === 1) ? -constants.textBoxWidth : 0;
    let w = constants.textBoxWidth;
    let h = constants.textBoxHeight;
    let x1 = line.startX + sign;
    let y1 = line.startY - (h / 2);

    // Set text in the center
    let textX = (centered === true) ? x1 + (w / 2) : x1;
    let textY = (centered === true) ? y1 + (h / 2) : y1;

    // Type specification of flip flop, can be logical level or edge behaviour
    let xPadInner = 8 * constants.scaleFactor;
    if (typeSpec === 0) {
        drawComplementIndicator(context, x1, line.startY, constants);
        textX += constants.schemaComplementRadius * 2;
        xPadInner += constants.schemaComplementRadius * 2;
        textX += xPadInner;
    } else if (typeSpec === 2) {
        drawComplementIndicator(context, x1, line.startY, constants);
        drawEdgeIndicator(context, x1, textY, constants, 1);
        textX += constants.schemaComplementRadius * 2;
        xPadInner += constants.schemaComplementRadius * 2;
        textX += xPadInner;
    } else if (typeSpec === 3) {
        drawEdgeIndicator(context, x1, line.startY, constants, 1);
        textX += xPadInner;
    }

    context.beginPath();
    context.font = constants.flipFontSizeInner + constants.flipFont;
    context.textAlign = (centered === true) ? "center" : "left";
    context.textBaseline = "middle";
    context.fillStyle = constants.colors.black;
    context.strokeStyle = constants.colors.black;
    context.lineWidth = constants.textLineWidth;
    if (toggleFill === true) {
        context.strokeText(line.text, textX, textY);
        context.stroke();
    } else {
        context.fillText(line.text, textX, textY);
        context.stroke();
    }
}

/**
 * Function draws IO lines at given box.
 * @param context context where lines are drawn
 * @param args encapsulation of an box which holds all needed parameters
 * @param numberOfIn number of lines to draw at left side of the box, input lines
 * @param numberOfOut number of lines to draw at right side of the box, output lies
 * @param constants commonly used constants for getting colors, widths, and other literals
 */
function drawLinesNew(context, args, numberOfIn, numberOfOut, constants) {
    // Draw input lines.
    let h = args.boxHeight;
    let lineDistanceIn = h / numberOfIn;
    let offFromSidesIn = (h - (numberOfIn - 1) * lineDistanceIn) / 2;
    let lineLength = constants.inputLineLengthNew;
    for (let c = 0; c < numberOfIn; c++) {
        let curLineIn = {
            startX: args.startX,
            endX: args.startX - (lineLength),
        };
        curLineIn.startY = args.startY + offFromSidesIn + (c * lineDistanceIn);
        curLineIn.endY = curLineIn.startY;
        curLineIn.color = constants.passiveStateColor;
        curLineIn.clickId = c;
        curLineIn.text = args.textIn[c];

        let flipTextIn = {
            text: args.textIn[c],
            startY: curLineIn.startY,
            color: curLineIn.color,
        };
        if (args.textPosIn === undefined) {
            flipTextIn.startX = curLineIn.startX;
        } else if (args.textPosIn[c] === 0) {
            flipTextIn.startX = curLineIn.startX - constants.inputLineLengthNew -
                args.clickBoxWidth - constants.textBoxWidth;
        } else {
            flipTextIn.startX = curLineIn.startX;
        }

        passiveStyle(context);
        drawValues(context, curLineIn, args.lineColorsIn[c], constants);
        if (curLineIn.text == "CP") {
            drawFlipFlopText(context, flipTextIn, 0, constants,
                true, true, args.flipFlopType);
        } else {
            drawFlipFlopText(context, flipTextIn, 0, constants, true, true, 1);
        }
        if (args.complementListIn[c] === 1) {
            drawComplementGeneric(context, curLineIn, 1, constants);
            curLineIn.startX -= constants.IEEEComplementDiameter;
        }

        if (args.addClickBox[c] === true) {
            drawClickBoxesNew(context, args, curLineIn, constants);
        }
        drawConnectLine(context, curLineIn, constants.lineWidthIO, true);
        if (args.lineColorsIn[c] === constants.greenLineActiveColor) {
            activeStyle(context, args.state);
            curLineIn.color = constants.currentColor;
            drawConnectLine(context, curLineIn, constants.currentLineWidth);
        }
    }

    // Draw output lines.
    let lineDistanceOut = h / numberOfOut;
    let offFromSidesOut = (h - (numberOfOut - 1) * lineDistanceOut) / 2;
    let curLineOut = {};
    for (let c = 0; c < numberOfOut; c++) {
        curLineOut.startX = args.startX + args.boxWidth;
        curLineOut.endX = args.startX + args.boxWidth + (lineLength);
        curLineOut.startY = args.startY + offFromSidesOut + (c * lineDistanceOut);
        curLineOut.endY = curLineOut.startY;
        curLineOut.color = constants.passiveStateColor;

        let flipTextOut = {
            text: args.outText[c],
            startY: curLineOut.startY,
            color: curLineOut.color
        };
        if (args.textPosOut === undefined) {
            flipTextOut.startX = curLineOut.startX;
        } else if (args.textPosOut[c] === 0) {
            flipTextOut.startX = curLineOut.startX +
                constants.inputLineLengthNew + constants.textBoxWidth;
        } else {
            flipTextOut.startX = curLineOut.startX
        }

        passiveStyle(context);
        drawValues(context, curLineOut, args.lineColorsOut[c], constants);
        let text = args.outText[c];
        if (text.search("'") != -1) {
            let textOnly = text.replace("'", "");
            drawOverlineText2(context, flipTextOut, textOnly,
                constants.overLineLength, constants);
        } else {
            drawFlipFlopText(context, flipTextOut, 1, constants, true, true, 1);
        }
        if (args.complementListOut[c] === 1) {
            drawComplementGeneric(context, curLineOut, 0, constants);
            curLineOut.startX += constants.IEEEComplementDiameter;
        }

        drawConnectLine(context, curLineOut, constants.lineWidthIO, true);
        if (args.lineColorsOut[c] === constants.greenLineActiveColor) {
            activeStyle(context, args.state);
            curLineOut.color = constants.currentColor;
            drawConnectLine(context, curLineOut, constants.currentLineWidth);
        }
    }
    passiveStyle(context);
}

/**
 * Draws output text for given box at given position.
 * @param context context to draw text to
 * @param box IEC or IEEE shaped logic gate box
 * @param constants constants of drawing BOX
 * @param position value are: 0 for above line, 1 for below line (default=0)
 * @param text text to put at canvas
 * @param value value to accommodate text
 * @param overLine if set to false there will be no overline, otherwise it will be drawn over given text
 * @param shift how much to shift the position of text in x direction
 * @param shiftY how much to shift up and down position of text
 */
function genericDrawOutText(context, box, text, value,
                            constants, position = 0, shift = 0, overLine = false, shiftY = 0) {
    let textLineOff = 4 * constants.scaleFactor;
    let togglePosition = (position === 1) ? box.startY +
        (box.boxHeight / 2) + textLineOff : box.startY - textLineOff;
    let xOff = 4 * constants.scaleFactor + shift;
    let yOff = shiftY;
    let textElements = (text.toString().length + value.toString().length) / 2;
    let qText = {
        leftVertexX: box.startX + constants.boxWidth +
        constants.IEEEComplementDiameter + constants.inputLineLength - xOff,
        leftVertexY: togglePosition + yOff,
        boxWidth: constants.textBoxWidth * textElements,
        boxHeight: constants.textBoxHeight,
        color: constants.colors.gray,
        fillStyle: "white",
        lineWidth: constants.textLineWidth,
        textFont: constants.varTextSize + "px Courier"
    };

    let overLineLength = constants.overLineLength;

    if (overLine === true) {
        drawOverlineText(context, qText, text + value, overLineLength, constants.scaleFactor);
    } else {
        drawText(context, qText, text + value, false);
    }
}

function drawSimpleRectangle(context, startX, startY, w, h) {
    context.beginPath();
    context.strokeStyle = "#000000";
    context.rect(startX, startY, w, h);
    context.stroke();
}

function drawOverlineText(context, args, text, lineLength, scaleFactor) {

    drawText(context, args, text, false);
    // Add overline
    let overClose = 12 * scaleFactor;
    // let numOfLetters = text.toString().length;
    // Works but needs corrections
    /*let overXOff = (numOfLetters - (numOfLetters - 1)) * args.boxWidth / numOfLetters;*/
    //let xOff = 5 * scaleFactor;
    let startX = args.leftVertexX;
    let startY = args.leftVertexY + overClose - args.boxHeight;
    let endX = startX + lineLength;
    let endY = startY;
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.stroke();
}


function drawOverlineText2(context, args, text, lineLength, constants) {
    args.text = text;
    drawFlipFlopText(context, args, 1, constants, true, true, 1);
    // Add overline
    let overClose = 1.5 * constants.scaleFactor;
    let xOff = 5 * constants.scaleFactor;
    let startX = args.startX - constants.textBoxWidth + xOff;
    let startY = args.startY - (constants.textBoxHeight / 2) + overClose;
    let endX = startX + lineLength;
    let endY = startY;
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.stroke();
}


function drawText(context, args, text, centered = true, toggleFill = true) {
    let x1 = args.leftVertexX;
    let y1 = args.leftVertexY;
    let w = args.boxWidth;
    let h = args.boxHeight;

    // Clear canvas
    /* context.beginPath();
     context.rect(x1, y1, w, h);
     context.stroke();*/
    context.clearRect(x1, y1, w, h);

    // Set text in the center
    let textX = (centered === true) ? x1 + (w / 2) : x1;
    let textY = (centered === true) ? y1 + (h / 2) : y1;
    context.beginPath();
    context.font = args.textFont;
    context.textAlign = (centered === true) ? "center" : "left";
    context.textBaseline = "middle";
    context.fillStyle = args.fillStyle;
    context.strokeStyle = args.color;
    context.lineWidth = args.lineWidth;
    if (toggleFill === true) {
        context.strokeText(text, textX, textY);
        context.stroke();
    } else {
        context.fillText(text, textX, textY);
        context.stroke();
    }
}

function drawTextBoxOnly(context, args, text) {
    // Input first box
    let w = args.boxWidth;
    let h = args.boxHeight;
    let x1 = args.leftVertexX;
    let y1 = args.leftVertexY;

    context.beginPath();
    context.rect(x1, y1, w, h);
    context.strokeStyle = args.color;
    context.fillStyle = args.fillStyle;
    context.fill();
    context.stroke();


    context.beginPath();
    context.font = args.textFont;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = args.fillStyle;
    context.strokeStyle = args.color;

    // Set text in the center
    let textX = x1 + (w / 2);
    let textY = y1 + (h / 2);

    context.strokeText(text, textX, textY);
    context.stroke();
}

function drawTextBox(context, args, text, hots = 1, constants) {
    // Input first box
    let x1 = args.leftVertexX;
    let y1 = args.leftVertexY;
    let w = args.boxWidth;
    let h = args.boxHeight;

    // Clear canvas
    context.clearRect(x1, y1, w, h);

    context.beginPath();
    context.rect(x1, y1, w, h);
    context.strokeStyle = args.color;
    context.fillStyle = args.fillStyle;
    context.lineWidth = constants.textLineWidth;
    context.fill();
    context.stroke();

    if (hots === 1) {
        saveClickHots(args.state, x1, y1, w, h, args.clickId);
    }

    context.beginPath();
    context.font = args.textFont;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = args.fillStyle;
    context.strokeStyle = args.color;
    context.lineWidth = constants.textLineWidth;

    // Set text in the center
    let textX = x1 + (w / 2);
    let textY = y1 + (h / 2);

    context.strokeText(text, textX, textY);
    //context.fillText(text, textX, textY);
    context.stroke();
}

function drawArrow(context, curvedLine, width, side = 0) {

    let startPointX = (side === 0) ? curvedLine.startX : curvedLine.endPointX;
    let startPointY = (side === 0) ? curvedLine.startY : curvedLine.endPointY;
    let controlPointX = curvedLine.controlPointX;
    let controlPointY = curvedLine.controlPointY;
    let arrowWidth = width;

    context.beginPath();
    context.strokeStyle = curvedLine.color;
    let arrowAngle = Math.atan2(controlPointX - startPointX,
            controlPointY - startPointY) + Math.PI;
    context.moveTo(startPointX - (arrowWidth * Math.sin(arrowAngle - Math.PI / 6)),
        startPointY - (arrowWidth * Math.cos(arrowAngle - Math.PI / 6)));
    context.lineTo(startPointX, startPointY);
    context.lineTo(startPointX - (arrowWidth * Math.sin(arrowAngle + Math.PI / 6)),
        startPointY - (arrowWidth * Math.cos(arrowAngle + Math.PI / 6)));
    context.stroke();
    context.closePath();
}

/**
 * Draws simple triangle in horizontal direction.
 * @param context context to draw lines to
 * @param args arguments described below,
 *          xOff x coordinate for offset from origin of this context
 *          y coordinate for offset from origin of this context
 *          sideLength length of base of a triangle
 *          height height of a triangle
 */
function drawTriangle(context, args) {
    let endY = args.startY + args.baseLength;
    let middle = args.startY + args.baseLength / 2;
    let height = args.startX + args.h;

    // Draw triangle
    context.beginPath();
    context.strokeStyle = args.color;
    context.lineWidth = args.lineWidth;
    context.moveTo(args.startX, args.startY);
    context.lineTo(args.startX, endY);
    context.lineTo(height, middle);
    context.closePath();
    context.stroke();
}

function drawCurvedConnectLine(context, args) {
    context.beginPath();
    context.strokeStyle = args.color;
    context.lineWidth = args.lineWidth;
    context.moveTo(args.startX, args.startY);
    context.quadraticCurveTo(args.controlPointX, args.controlPointY, args.endPointX, args.endPointY);
    context.stroke();
}

/**
 * Draws simple circle for making little "Complement" figure on each inverter.
 * @param context context to draw lines to
 * @param args arguments are as follows,
 *          startX x coordinate for circle center
 *          startY y coordinate for circle center
 *          radius radius of a circle
 *          startAngle angle at which circle is drawn
 *          endAngle representing value for whole circle (basically 360)
 */
function drawCircle(context, args) {
    context.beginPath();
    context.strokeStyle = args.color;
    context.fillStyle = args.fillStyle;
    context.lineWidth = args.lineWidth;
    context.arc(args.centerX, args.centerY, args.radius, args.startAngle, args.endAngle, args.rotation);
    context.fill();
    context.stroke();
}

/**
 * Function which draws Box on given context.
 * @param context context in which box is drawn
 * @param params parameters which are used for drawing box
 */
function drawBox(context, params) {
    context.beginPath();
    context.rect(params.boxXOff, params.boxYOff,
        params.boxWidth, params.boxHeight);
    context.fillStyle = params.getColor;
    context.fill();
    context.strokeStyle = globalConstants.standardLineColor;
    context.stroke();
}

function drawFlipFlopBox(context, args, constants) {
    context.beginPath();
    context.rect(args.startX, args.startY,
        args.boxWidth, args.boxHeight);
    context.fillStyle = args.fillStyle;
    context.fill();
    context.strokeStyle = constants.colors.black;
    context.stroke();
}

/**
 * Function which sets common options which are used for creating classes.
 * @param boxXOff horizontal offset from canvas to box
 * @param boxYOff vertical offset from canvas to box
 * @param boxWidth box width
 * @param boxHeight box height
 * @param lineWidth width of connecting lines for boxes
 * @param lineLength length for input and output lines
 * @param inputLineDistance distance between input lines
 * @param clickBoxWidth width of clickBox (cBox)
 * @param clickBoxHeight height of clickBox (cBox)
 * @param complementList list of complemented signals
 * @param colors colors are represented with dictionary: {boxColor, lineColor, cBoxColor}
 * @returns {{}} dictionary of input parameters
 */
function setOptions(boxXOff, boxYOff, boxWidth, boxHeight, lineWidth, lineLength,
                    inputLineDistance, clickBoxWidth, clickBoxHeight, complementList,
                    colors) {
    // Set dimensions and styles...
    let opts = {};

    // Main box dimensions
    opts.boxXOff = boxXOff;
    opts.boxYOff = boxYOff;
    opts.boxWidth = boxWidth;
    opts.boxHeight = boxHeight;

    // Line dimensions
    opts.lineWidth = lineWidth;
    opts.lineLength = lineLength;
    opts.inputLineDistance = inputLineDistance; // generates offset 50

    // Click box dimensions
    opts.clickBoxWidth = clickBoxWidth;
    opts.clickBoxHeight = clickBoxHeight;

    // Complement lists
    opts.complementList = complementList;

    // Styles
    opts.boxColor = colors.boxColor;
    opts.lineColor = colors.lineColor;
    opts.clickBoxColor = colors.cBoxColor;

    return opts;
}

/**
 * Function sets background rectangle box in given canvas.
 * @param context context for drawing frame
 * @param width of context which frame goes to
 * @param height of context which frame goes to
 */
function setBackgroundFrame(context, width, height) {
    context.beginPath();
    context.strokeStyle = "#000000";
    context.strokeWidth = globalConstants.backgroundFrameWidth;
    context.rect(0, 0, width, height);
    context.stroke();
}


function drawComplementIndicator(context, xStart, yStart, constants) {
    let params = {
        centerX: xStart,
        centerY: yStart + constants.schemaComplementRadius,
        radius: constants.schemaComplementRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        color: constants.complementColor,
        fillStyle: "white",
        lineWidth: constants.complementLineWidth,
        rotation: true
    };
    drawCircle(context, params);
}

function drawEdgeIndicator(context, xStart, yStart,
                           constants = globalConstants, orientation = 0) {
    let side = 8 * constants.scaleFactor;
    let height = 7 * constants.scaleFactor;
    let moveX, moveY;
    let x1, y1, x2, y2;
    if (orientation === 0) {
        moveX = xStart - side;
        moveY = yStart;
        x1 = xStart;
        y1 = yStart - height;
        x2 = xStart + side;
        y2 = yStart;

    } else {
        moveX = xStart;
        moveY = yStart - side;
        x1 = xStart + height;
        y1 = yStart;
        x2 = xStart;
        y2 = yStart + side;
    }

    context.beginPath();
    context.moveTo(moveX, moveY);
    context.lineTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
}

function drawCPBelow(context, box, constants, typeSpec = 1) {
    // Calc common values
    let xStart = box.startX + (box.boxWidth / 2);
    let yStart = box.startY + box.boxHeight;
    let yPadInner = 15 * constants.scaleFactor;
    if (typeSpec === 0) {
        drawComplementIndicator(context, xStart, yStart, constants);
        yStart += constants.schemaComplementRadius * 2;
        yPadInner += constants.schemaComplementRadius * 2;
    } else if (typeSpec === 2) {
        drawComplementIndicator(context, xStart, yStart, constants);
        drawEdgeIndicator(context, xStart, yStart, constants);
        yStart += constants.schemaComplementRadius * 2;
        yPadInner += constants.schemaComplementRadius * 2;
    } else if (typeSpec === 3) {
        drawEdgeIndicator(context, xStart, yStart, constants);
    }

    let xOff = 8 * constants.scaleFactor;
    let yOff = 3 * constants.scaleFactor;
    let value = (box.state.CP === 1) ? 1 : 0;
    let textBox = {
        leftVertexX: xStart - xOff,
        leftVertexY: yStart + yOff,
        boxWidth: constants.valuesBoxWidth,
        boxHeight: constants.valuesBoxHeight,
        color: constants.passiveStateColor,
        textFont: constants.fontSizeIOValues + constants.standardFont,
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };
    drawText(context, textBox, value, true);
    // Add CP text
    let staticCP = "CP";

    // Text Style:
    context.font = constants.flipFontSizeInner + constants.flipFont;
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = box.fillStyle;
    context.strokeStyle = "black";
    context.lineWidth = constants.textLineWidth;

    context.beginPath();
    // Inner CP
    let textX = xStart;
    let textY = yStart - yPadInner;

    context.beginPath();
    //context.fillText(staticCP, textX, textY);
    context.strokeText(staticCP, textX, textY);

}

/**
 * Draws CP signal for Flip-Flop
 * @param context context to draw CP signal
 * @param params parameters for calculating position of draw-ed objects
 * @param constants all library constants at default values
 * @param typeSpec if 1 set a high level flip flop, if 0 low level, default=1
 */
function drawCP(context, params, constants, typeSpec = 1) {
    // Calc common values
    let xStart = params.boxXOff + (params.boxWidth / 2);
    let yStart = params.boxYOff + params.boxHeight;
    let yPadInner = 25;
    if (typeSpec === 0) {
        drawComplementIndicator(context, xStart, yStart, constants);
        yStart += constants.schemaComplementRadius * 2;
        yPadInner += constants.schemaComplementRadius * 2;
    } else if (typeSpec === 2) {
        drawComplementIndicator(context, xStart, yStart, constants);
        drawEdgeIndicator(context, xStart, yStart, constants);
        yStart += constants.schemaComplementRadius * 2;
        yPadInner += constants.schemaComplementRadius * 2;
    } else if (typeSpec === 3) {
        drawEdgeIndicator(context, xStart, yStart, constants);
    }

    // Draw CP signal line
    let lineLengthY = 30;
    let lineLengthX = -50;
    let xEnd = xStart;
    let yEnd = yStart + lineLengthY;
    let x2End = xEnd + lineLengthX;
    let y2End = yEnd;
    context.beginPath();
    context.strokeStyle = params.getColorCp;
    context.lineWidth = constants.CPLineWidth;
    context.fill();
    context.moveTo(xStart, yStart);
    context.lineTo(xEnd, yEnd);
    context.lineTo(x2End, y2End);

    context.stroke();

    // Add CP text
    let value = (params.getColorCp.localeCompare("red") != 0) ? 0 : 1;
    let CP = "CP" + " = " + value.toString();
    let staticCP = "CP";
    let xPad = 25;
    let textX = xStart + lineLengthX - xPad;
    let textY = yStart + lineLengthY;

    // Text Style:
    context.font = "24px Courier";
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = params.getColor;
    context.strokeStyle = "black";
    context.lineWidth = constants.textLineWidth;

    context.beginPath();
    //context.fillText(CP, textX, textY);
    context.strokeText(CP, textX - 30, textY);

    // Inner CP
    textX = xStart;
    textY = yStart - yPadInner;

    context.beginPath();
    //context.fillText(staticCP, textX, textY);
    context.strokeText(staticCP, textX, textY);

}

/**
 * function which draws text on boxes.
 * @param context context in which text is drawn
 * @param params parameters used to determine text position
 * @param constants constants used for IEEE specific values
 */
function drawTextForBox(context, params, constants) {
    // Draw "A" input
    let inputLinesYOff = (params.boxHeight - params.inputLineDistance) / 2;
    let x = params.boxXOff + 25;
    let y = params.boxYOff + inputLinesYOff;

    context.beginPath();
    context.font = "35px Courier";
    context.lineWidth = constants.textLineWidth;
    context.strokeText(String.fromCharCode(65), x, y);
    context.stroke();

    // Draw "B" input
    let x2 = x;
    let y2 = y + params.inputLineDistance;

    context.beginPath();
    context.font = "35px Courier";
    context.lineWidth = constants.textLineWidth;
    context.strokeText(String.fromCharCode(66), x2, y2);
    context.stroke();

    // Draw "F" output
    let x3 = params.boxXOff + params.boxWidth - 25;
    let y3 = params.boxYOff + params.boxHeight / 2;

    context.beginPath();
    context.font = "35px Courier";
    context.lineWidth = constants.textLineWidth;
    context.strokeText(String.fromCharCode(102), x3, y3);
    context.stroke();
}

/**
 * Saves click hots for simple vertical and horizontal lines.
 * @param context used only for representing rectangles of clicks (remove)
 * @param state state where the hots are saved
 * @param line line to make clickable (must define startX/Y and endX/Y
 * @param offset offset for click rectangle creation
 * @param lineType set 1 for vertical or 0 for horizontal, default=0
 */
function saveConnectLineClickHots(context, state, line, offset, lineType = 0) {
    let startX = (line.startX > line.endX) ? line.endX : line.startX;
    let startY = (line.startY > line.endY) ? line.endY : line.startY;
    let w = Math.abs(line.endX - line.startX);
    let h = Math.abs(line.endY - line.startY);
    if (lineType === 1) {
        saveClickHots(state, startX - offset, startY,
            2 * offset, h);
    } else if (lineType === 0) {
        saveClickHots(state, startX, startY - offset,
            w, 2 * offset);
    }
}

/**
 * Draws little bit of lines from one end of inverter to another inverter.
 * @param context context to draw lines to
 * @param args
 *          line color
 *          x coordinate of starting point where returning line is drawn
 *          y coordinate of starting point where returning line is drawn
 * @param constants constants for IEEE parameters
 * @param width defines different line width, for current it might be bigger
 *
 */
function drawReturnConnector(context, args, constants,
                             width = globalConstants.connectLinesWidth) {

    let startX = args.startX;
    let startY = args.startY;
    let endX = args.startX + constants.outLineLength;
    let endY = args.startY;

    let clickHotsOffset = 10;
    if (args.saveClickHots === true) {
        saveClickHots(args.state, startX, startY - clickHotsOffset,
            constants.outLineLength, 2 * clickHotsOffset);
    }

    context.beginPath();
    context.strokeStyle = args.color;
    context.lineWidth = width;
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.stroke();

    startX = endX;
    startY = endY;
    let lengthUp = constants.verticalUpLength;
    endX = startX;
    endY = startY - lengthUp;

    let returnLength = args.sketchLength + 2 * constants.outLineLength;
    let endX2 = endX - returnLength;
    let endY2 = endY;

    let endAtY = args.endAtY;

    if (args.saveClickHots === true) {
        saveClickHots(args.state, endX2, endY2 - clickHotsOffset,
            returnLength, 2 * clickHotsOffset);
        saveClickHots(args.state, endX2, endAtY - clickHotsOffset,
            constants.outLineLength + constants.inputLineLength, 2 * clickHotsOffset);
        saveClickHots(args.state, endX2 - clickHotsOffset,
            endY2, 2 * clickHotsOffset, endAtY - endY2);
        saveClickHots(args.state, endX - clickHotsOffset, endY,
            2 * clickHotsOffset, startY - endY);
    }

    context.beginPath();
    context.strokeStyle = args.color;
    context.lineWidth = width;
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.lineTo(endX2, endY2);
    context.lineTo(endX2, endAtY);
    context.lineTo(endX2 + constants.outLineLength, endAtY);
    context.stroke();
}

function drawIOLines(context, args, constants, lineLengths =
    [constants.inputLineLength,
        constants.inputLineLength,
        constants.inputLineLength]) {

    let params = {
        leftVertexX: args.startX,
        leftVertexY: args.startY,
        boxWidth: args.boxWidth,
        boxHeight: args.boxHeight,
        color: args.color,
        fillStyle: args.fillStyle,
        textFont: args.textFont,
        complementList: args.complementList,
        lineColorsIO: args.lineColorsIO,
    };

    /* Draw Input Output Lines IO. */
    let linesAND = new IEEELineHandler(params.leftVertexX, params.leftVertexY,
        params.boxWidth, params.boxHeight,
        params.complementList, constants.IEEEComplementDiameter,
        constants.lineWidthIO, constants.lineLength,
        constants.inputLineDistance);

    linesAND.currentFlow = undefined;
    let canvasClear = true;
    passiveStyle(context);
    drawLines(context, linesAND, constants,
        args.state, args.clickHotsIO, canvasClear, lineLengths);
    linesAND.colors = [linesAND.colorIn1, linesAND.colorIn2,
        linesAND.colorOut];

    linesAND.currentFlow = [
        (params.lineColorsIO[0] === constants.greenLineActiveColor),
        (params.lineColorsIO[1] === constants.greenLineActiveColor),
        (params.lineColorsIO[2] === constants.greenLineActiveColor)
    ];
    linesAND.colorIn1 = (linesAND.currentFlow[0] === true) ? constants.greenLineActiveColor : linesAND.colorIn1;
    linesAND.colorIn2 = (linesAND.currentFlow[1] === true) ? constants.greenLineActiveColor : linesAND.colorIn2;
    linesAND.colorOut = (linesAND.currentFlow[2] === true) ? constants.greenLineActiveColor : linesAND.colorOut;

    linesAND.lineWidth = constants.currentLineWidth;
    activeStyle(context, args.state);
    drawLines(context, linesAND, constants, args.state, args.clickHotsIO,
        false, lineLengths);
    passiveStyle(context);
}

function drawRectangularBox(context, args, gateName, constants) {
    let params = {
        leftVertexX: args.startX,
        leftVertexY: args.startY,
        boxWidth: args.boxWidth,
        boxHeight: args.boxHeight,
        color: args.color,
        fillStyle: args.fillStyle,
        textFont: args.textFont,
        complementList: args.complementList,
        lineColorsIO: args.lineColorsIO
    };

    /* Set hots to 0 to exclude click checks. */

    drawTextBox(context, params, gateName, 0, constants);
    drawSimpleRectangle(context, args.startX, args.startY, args.boxWidth, args.boxHeight);
}

/**
 * ChangeLog, changed to clear it own canvas
 * @param context
 * @param args
 * @param width
 * @param clearCanvas clears the canvas if set to true
 * @param direction if set to anything else, it will be in direction
 * of line
 */
function drawConnectLine(context, args, width, clearCanvas = false, direction = true) {
    // Connect line
    if (clearCanvas === true) {
        clearLineCanvas(context, args, globalConstants.tubeLineWidth);
    }
    let startX = args.startX;
    let startY = args.startY;
    let endX = args.endX;
    let endY = args.endY;

    if ((args.startY === args.endY) && (direction === true)) {
        startX = (args.startX > args.endX) ? args.endX : args.startX;
        endX = (args.endX === startX) ? args.startX : args.endX;
    }
    if ((args.startX === args.endX) && (direction === true)) {
        startY = (args.startY > args.endY) ? args.endY : args.startY;
        endY = (args.endY === startY) ? args.startY : args.endY;
    }
    context.beginPath();
    context.strokeStyle = args.color;
    context.lineWidth = width;
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.stroke();
}

function drawAND(context, args) {

    let AND = {};
    AND.color = args.color;
    AND.arc = {
        centerX: args.startX + args.lineLength,
        centerY: args.startY + args.arcRadius,
        radius: args.arcRadius,
        startAngle: toRadians(90),
        endAngle: toRadians(-90),
        rotation: true,
        color: AND.color,
        fillStyle: args.fillStyle,
        lineWidth: args.arcLineWidth
    };

    AND.connectLine1 = {
        startX: AND.arc.centerX,
        startY: AND.arc.centerY - AND.arc.radius,
        endX: AND.arc.centerX - args.lineLength,
        endY: AND.arc.centerY - AND.arc.radius,
        color: AND.color
    };
    AND.connectLine2 = {
        startX: AND.connectLine1.startX,
        startY: AND.connectLine1.startY + (2 * AND.arc.radius),
        endX: AND.connectLine1.endX,
        endY: AND.connectLine1.startY + (2 * AND.arc.radius),
        color: AND.color
    };
    AND.connectLine3 = {
        startX: AND.connectLine2.endX,
        startY: AND.arc.centerY - AND.arc.radius,
        endX: AND.connectLine2.endX,
        endY: AND.connectLine2.endY,
        color: AND.color
    };
    /* Last argument is standard connect line width */
    drawCircle(context, AND.arc);
    drawConnectLine(context, AND.connectLine1, args.connectLineWidth);
    drawConnectLine(context, AND.connectLine2, args.connectLineWidth);
    drawConnectLine(context, AND.connectLine3, args.connectLineWidth);
}

function drawClickBox(context, params, state, states) {
    // Input first box
    let x1 = params.leftVertexX;
    let y1 = params.leftVertexY;
    let w = params.clickBoxWidth;
    let h = params.clickBoxHeight;

    context.beginPath();
    context.rect(x1, y1, w, h);
    context.fillStyle = params.fillStyle;
    context.strokeStyle = params.color;
    context.fill();
    saveClickHots(state, x1, y1, w, h, states);

    context.font = "24px Courier";
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = params.color;
    context.strokeStyle = "black";
    context.lineWidth = params.lineWidth;

    // Set text in the center
    let textX = x1 + (w / 2);
    let textY = y1 + (h / 2);

    context.strokeText(state.userInput[states], textX, textY);
    context.stroke();
}

/**
 *
 * @param context context to draw IEEE boxes to
 * @param typeOfGate gate type
 * @param args
 *          start X and Y top left vertex, color and fill style,
 *          complement list and lineColorsIO
 * @param constants constants for IEEE parameters
 */
function drawIEEEBoxes(context, typeOfGate, args, constants) {
    let gates = new LogicGates();
    switch (typeOfGate) {
        case gates.AND:
        case gates.NAND:
            let params = {
                startX: args.startX,
                startY: args.startY,
                lineLength: constants.lineLength,
                arcRadius: constants.IEEEArcRadius,
                color: args.color,
                fillStyle: args.fillStyle,
                connectLineWidth: constants.connectLinesWidth,
                arcLineWidth: constants.connectLinesWidth,
                complementList: args.complementList,
                lineColorsIO: args.lineColorsIO
            };
            let linesAND = new IEEELineHandler(params.startX, params.startY,
                constants.NANDBoxWidth, constants.NANDBoxHeight,
                params.complementList, constants.IEEEComplementDiameter,
                constants.lineWidthIO, constants.lineLength,
                constants.inputLineDistance, params.lineColorsIO);

            drawAND(context, params);
            drawLines(context, linesAND, constants);
            break;
        case gates.OR:
        case gates.NOR:
            // TO DO
            break;
        case gates.XOR:
        case gates.XNOR:
            // TO DO
            //drawXOR(context, args);
            break;
    }
}

function passiveStyle(context) {
    context.strokeStyle = globalConstants.passiveStateColor;
    context.lineWidth = globalConstants.tubeLineWidth;
    context.setLineDash([]);
}

function activeStyle(context, state) {
    context.setLineDash(state.animationPattern);
    context.lineDashOffset = -state.animationOffset;
}

// Simulations

function drawAnimated(context, drawSketch, passiveStyle, condition, activeStyle) {

    //context.beginPath();
    passiveStyle();
    drawSketch();
    //context.stroke();

    if (condition === globalConstants.greenLineActiveColor) {
        //context.beginPath();
        activeStyle();
        drawSketch();
        //context.stroke();
        passiveStyle();
    }
}


// Diagrams

function drawDiagramGeneric(context, state, x, y, cDistance, cRadius, constants) {
    let stayInZero;
    let stayInOne;
    let set, reset;

    if (state.Q === 0 && state.QnNext === 0) {
        stayInZero = "red";
    } else {
        stayInZero = "black";
    }
    if (state.Q === 0 && state.QnNext === 1) {
        set = "red";
    } else {
        set = "black";
    }
    if (state.Q === 1 && state.QnNext === 0) {
        reset = "red";
    } else {
        reset = "black";
    }
    if (state.Q === 1 && state.QnNext === 1) {
        stayInOne = "red";
    } else {
        stayInOne = "black";
    }

    let stateZero = (state.Q === 1)
        ? constants.passiveStateColor : constants.activeStateColor;
    let stateOne = (state.Q === 0)
        ? constants.passiveStateColor : constants.activeStateColor;


    let xOff = x;
    let yOff = y;
    let state0 = {
        centerX: xOff,
        centerY: yOff,
        radius: cRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        rotation: false,
        color: stateZero,
        fillStyle: "white",
        lineWidth: constants.connectLinesWidth + 2
    };

    let circleDistance = cDistance;
    let state1 = {
        centerX: xOff + circleDistance,
        centerY: yOff,
        radius: cRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        rotation: false,
        color: stateOne,
        fillStyle: "white",
        lineWidth: constants.connectLinesWidth + 2
    };
    drawCircle(context, state0);
    drawCircle(context, state1);

    // Draw Text for circle states
    let textS0 = {
        leftVertexX: (state0.centerX) - (constants.textBoxWidth / 2),
        leftVertexY: (state0.centerY) - (constants.textBoxHeight / 2),
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: (constants.varTextSize + 5) + "px Courier",
        color: constants.standardLineColor,
        lineWidth: constants.textLineWidth
    };
    let textS1 = {
        leftVertexX: (state1.centerX) - (constants.textBoxWidth / 2),
        leftVertexY: (state1.centerY) - (constants.textBoxHeight / 2),
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: (constants.varTextSize + 5) + "px Courier",
        color: constants.standardLineColor,
        lineWidth: constants.textLineWidth
    };

    drawText(context, textS0, "0");
    drawText(context, textS1, "1");

    // Draw connect lines
    let yLineOff = cRadius - Math.PI - 2;
    let distX = Math.sqrt(Math.pow(cRadius, 2) -
        Math.pow(yLineOff, 2));

    let lineSet = {
        startX: xOff + distX,
        startY: yOff - yLineOff,
        controlPointX: xOff + (circleDistance - 2 * distX) / 2,
        controlPointY: yOff - cRadius * 2,
        endPointX: xOff - distX + circleDistance - 5,
        endPointY: yOff - yLineOff,
        color: set,
        lineWidth: constants.connectLinesWidth

    };

    let lineReset = {
        startX: xOff + distX,
        startY: yOff + yLineOff,
        controlPointX: xOff + (circleDistance - 2 * distX) / 2,
        controlPointY: yOff + cRadius * 2,
        endPointX: xOff - distX + circleDistance,
        endPointY: yOff + yLineOff,
        color: reset,
        lineWidth: constants.connectLinesWidth
    };

    let yCenterOff = 45;
    let lineStayZero = {
        startX: xOff - distX,
        startY: yOff - yLineOff,
        controlPointX: xOff - 3 * cRadius,
        controlPointY: yOff - yCenterOff,
        endPointX: xOff - cRadius,
        endPointY: yOff,
        color: stayInZero,
        lineWidth: constants.connectLinesWidth
    };


    let lineStayOne = {
        startX: xOff + circleDistance + distX,
        startY: yOff - yLineOff,
        controlPointX: xOff + circleDistance + 3 * cRadius,
        controlPointY: yOff - yCenterOff,
        endPointX: xOff + circleDistance + cRadius,
        endPointY: yOff,
        color: stayInOne,
        lineWidth: constants.connectLinesWidth
    };

    let textYOff = 20;
    let textForLineSet = {
        leftVertexX: lineSet.controlPointX,
        leftVertexY: lineSet.controlPointY - textYOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let textYDown = 5;
    let textForLineReset = {
        leftVertexX: lineReset.controlPointX,
        leftVertexY: lineReset.controlPointY - textYDown,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let smallInc = 17 * constants.scaleFactor;
    let twoLineOff = 20 * constants.scaleFactor;
    let textForLineStay0 = {
        leftVertexX: lineStayZero.controlPointX + smallInc,
        leftVertexY: lineStayZero.controlPointY - smallInc - twoLineOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    /** Constant is used for leftVertexX because of zooming makes change to it and it must be fixed (y is only moving axis). */
    let textForLineStay1 = {
        leftVertexX: 466,
        leftVertexY: lineStayOne.controlPointY - smallInc - twoLineOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let arrowWidth = 15;
    let lineHeight = 30;
    drawCurvedConnectLine(context, lineSet);
    drawCurvedConnectLine(context, lineReset);
    drawCurvedConnectLine(context, lineStayZero);
    drawCurvedConnectLine(context, lineStayOne);
    drawArrow(context, lineSet, arrowWidth, 1);
    drawArrow(context, lineReset, arrowWidth, 0);
    drawArrow(context, lineStayZero, arrowWidth, 1);
    drawArrow(context, lineStayOne, arrowWidth, 1);

    // Draw text on lines
    writeMultilineText2(context, textForLineSet, lineHeight, "(1, 0)");
    writeMultilineText2(context, textForLineReset, lineHeight, "(0, 1)");
    writeMultilineText2(context, textForLineStay0, lineHeight, "(0, 0)\n(0, 1)");
    writeMultilineText2(context, textForLineStay1, lineHeight, "(0, 0)\n(1, 0)");
}


function drawShapeGeneric(context, state, constants, rh, toff, loff, cw) {
    context.beginPath();
    context.rect(loff, toff, state.elems[0].length * cw, rh);
    context.fillStyle = '#cccccc';
    context.fill();

    context.beginPath();
    context.rect(loff, toff + (state.currentRow) * rh,
        state.elems[0].length * cw, rh);
    context.fillStyle = "#0d9679";
    context.fill();

    context.beginPath();
    context.lineWidth = 1;
    for (let y = 0; y <= state.elems.length + 1; y++) {
        context.moveTo(loff, y * rh + toff);
        context.lineTo(loff + state.elems[0].length * cw, y * rh + toff);
    }
    for (let x = 0; x <= state.elems[0].length; x++) {
        context.moveTo(loff + x * cw, toff);
        context.lineTo(loff + x * cw, (state.elems.length + 1) * rh + toff);
    }
    context.strokeStyle = constants.standardLineColor;
    context.stroke();

}


function drawShapeGeneric2(context, state, constants, rh, toff, loff, cw) {
    context.beginPath();
    context.rect(loff, toff, state.elems[0].length * cw, rh);
    context.fillStyle = '#cccccc';
    context.fill();

    context.beginPath();
    context.rect(loff, toff + (state.currentRow) * rh,
        state.elems[0].length * cw, rh);
    context.fillStyle = "#0d9679";
    context.fill();

    context.beginPath();
    context.lineWidth = 1;
    for (let y = 0; y <= state.elems.length; y++) {
        context.moveTo(loff, y * rh + toff);
        context.lineTo(loff + state.elems[0].length * cw, y * rh + toff);
    }
    for (let x = 0; x <= state.elems[0].length; x++) {
        context.moveTo(loff + x * cw, toff);
        context.lineTo(loff + x * cw, (state.elems.length) * rh + toff);
    }
    context.strokeStyle = constants.standardLineColor;
    context.stroke();

}


function drawTableGeneric(context, state, constants, letters) {
    let rh = 30;
    let toff = 15;
    let loff = 550;
    let cw = 30;
    drawShapeGeneric(context, state, constants, rh, toff, loff, cw);

    let fontSize = 18;
    let lettersG = letters;
    context.beginPath();
    context.font = fontSize + "px Courier";
    context.strokeStyle = constants.standardLineColor;
    for (let y = 0; y < state.elems.length + 1; y++) {
        for (let x = 0; x < state.elems[0].length; x++) {
            let txt = y == 0 ? (x < state.elems[0].length - 1 ?
                lettersG[x] :
                "f") :
                '' + state.elems[y - 1][x];
            if (y > 0 && x == state.elems[0].length - 1) {
                txt = state.elems[y - 1][3];
            }
            context.strokeText(txt, loff + (x + 0.5 ) * cw,
                (y + 0.6) * rh + toff - (rh - fontSize) / 4);
        }
    }
}

function drawComplementLine(context, args) {
    /* let overClose = 1.5 * 1.75;
     let xOff = 5 * 1.75;
     let startX = args.startX - constants.textBoxWidth + xOff;
     let startY = args.startY - (constants.textBoxHeight / 2) + overClose;
     let endX = startX + lineLength;
     let endY = startY;*/
    context.beginPath();
    context.moveTo(args.startX, args.startY);
    context.lineTo(args.endX, args.endY);
    context.stroke();
}
function drawTableGeneric2(context, x, y, rowHeight, columnWidth, fSize, state, constants) {
    let rh = rowHeight;
    let toff = y;
    let loff = x;
    let cw = columnWidth;
    drawShapeGeneric2(context, state, constants, rh, toff, loff, cw);

    let fontSize = fSize;
    let headers = state.elems[0];
    context.beginPath();
    context.font = fontSize + "px Courier";
    context.strokeStyle = constants.standardLineColor;
    for (let y = 0; y < state.elems.length; y++) {
        for (let x = 0; x < state.elems[0].length; x++) {
            let text = state.elems[y][x];
            if (state.elems[y][x].toString().search("'") != -1) {
                text = text.replace("'", "");
                let xOff = -10;
                let yOff = -13;
                let tableTextLine = {
                    startX: loff + (x + 0.5) * cw + xOff,
                    startY: (y + 0.6) * rh + toff - (rh - fontSize) / 4 + yOff,
                    color: constants.passiveStateColor
                };
                let lineLength = 14;
                tableTextLine.endX = tableTextLine.startX + lineLength;
                tableTextLine.endY = tableTextLine.startY;
                context.save();
                drawComplementLine(context, tableTextLine);
                context.restore();
            }
            let txt = (y === 0) ? headers[x] : "" + text;
            context.strokeText(txt, loff + (x + 0.5) * cw,
                (y + 0.6) * rh + toff - (rh - fontSize) / 4);
        }
    }
}


function setNextStateGeneric(state) {
    // One more skips the HEADER row in table, which is at row 0.
    let currentState = [state.Q, state.userInput[0], state.userInput[1]];
    state.currentRow = binaryToDecimal(currentState) + 1;
    state.nextState = state.elems[state.currentRow - 1][3];
}

function setCurrentStateGeneric(state) {
    // One more skips the HEADER row in table, which is at row 0.
    let currentState = [state.Q, state.userInput[0], state.userInput[1]];
    state.currentRow = binaryToDecimal(currentState) + 1;
    if (state.currentRow >= 8) {
        state.currentRow = 1;
    }
    state.QnNext = state.elems[state.currentRow - 1][3];
}

// Generic functions for drawing SR flip flop

function drawReturnConnectors(context, nand1, nand2, constants, actualDistance = 50, lineOff = 75) {
    // Constants
    let q = (nand1.lineColorsIO[2] === constants.greenLineActiveColor) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let nQ = (nand1.lineColorsIO[1] === constants.greenLineActiveColor) ?
        constants.greenLineActiveColor : constants.passiveStateColor;

    let inputLinesYOff = (constants.universalBoxHeight -
        constants.inputLineDistance) / 2;

    // Draw return line1
    let downYOff = actualDistance;
    let downLine = {};

    downLine.startX = nand1.startX + constants.universalBoxWidth +
        constants.IEEEComplementDiameter + constants.inputLineLength;
    downLine.startY = nand1.startY + constants.universalBoxHeight / 2;
    downLine.endX = downLine.startX;
    downLine.endY = downLine.startY + downYOff;
    downLine.color = constants.passiveStateColor;

    let retLineOff = lineOff;
    let retLine1 = {
        startX: downLine.endX,
        startY: downLine.endY,
        endX: nand1.startX - constants.inputLineLength,
        endY: nand1.startY + constants.universalBoxHeight + retLineOff,
        color: constants.passiveStateColor
    };

    passiveStyle(context);
    drawConnectLine(context, downLine, constants.tubeLineWidth, true);
    if (q === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        downLine.color = constants.currentColor;
        drawConnectLine(context, downLine, constants.currentLineWidth);
    }
    passiveStyle(context);

    drawConnectLine(context, retLine1, constants.tubeLineWidth, true);
    if (q === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        retLine1.color = constants.currentColor;
        drawConnectLine(context, retLine1, constants.currentLineWidth);
    }
    passiveStyle(context);

    let retLineConnect = {
        startX: retLine1.endX,
        startY: retLine1.endY,
        endX: retLine1.endX,
        endY: nand2.startY + inputLinesYOff,
        color: constants.passiveStateColor
    };

    drawConnectLine(context, retLineConnect, constants.tubeLineWidth, true);
    if (q === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        retLineConnect.color = constants.currentColor;
        drawConnectLine(context, retLineConnect, constants.currentLineWidth);
    }
    passiveStyle(context);

    let upLine = {};
    let upYOff = downYOff;
    upLine.startX = nand2.startX + constants.universalBoxWidth +
        constants.IEEEComplementDiameter + constants.inputLineLength;
    upLine.startY = nand2.startY + constants.universalBoxHeight / 2;
    upLine.endX = upLine.startX;
    upLine.endY = upLine.startY - upYOff;
    upLine.color = constants.passiveStateColor;

    let retLine2 = {
        startX: upLine.endX,
        startY: upLine.endY,
        endX: nand2.startX - constants.inputLineLength,
        endY: nand2.startY - retLineOff,
        color: constants.passiveStateColor
    };

    let retLineConnect2 = {
        startX: retLine2.endX,
        startY: retLine2.endY,
        endX: retLine2.endX,
        endY: nand1.startY + constants.universalBoxHeight - inputLinesYOff,
        color: constants.passiveStateColor
    };

    drawConnectLine(context, upLine, constants.tubeLineWidth, true);
    if (nQ === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        upLine.color = constants.currentColor;
        drawConnectLine(context, upLine, constants.currentLineWidth, false, false);
    }
    passiveStyle(context);

    drawConnectLine(context, retLine2, constants.tubeLineWidth);
    if (nQ === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        retLine2.color = constants.currentColor;
        drawConnectLine(context, retLine2, constants.currentLineWidth);
    }
    passiveStyle(context);

    drawConnectLine(context, retLineConnect2, constants.tubeLineWidth, true);
    if (nQ === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        retLineConnect2.color = constants.currentColor;
        drawConnectLine(context, retLineConnect2, constants.currentLineWidth, false, false);
    }
    passiveStyle(context);

    let connectDotQ =
        {
            centerX: downLine.startX,
            centerY: downLine.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

    let connectDotNQ =
        {
            centerX: upLine.startX,
            centerY: upLine.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };
    drawCircle(context, connectDotQ);
    drawCircle(context, connectDotNQ);


    let outQLine = {
        startX: connectDotQ.centerX + connectDotQ.radius,
        startY: connectDotQ.centerY,
        color: constants.passiveStateColor
    };
    outQLine.endX = outQLine.startX + 2 * constants.inputLineLength;
    outQLine.endY = outQLine.startY;

    let outNQLine = {
        startX: connectDotNQ.centerX + connectDotNQ.radius,
        startY: connectDotNQ.centerY,
        color: constants.passiveStateColor
    };
    outNQLine.endX = outNQLine.startX + 2 * constants.inputLineLength;
    outNQLine.endY = outNQLine.startY;

    drawConnectLine(context, outQLine, constants.tubeLineWidth, true);
    if (q === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        outQLine.color = constants.currentColor;
        drawConnectLine(context, outQLine, constants.currentLineWidth);
    }
    passiveStyle(context);

    drawConnectLine(context, outNQLine, constants.tubeLineWidth);
    if (nQ === constants.greenLineActiveColor) {
        activeStyle(context, nand1.state);
        outNQLine.color = constants.currentColor;
        drawConnectLine(context, outNQLine, constants.currentLineWidth);
    }
    passiveStyle(context);
}


/**
 *
 * @param prevLevel
 * @param nextLevelState
 * @param nextLevelType
 * @param desiredSide
 * @param toggleIn
 * @param lineColorsIO
 * @param constants
 * @param distanceFromPrevLevel
 * @returns {[*,*]}
 */
function addNextLogicLevel(prevLevel, nextLevelState,
                           nextLevelType = "NAND", desiredSide = "L",
                           toggleIn = 1, lineColorsIO, constants,
                           distanceFromPrevLevel = 2, fillStyle = "white") {
    let inputLinesYOff = (constants.universalBoxHeight - constants.inputLineDistance) / 2;
    let complementList = (nextLevelType.charAt(0) === "N") ? [0, 0, 1] : [0, 0, 0];

    let side = (desiredSide === "L") ? -1 : 1;
    let complementOff = (complementList[2] === 1) ?
        constants.IEEEComplementDiameter : 0;
    let xOff = (distanceFromPrevLevel * constants.inputLineLength) + constants.universalBoxWidth
        + complementOff;
    let yOff = (desiredSide === "L") ?
        prevLevel[0].startY + inputLinesYOff -
        (constants.universalBoxHeight / 2) :
        prevLevel[0].startY + constants.universalBoxHeight / 2
        - inputLinesYOff;
    // Place the sign for level side, left is standard,
    // but it can be set to right as well.
    xOff = prevLevel[0].startX + side * xOff;

    // Draw Another NANDS
    let circuit1 = {
        startX: xOff,
        startY: yOff,
        boxWidth: constants.universalBoxWidth,
        boxHeight: constants.universalBoxHeight,
        color: constants.frameBlacked,
        fillStyle: fillStyle,
        textFont: constants.IECTextFont,
        complementList: complementList,
        lineColorsIO: lineColorsIO[0],
        state: nextLevelState
    };
    let down = (toggleIn === 0) ?
        constants.inputLineLength : inputLinesYOff + constants.inputLineDistance;
    let circuit2 = {
        startX: circuit1.startX,
        startY: prevLevel[1].startY + down -
        (constants.universalBoxHeight / 2),
        boxWidth: constants.universalBoxWidth,
        boxHeight: constants.universalBoxHeight,
        color: constants.frameBlacked,
        fillStyle: fillStyle,
        textFont: constants.IECFontSize + "px Courier",
        complementList: complementList,
        lineColorsIO: lineColorsIO[1],
        state: nextLevelState
    };
    return [circuit1, circuit2];
}

function drawUserBoxes(context, circuits, constants) {
    let inColor1 = (circuits[0].state.userInput[0] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let inColor2 = (circuits[0].state.userInput[1] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;

    let inputLinesYOff = getInputLinesYOff(constants);
    let clickBoxX = {
        leftVertexX: circuits[0].startX - constants.inputLineLength -
        constants.constBoxWidth,
        leftVertexY: circuits[0].startY + inputLinesYOff -
        (constants.constBoxHeight / 2) + constants.inputLineDistance,
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 0,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let clickBoxY = {
        leftVertexX: circuits[1].startX - constants.inputLineLength -
        constants.constBoxWidth,
        leftVertexY: circuits[1].startY + inputLinesYOff -
        (constants.constBoxHeight / 2),
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 1,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let input1 =
        {
            startX: circuits[0].startX - constants.inputLineLength -
            constants.clickBoxWidth,
            startY: circuits[0].startY + inputLinesYOff +
            constants.inputLineDistance,
            color: constants.passiveStateColor
        };
    input1.endX = input1.startX;//- 2 * constants.inputLineLength;
    input1.endY = input1.startY;

    let input2 =
        {
            startX: circuits[1].startX - constants.inputLineLength -
            constants.clickBoxWidth,
            startY: circuits[1].startY + inputLinesYOff,
            color: constants.passiveStateColor
        };
    input2.endX = input2.startX;//- 2 * constants.inputLineLength;
    input2.endY = input2.startY;


    drawConnectLine(context, input1, constants.tubeLineWidth, true);
    if (inColor1 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        input1.color = constants.currentColor;
        drawConnectLine(context, input1, constants.currentLineWidth);
    }
    passiveStyle(context);


    drawConnectLine(context, input2, constants.tubeLineWidth, true);
    if (inColor2 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        input2.color = constants.currentColor;
        drawConnectLine(context, input2, constants.currentLineWidth);
    }
    passiveStyle(context);

    let centerWithClickBox = 5;
    let textS = {
        leftVertexX: input1.endX - constants.textBoxWidth,
        leftVertexY: clickBoxX.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };

    let textR = {
        leftVertexX: input2.endX - constants.textBoxWidth,
        leftVertexY: clickBoxY.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };

    drawText(context, textS, "J");
    drawText(context, textR, "K");
    drawTextBox(context, clickBoxX,
        circuits[0].state.userInput[clickBoxX.clickId], 1, constants);
    drawTextBox(context, clickBoxY,
        circuits[0].state.userInput[clickBoxY.clickId], 1, constants);
}

function drawOutputs2(context, circuits, constants) {
    let underLinePosition = 1;
    let overLine = true;
    let shift = -constants.inputLineLength * 1.2;
    let shiftY = constants.constBoxHeight * 2.5;
    let shiftY2 = constants.constBoxHeight * -1;
    genericDrawOutText(context, circuits[0], "Q =", " " + circuits[0].state.Q, constants, 0, shift, false, shiftY);
    genericDrawOutText(context, circuits[1], "Q =", " " + circuits[0].state.nQ,
        constants, underLinePosition, shift, overLine, -shiftY2);
}

function drawOutputs(context, circuits, constants) {
    let underLinePosition = 1;
    let overLine = true;
    let shift = -2 * constants.inputLineLength;
    let shiftY = constants.constBoxHeight * 1.5;
    let shiftY2 = constants.constBoxHeight / 2;
    genericDrawOutText(context, circuits[0], "Q =", " " + circuits[0].state.Q, constants, 0, shift, false, shiftY);
    genericDrawOutText(context, circuits[1], "Q =", " " + circuits[0].state.nQ,
        constants, underLinePosition, shift, overLine, -shiftY2);
}


function drawCPConnections(context, circuits, constants) {
    let CP = (circuits[0].state.userInput[2] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;

    let inputLinesYOff = (constants.universalBoxHeight -
        constants.inputLineDistance) / 2;
    let halfOfCPLine = ((circuits[1].startY + inputLinesYOff) -
        (circuits[0].startY + constants.universalBoxHeight -
        inputLinesYOff)) / 2;

    let centerWithClickBox = 5;
    // Make two lines for nice current flow
    let inputCPUp = {
        startX: circuits[0].startX - constants.inputLineLength,
        startY: circuits[0].startY + constants.universalBoxHeight -
        inputLinesYOff + halfOfCPLine,
        color: constants.passiveStateColor
    };

    inputCPUp.endX = inputCPUp.startX;
    inputCPUp.endY = inputCPUp.startY - halfOfCPLine;
    let inputCPDown =
        {
            startX: circuits[0].startX - constants.inputLineLength,
            startY: inputCPUp.startY,
            color: constants.passiveStateColor
        };
    inputCPDown.endX = inputCPDown.startX;
    inputCPDown.endY = inputCPDown.startY + halfOfCPLine;

    passiveStyle(context);
    drawConnectLine(context, inputCPUp, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputCPUp.color = constants.currentColor;
        drawConnectLine(context, inputCPUp, constants.currentLineWidth, false, false);
    }
    passiveStyle(context);

    drawConnectLine(context, inputCPDown, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputCPDown.color = constants.currentColor;
        drawConnectLine(context, inputCPDown, constants.currentLineWidth);
    }
    passiveStyle(context);

    let inputCPOut = {
        startX: inputCPUp.startX,
        startY: inputCPUp.startY,
        color: constants.passiveStateColor
    };

    inputCPOut.endX = inputCPOut.startX - 2 * constants.inputLineLength -
        constants.universalBoxWidth;
    inputCPOut.endY = inputCPOut.startY;
    let clickBoxCP = {
        leftVertexX: inputCPOut.endX - constants.clickBoxWidth,
        leftVertexY: inputCPOut.endY - constants.clickBoxHeight / 2,
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 2,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let inputLineCP = {
        startX: clickBoxCP.leftVertexX,
        startY: clickBoxCP.leftVertexY + constants.clickBoxHeight / 2,
        color: constants.passiveStateColor
    };

    inputLineCP.endX = inputLineCP.startX;//- 2 * constants.inputLineLength;
    inputLineCP.endY = inputLineCP.startY;

    drawConnectLine(context, inputLineCP, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputLineCP.color = constants.currentColor;
        drawConnectLine(context, inputLineCP, constants.currentLineWidth);
    }
    passiveStyle(context);


    drawConnectLine(context, inputCPOut, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputCPOut.color = constants.currentColor;
        drawConnectLine(context, inputCPOut, constants.currentLineWidth);
    }
    passiveStyle(context);


    let letterDistance = constants.textBoxWidth / 2;
    let widthOff = constants.connectLinesWidth;
    let textCP = {
        leftVertexX: inputLineCP.endX - constants.clickBoxWidth -
        letterDistance - widthOff,
        leftVertexY: clickBoxCP.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };


    let connectDotCP =
        {
            centerX: inputCPUp.startX,
            centerY: inputCPUp.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

    drawCircle(context, connectDotCP);

    drawTextBox(context, clickBoxCP,
        circuits[0].state.userInput[clickBoxCP.clickId], 1, constants);
    drawText(context, textCP, "CP");

}

function drawTextAboveBox(context, box, text, constants) {
    let xOff = 15;
    let args = [];
    // let letters = text.length - 8;
    for (let i = 0, l = box.length; i < l; i++) {
        args[i] = {
            leftVertexX: box[i].startX - xOff,
            leftVertexY: box[i].startY - (constants.textBoxHeight / 2.5),
            boxWidth: constants.textBoxWidth * 3,
            boxHeight: constants.timeTextSize,
            textFont: constants.timeTextFont,
            fillStyle: constants.delayTextColor,
            lineWidth: constants.textLineWidth,
            color: constants.delayTextColor
        };
        drawText(context, args[i], text, true, false);
    }
}

function clearLineCanvas(context, line, width) {
    let x = (line.endX < line.startX) ? line.endX : line.startX;
    let y = (line.endY < line.startY) ? line.endY : line.startY;
    let w = Math.abs(line.endX - line.startX);
    let h = Math.abs(line.endY - line.startY);

    if (w === 0) {
        w = width;
        x -= (width / 2);
    } else if (h === 0) {
        h = width;
        y -= (width / 2);
    }
    /*context.beginPath();
     context.rect(x, y, w, h);
     context.stroke();*/
    context.clearRect(x, y, w, h);
}

function drawReturnLines2(context, ANDS, NANDS, constants, offFromMiddleVal = 50) {
    let offFromMiddle = offFromMiddleVal * constants.scaleFactor;
    let shiftAfterDot = 15;
    let shiftOffset = 15;
    let yLinesOff = getInputLinesYOff(constants);
    let lineQ = {
        startX: NANDS[0].startX + NANDS[0].boxWidth +
        constants.IEEEComplementDiameter + constants.inputLineLength +
        shiftAfterDot,
        startY: NANDS[0].startY + (NANDS[0].boxHeight / 2),
        endY: NANDS[1].startY + (NANDS[1].boxHeight / 2) + offFromMiddle,
        color: constants.passiveStateColor
    };
    lineQ.endX = lineQ.startX;


    let lineNQ = {
        startX: lineQ.startX + shiftOffset,
        startY: NANDS[1].startY + (NANDS[1].boxHeight / 2),

        color: constants.passiveStateColor
    };
    lineNQ.endX = lineNQ.startX;
    lineNQ.endY = NANDS[0].startY + (NANDS[0].boxHeight / 2) - offFromMiddle;

    passiveStyle(context);
    drawConnectLine(context, lineQ, constants.tubeLineWidth, true, false);
    drawConnectLine(context, lineNQ, constants.tubeLineWidth, true, false);
    if (NANDS[0].state.Q === 1) {
        activeStyle(context, NANDS[0].state);
        lineQ.color = constants.currentColor;
        drawConnectLine(context, lineQ, constants.currentLineWidth);
    }

    if (NANDS[0].state.nQ === 1) {
        activeStyle(context, NANDS[0].state);
        lineNQ.color = constants.currentColor;
        drawConnectLine(context, lineNQ, constants.currentLineWidth, false, false);
    }

    let lineQLeft = {
        startX: lineQ.endX,
        startY: lineQ.endY,
        endX: ANDS[0].startX - constants.inputLineLength,
        endY: lineQ.endY,
        color: constants.passiveStateColor
    };

    let lineNQLeft = {
        startX: lineNQ.endX,
        startY: lineNQ.endY,
        endX: ANDS[1].startX - constants.inputLineLength,
        endY: lineNQ.endY,
        color: constants.passiveStateColor
    };

    passiveStyle(context);
    drawConnectLine(context, lineQLeft, constants.tubeLineWidth, true);
    drawConnectLine(context, lineNQLeft, constants.tubeLineWidth, true);
    if (NANDS[0].state.Q === 1) {
        activeStyle(context, NANDS[0].state);
        lineQLeft.color = constants.currentColor;
        drawConnectLine(context, lineQLeft, constants.currentLineWidth, false, false);
    }
    if (NANDS[0].state.nQ === 1) {
        activeStyle(context, NANDS[0].state);
        lineNQLeft.color = constants.currentColor;
        drawConnectLine(context, lineNQLeft, constants.currentLineWidth, false, false);
    }

    let lineQDown = {
        startX: lineQLeft.endX,
        startY: lineQLeft.endY,
        endX: lineQLeft.endX,
        endY: ANDS[1].startY + ANDS[0].boxHeight - yLinesOff,
        color: constants.passiveStateColor
    };

    let lineNQDown = {
        startX: lineNQLeft.endX,
        startY: lineNQLeft.endY,
        endX: lineNQLeft.endX,
        endY: ANDS[0].startY + yLinesOff,
        color: constants.passiveStateColor
    };

    passiveStyle(context);
    drawConnectLine(context, lineQDown, constants.tubeLineWidth, true);
    drawConnectLine(context, lineNQDown, constants.tubeLineWidth, true);
    if (NANDS[0].state.Q === 1) {
        activeStyle(context, NANDS[0].state);
        lineQDown.color = constants.currentColor;
        drawConnectLine(context, lineQDown, constants.currentLineWidth, false, false);
    }
    if (NANDS[0].state.nQ === 1) {
        activeStyle(context, NANDS[0].state);
        lineNQDown.color = constants.currentColor;
        drawConnectLine(context, lineNQDown, constants.currentLineWidth, false, false);
    }

    let connectDotQ =
        {
            centerX: lineQ.startX,
            centerY: lineQ.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

    let connectDotNQ =
        {
            centerX: lineNQ.startX,
            centerY: lineNQ.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };
    drawCircle(context, connectDotQ);
    drawCircle(context, connectDotNQ);

}

function drawValues(context, args, text, constants) {
    let textOut = text;
    if (text != 1 && text != 0) {
        textOut = (text === constants.greenLineActiveColor) ? 1 : 0;
    }
    let xOff = 8 * constants.scaleFactor;
    let yOff = 9 * constants.scaleFactor;

    if (args.startX > args.endX) {
        xOff *= -1;
    } else {
        xOff = 3 * constants.scaleFactor;
    }
    let box = {
        leftVertexX: args.startX + xOff,
        leftVertexY: args.startY - yOff,
        boxWidth: constants.valuesBoxWidth,
        boxHeight: constants.valuesBoxHeight,
        color: constants.passiveStateColor,
        textFont: constants.fontSizeIOValues + constants.standardFont,
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    drawText(context, box, textOut, true);
}

function setGradientsForList(context, list) {
    for (let i = 0, l = list.length; i < l; i++) {
        setGradientGeneric(list[i],
            calcGradientCurrent(context, getGradientParams(list[i])));
    }
}


function drawCPConnectionsComplex(context, circuits, constants) {
    let CP = (circuits[0].state.userInput[2] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;

    let inputLinesYOff = (constants.universalBoxHeight -
        constants.inputLineDistance) / 2;

    let centerWithClickBox = 5;

    let cpOff = 30 * constants.scaleFactor;
    // Make two lines for nice current flow
    let inputCPUp = {
        startX: circuits[1].startX - 1.5 * constants.inputLineLength,
        startY: circuits[1].startY + constants.universalBoxHeight + cpOff,
        color: constants.passiveStateColor
    };

    inputCPUp.endX = inputCPUp.startX;
    inputCPUp.endY = circuits[0].startY + constants.universalBoxHeight - inputLinesYOff;

    let connectFirst =
        {
            startX: circuits[0].startX - 1.5 * constants.inputLineLength,
            startY: circuits[0].startY + constants.universalBoxHeight -
            inputLinesYOff,
            color: constants.passiveStateColor
        };
    connectFirst.endX = circuits[0].startX - constants.inputLineLength;
    connectFirst.endY = connectFirst.startY;

    let connectSecond =
        {
            startX: circuits[0].startX - 1.5 * constants.inputLineLength,
            startY: circuits[1].startY + inputLinesYOff,
            color: constants.passiveStateColor
        };
    connectSecond.endX = circuits[1].startX - constants.inputLineLength;
    connectSecond.endY = connectSecond.startY;

    passiveStyle(context);
    drawConnectLine(context, inputCPUp, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputCPUp.color = constants.currentColor;
        drawConnectLine(context, inputCPUp, constants.currentLineWidth, false, false);
    }
    passiveStyle(context);

    let inputCPOut = {
        startX: inputCPUp.startX,
        startY: inputCPUp.startY,
        color: constants.passiveStateColor
    };

    inputCPOut.endX = inputCPOut.startX - constants.inputLineLength;
    inputCPOut.endY = inputCPOut.startY;
    let clickBoxCP = {
        leftVertexX: inputCPOut.endX - constants.clickBoxWidth,
        leftVertexY: inputCPOut.endY - constants.clickBoxHeight / 2,
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 2,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let inputLineCP = {
        startX: clickBoxCP.leftVertexX,
        startY: clickBoxCP.leftVertexY + constants.clickBoxHeight / 2,
        color: constants.passiveStateColor
    };

    inputLineCP.endX = inputLineCP.startX;
    inputLineCP.endY = inputLineCP.startY;

    drawConnectLine(context, inputLineCP, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputLineCP.color = constants.currentColor;
        drawConnectLine(context, inputLineCP, constants.currentLineWidth);
    }
    passiveStyle(context);


    drawConnectLine(context, inputCPOut, constants.tubeLineWidth, true);
    if (CP === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        inputCPOut.color = constants.currentColor;
        drawConnectLine(context, inputCPOut, constants.currentLineWidth);
    }
    passiveStyle(context);


    let letterDistance = constants.textBoxWidth / 2;
    let widthOff = constants.connectLinesWidth;
    let textCP = {
        leftVertexX: inputLineCP.endX - constants.clickBoxWidth -
        letterDistance - widthOff,
        leftVertexY: clickBoxCP.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };


    let connectDotCP =
        {
            centerX: inputCPUp.startX,
            centerY: inputCPUp.startY,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

    let connectDotK =
        {
            centerX: circuits[1].startX - 1.5 * constants.inputLineLength,
            centerY: circuits[1].startY + inputLinesYOff,
            radius: constants.IEEEConnectDotRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            fillStyle: constants.passiveStateColor,
            lineWidth: constants.connectDotLineWidth
        };

    drawCircle(context, connectDotCP);
    drawCircle(context, connectDotK);

    drawTextBox(context, clickBoxCP,
        circuits[0].state.userInput[clickBoxCP.clickId], 1, constants);
    drawText(context, textCP, "CP");

}

function drawUserBoxesThree(context, circuits, constants) {
    let inColor1 = (circuits[0].state.userInput[0] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let inColor2 = (circuits[0].state.userInput[1] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;

    let connectToBox1 = {
        startX: circuits[0].startX,
        startY: circuits[0].startY + (circuits[0].boxHeight / 2),
    };
    connectToBox1.endY = connectToBox1.startY;
    connectToBox1.endX = connectToBox1.startX -
        (2 * constants.inputLineLength);

    let connectToBox2 = {
        startX: circuits[1].startX,
        startY: circuits[1].startY + (circuits[1].boxHeight / 2),
    };
    connectToBox2.endY = connectToBox2.startY;
    connectToBox2.endX = connectToBox2.startX -
        (2 * constants.inputLineLength);

    let value1 = (inColor1 === constants.greenLineActiveColor) ? 1 : 0;
    let value2 = (inColor2 === constants.greenLineActiveColor) ? 1 : 0;
    drawValues(context, connectToBox1, value1, constants);
    drawValues(context, connectToBox2, value2, constants);
    passiveStyle(context);
    drawConnectLine(context, connectToBox1, constants.tubeLineWidth, true);
    drawConnectLine(context, connectToBox2, constants.tubeLineWidth, true);
    if (inColor1 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        connectToBox1.color = constants.currentColor;
        drawConnectLine(context, connectToBox1, constants.currentLineWidth);
    }
    if (inColor2 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        connectToBox2.color = constants.currentColor;
        drawConnectLine(context, connectToBox2, constants.currentLineWidth);
    }
    passiveStyle(context);

    let clickBoxX = {
        leftVertexX: circuits[0].startX - (2 * constants.inputLineLength) -
        constants.constBoxWidth,
        leftVertexY: circuits[0].startY + (circuits[0].boxHeight / 2) -
        (constants.clickBoxHeight / 2),
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 0,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let clickBoxY = {
        leftVertexX: circuits[1].startX - (2 * constants.inputLineLength) -
        constants.constBoxWidth,
        leftVertexY: circuits[1].startY + (circuits[1].boxHeight / 2) -
        (constants.constBoxHeight / 2),
        boxWidth: constants.clickBoxWidth,
        boxHeight: constants.clickBoxHeight,
        state: circuits[0].state,
        clickId: 1,
        color: "black",
        fillStyle: constants.commonFillStyle,
        textFont: "20px Courier",
        lineWidth: constants.constBoxLineWidth
    };

    let input1 =
        {
            startX: circuits[0].startX - (constants.inputLineLength) -
            constants.clickBoxWidth,
            startY: circuits[0].startY + (circuits[0].boxHeight / 2),
            color: constants.passiveStateColor
        };
    input1.endX = input1.startX - constants.inputLineLength;
    input1.endY = input1.startY;

    let input2 =
        {
            startX: circuits[1].startX - (constants.inputLineLength) -
            constants.clickBoxWidth,
            startY: circuits[1].startY + (circuits[1].boxHeight / 2),
            color: constants.passiveStateColor
        };
    input2.endX = input2.startX - constants.inputLineLength;
    input2.endY = input2.startY;


    drawConnectLine(context, input1, constants.tubeLineWidth, true);
    if (inColor1 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        input1.color = constants.currentColor;
        drawConnectLine(context, input1, constants.currentLineWidth);
    }
    passiveStyle(context);


    drawConnectLine(context, input2, constants.tubeLineWidth, true);
    if (inColor2 === constants.greenLineActiveColor) {
        activeStyle(context, circuits[0].state);
        input2.color = constants.currentColor;
        drawConnectLine(context, input2, constants.currentLineWidth);
    }
    passiveStyle(context);

    let centerWithClickBox = 5;
    let textS = {
        leftVertexX: input1.endX - constants.textBoxWidth,
        leftVertexY: clickBoxX.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };

    let textR = {
        leftVertexX: input2.endX - constants.textBoxWidth,
        leftVertexY: clickBoxY.leftVertexY - centerWithClickBox,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        color: "black",
        fillStyle: "white",
        textFont: constants.varTextSize + "px Courier",
        lineWidth: constants.textLineWidth
    };

    drawText(context, textS, "J");
    drawText(context, textR, "K");
    drawTextBox(context, clickBoxX,
        circuits[0].state.userInput[clickBoxX.clickId], 1, constants);
    drawTextBox(context, clickBoxY,
        circuits[0].state.userInput[clickBoxY.clickId], 1, constants);

}

function createState(state, simulationCallback) {
    state.simName = "JK-bistabil ostvaren uporabom sklopova I i bistabila SR.\nKašnjenje logičkih sklopova: 1000 ms";
    state.J = 0;
    state.K = 0;
    state.S = 0;
    state.R = 0;
    state.CP = 1;
    state.Q = 1;
    state.nQ = 0;
    state.nS = 1;
    state.nR = 1;
    state.msDelay = 1000;
    state.userInput = [0, 0, 1];
    state.hots = [];
    state.animationOffset = 0;
    state.animationPattern = [16, 16];

    Object.defineProperties(state, {
        "getJ": {
            get: function () {
                return state.J
            }
        },
        "setJ": {
            set: function (newVal) {
                state.J = newVal;
                let setVal = ((state.nQ === 1) && (state.J === 1)) ? 1 : 0;
                setTimeout(simulationCallback(state, "S", setVal), state.msDelay);
            }
        },
        "getK": {
            get: function () {
                return state.K
            }
        },
        "setK": {
            set: function (newVal) {
                state.K = newVal;
                let setVal = ((state.Q === 1) && (state.K === 1)) ? 1 : 0;
                setTimeout(simulationCallback(state, "R", setVal), state.msDelay);
            }
        },
        "getS": {
            get: function () {
                return state.S
            }
        },
        "setS": {
            set: function (newVal) {

                state.S = newVal;
                let setVal = !(((state.S === 1) && (state.CP === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "NS", setVal), state.msDelay);

            }
        },
        "getNS": {
            get: function () {
                return state.nS
            }
        },
        "setNS": {
            set: function (newVal) {

                state.nS = newVal;
                let setVal = !(((state.nS === 1) && (state.nQ === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "Q", setVal), state.msDelay);

            }
        },
        "getQ": {
            get: function () {
                return state.Q
            }
        },
        "setQ": {
            set: function (newVal) {

                state.Q = newVal;
                let setValR = ((state.K === 1) && (state.Q === 1)) ? 1 : 0;
                let setValNQ = !(((state.nR === 1) && (state.Q === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "R", setValR), state.msDelay);
                setTimeout(simulationCallback(state, "NQ", setValNQ), state.msDelay);

            }
        },
        "getR": {
            get: function () {
                return state.R
            }
        },
        "setR": {
            set: function (newVal) {

                state.R = newVal;
                let setVal = !(((state.R === 1) && (state.CP === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "NR", setVal), state.msDelay);

            }
        },
        "getNR": {
            get: function () {
                return state.nR
            }
        },
        "setNR": {
            set: function (newVal) {

                state.nR = newVal;
                let setVal = !(((state.nR === 1) && (state.Q === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "NQ", setVal), state.msDelay);

            }
        },
        "getNQ": {
            get: function () {
                return state.nQ
            }
        },
        "setNQ": {
            set: function (newVal) {
                state.nQ = newVal;
                let setValS = ((state.nQ === 1) && (state.J === 1)) ? 1 : 0;
                let setValQ = !(((state.nS === 1) && (state.nQ === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "S", setValS), state.msDelay);
                setTimeout(simulationCallback(state, "Q", setValQ), state.msDelay);

            }
        },
        "getCP": {
            get: function () {
                return state.CP
            }
        },
        "setCP": {
            set: function (newVal) {
                state.CP = newVal;
                let setValNS = !(((state.S === 1) && (state.CP === 1))) ? 1 : 0;
                let setValNR = !(((state.R === 1) && (state.CP === 1))) ? 1 : 0;
                setTimeout(simulationCallback(state, "NS", setValNS), state.msDelay);
                setTimeout(simulationCallback(state, "NR", setValNR), state.msDelay);
            }
        }
    });
    return state;
}


function drawJK(canvas, context, clear = false, state, constants) {
    // Clear rect
    if (clear === true) {
        // Do nothnig
    }
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.save();

    state.hots.length = 0;
    // init current values, passive and active states.

    let q = (state.Q === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let nQ = (state.nQ === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let nS = (state.nS === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let nR = (state.nR === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let CP = (state.userInput[2] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let s = (state.S === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let r = (state.R === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let J = (state.userInput[0] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;
    let K = (state.userInput[1] === 1) ?
        constants.greenLineActiveColor : constants.passiveStateColor;


    let alignLeft = 80;
    let alignUp = -30;
    // Center the sketch on canvas
    let xOff = (canvas.width / 2) + constants.universalBoxWidth +
        2 * constants.inputLineLength + constants.IEEEComplementDiameter -
        alignLeft;
    let yOff = (canvas.height - 3 * constants.universalBoxHeight) / 2 + alignUp;

    // Draw flip flop
    let NAND1 = {
        startX: xOff,
        startY: yOff,
        boxWidth: constants.universalBoxWidth,
        boxHeight: constants.universalBoxHeight,
        color: constants.frameBlacked,
        fillStyle: constants.colors.andColor,
        textFont: constants.IECTextFont,
        complementList: [0, 0, 1],
        lineColorsIO: [nS, nQ, q],
        clickHotsIO: [false, false, false],
        state: state,
    };

    let NANDYOff = 90;
    let NAND2 = {
        startX: NAND1.startX,
        startY: NAND1.startY + constants.universalBoxHeight + NANDYOff,
        boxWidth: constants.universalBoxWidth,
        boxHeight: constants.universalBoxHeight,
        color: constants.frameBlacked,
        fillStyle: constants.colors.andColor,
        textFont: constants.IECTextFont,
        complementList: [0, 0, 1],
        lineColorsIO: [q, nR, nQ],
        clickHotsIO: [false, false, false],
        state: state
    };

    // Add returning lines
    drawReturnConnectors(context, NAND1, NAND2, constants);
    context.restore();
    // Draw second logic
    let nextState = state;
    let andState = state;
    let colorsIO = [[s, CP, nS], [CP, r, nR]];
    let colorsJK = [[nQ, J, s], [K, q, r]];
    let NANDS = addNextLogicLevel([NAND1, NAND2],
        nextState, "NAND", "L", 1, colorsIO, constants,
        2, constants.colors.andColor);
    let ANDS = addNextLogicLevel([NANDS[0], NANDS[1]],
        andState, "AND", "L", 1, colorsJK, constants,
        2, constants.colors.andColor);
    for (let i = 0, l = ANDS.length; i < l; i++) {
        ANDS[i].state = state;
        NANDS[i].state = state;
    }

    // drawReturnLines(context, ANDS, [NAND1, NAND2], constants);
    drawReturnLines2(context, ANDS, [NAND1, NAND2], constants);
    drawIOLines(context, NAND1, constants);
    drawIOLines(context, NAND2, constants);
    drawIOLines(context, NANDS[0], constants);
    drawIOLines(context, NANDS[1], constants);
    drawIOLines(context, ANDS[0], constants);
    drawIOLines(context, ANDS[1], constants);

    // Draw boxes

    //if (clear === true) {
    writeSimulationName(canvas, context, state.simName);
    setBackgroundFrame(context, canvas.width, canvas.height);
    // Add Q and Q' text
    drawOutputs(context, [NAND1, NAND2], constants);

    let logicGates = new LogicGates();
    drawRectangularBox(context, NAND1, logicGates.ANDSIGN, constants);
    drawRectangularBox(context, NAND2, logicGates.ANDSIGN, constants);

    drawRectangularBox(context, NANDS[0], logicGates.ANDSIGN, constants);
    drawRectangularBox(context, NANDS[1], logicGates.ANDSIGN, constants);
    drawRectangularBox(context, ANDS[0], logicGates.ANDSIGN, constants);
    drawRectangularBox(context, ANDS[1], logicGates.ANDSIGN, constants);
    // }
    drawUserBoxes(context, ANDS, constants);
    drawCPConnections(context, NANDS, constants);

    // Text S and R
    let textS = {
        leftVertexX: ANDS[0].startX + ANDS[0].boxWidth + 10,
        leftVertexY: ANDS[0].startY +
        ANDS[0].boxHeight / 2 - constants.textBoxHeight + 5,
        boxWidth: constants.textBoxWidth / 2,
        boxHeight: constants.textBoxHeight / 2,
        textFont: constants.flipFont,
        fillStyle: "rgba(0, 0, 200, 0)",
        color: constants.colors.black,
        lineWidth: constants.textLineWidth
    };

    let textR = {
        leftVertexX: ANDS[1].startX + ANDS[1].boxWidth + 10,
        leftVertexY: ANDS[1].startY +
        ANDS[1].boxHeight / 2 - constants.textBoxHeight + 10,
        boxWidth: constants.textBoxWidth / 2,
        boxHeight: constants.textBoxHeight / 2,
        textFont: constants.flipFont,
        fillStyle: constants.colors.white,
        color: constants.colors.black,
        lineWidth: constants.textLineWidth
    };
    drawText(context, textS, "S");
    drawText(context, textR, "R");

}


// Click hot event e, state, h
function clickHotEvent2(e, canvas, state, prop) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    // Check if click was in area which we consider
    for (let i = 0, l = state.hots.length; i < l; i++) {
        let h = state.hots[i];
        if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
            && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
            // We are inside click hot
            prop(e, state, h);
            break;
        }
    }
}

function clickHotEvent(e, canvas, state, prop) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    // Check if click was in area which we consider
    for (let i = 0, l = state.hots.length; i < l; i++) {
        let h = state.hots[i];
        if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
            && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {
            // We are inside click hot
            prop(state, h);
            break;
        }
    }
}

function propClickHotEvent(state, h) {
    let u = state.userInput[h.row];
    state.userInput[h.row] = (u === 0) ? 1 : 0;
    if (h.row === 0) {
        // J was pressed
        state.setJ = (state.J === 1) ? 0 : 1;
    } else if (h.row === 2) {
        // CP was pressed
        state.setCP = (state.CP === 1) ? 0 : 1;
    } else if (h.row === 1) {
        // K was pressed
        state.setK = (state.K === 1) ? 0 : 1;
    }
}

function update(canvas, context, clear = false, state, constants) {
    drawJK(canvas, context, clear, state, constants);
}


function drawDiagramGeneric002(context, state, x, y, cDistance, cRadius, transitions, constants) {
    let stayInZero;
    let stayInOne;
    let set, reset;

    if (state.Q === 0 && state.QnNext === 0) {
        stayInZero = constants.colors.red;
    } else {
        stayInZero = constants.colors.black;
    }
    if (state.Q === 0 && state.QnNext === 1) {
        set = constants.colors.red;
    } else {
        set = constants.colors.black;
    }
    if (state.Q === 1 && state.QnNext === 0) {
        reset = constants.colors.red;
    } else {
        reset = constants.colors.black;
    }
    if (state.Q === 1 && state.QnNext === 1) {
        stayInOne = constants.colors.red;
    } else {
        stayInOne = constants.colors.black;
    }

    let stateZero = (state.Q === 1)
        ? constants.passiveStateColor : constants.activeStateColor;
    let stateOne = (state.Q === 0)
        ? constants.passiveStateColor : constants.activeStateColor;


    let xOff = x;
    let yOff = y;
    let state0 = {
        centerX: xOff,
        centerY: yOff,
        radius: cRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        rotation: false,
        color: stateZero,
        fillStyle: "white",
        lineWidth: constants.connectLinesWidth + 2
    };

    let circleDistance = cDistance;
    let state1 = {
        centerX: xOff + circleDistance,
        centerY: yOff,
        radius: cRadius,
        startAngle: toRadians(0),
        endAngle: toRadians(360),
        rotation: false,
        color: stateOne,
        fillStyle: "white",
        lineWidth: constants.connectLinesWidth + 2,
    };
    drawCircle(context, state0);
    drawCircle(context, state1);

    // Draw Text for circle states
    let textS0 = {
        leftVertexX: (state0.centerX) - (constants.textBoxWidth / 2),
        leftVertexY: (state0.centerY) - (constants.textBoxHeight / 2),
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: (constants.varTextSize + 5) + "px Courier",
        color: constants.standardLineColor,
        lineWidth: constants.textLineWidth
    };
    let textS1 = {
        leftVertexX: (state1.centerX) - (constants.textBoxWidth / 2),
        leftVertexY: (state1.centerY) - (constants.textBoxHeight / 2),
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: (constants.varTextSize + 5) + "px Courier",
        color: constants.standardLineColor,
        lineWidth: constants.textLineWidth
    };

    drawText(context, textS0, "0");
    drawText(context, textS1, "1");

    // Draw connect lines
    let yLineOff = cRadius - Math.PI - 2;
    let distX = Math.sqrt(Math.pow(cRadius, 2) -
        Math.pow(yLineOff, 2));

    let lineSet = {
        startX: xOff + distX,
        startY: yOff - yLineOff,
        controlPointX: xOff + (circleDistance - 2 * distX) / 2,
        controlPointY: yOff - cRadius * 2,
        endPointX: xOff - distX + circleDistance - 5,
        endPointY: yOff - yLineOff,
        color: set,
        lineWidth: constants.connectLinesWidth
    };

    let lineReset = {
        startX: xOff + distX,
        startY: yOff + yLineOff,
        controlPointX: xOff + (circleDistance - 2 * distX) / 2,
        controlPointY: yOff + cRadius * 2,
        endPointX: xOff - distX + circleDistance,
        endPointY: yOff + yLineOff,
        color: reset,
        lineWidth: constants.connectLinesWidth
    };

    let yCenterOff = 45;
    let lineStayZero = {
        startX: xOff - distX,
        startY: yOff - yLineOff,
        controlPointX: xOff - 3 * cRadius,
        controlPointY: yOff - yCenterOff,
        endPointX: xOff - cRadius,
        endPointY: yOff,
        color: stayInZero,
        lineWidth: constants.connectLinesWidth
    };


    let lineStayOne = {
        startX: xOff + circleDistance + distX,
        startY: yOff - yLineOff,
        controlPointX: xOff + circleDistance + 3 * cRadius,
        controlPointY: yOff - yCenterOff,
        endPointX: xOff + circleDistance + cRadius,
        endPointY: yOff,
        color: stayInOne,
        lineWidth: constants.connectLinesWidth
    };

    let rows = transitions.lineSet.split("\n").length;
    let textYOff = rows * 24;
    let textForLineSet = {
        leftVertexX: lineSet.controlPointX,
        leftVertexY: lineSet.controlPointY - textYOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let textYDown = 5;
    let textForLineReset = {
        leftVertexX: lineReset.controlPointX,
        leftVertexY: lineReset.controlPointY - textYDown,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let smallInc = rows * 9 * constants.scaleFactor;
    let twoLineOff = rows * 10 * constants.scaleFactor;
    let textForLineStay0 = {
        leftVertexX: lineStayZero.controlPointX + smallInc,
        leftVertexY: lineStayZero.controlPointY - smallInc - twoLineOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    /** Constant is used for leftVertexX because of zooming makes change to it and it must be fixed (y is only moving axis). */
    let textForLineStay1 = {
        leftVertexX: state1.centerX + state1.radius,
        leftVertexY: lineStayOne.controlPointY - smallInc - twoLineOff,
        boxWidth: constants.textBoxWidth,
        boxHeight: constants.textBoxHeight,
        textFont: constants.varTextSize + "px Courier",
        color: "black",
        fillStyle: constants.passiveStateColor,
        lineWidth: constants.textLineWidth

    };

    let arrowWidth = 15;
    let lineHeight = 30;
    drawCurvedConnectLine(context, lineSet);
    drawCurvedConnectLine(context, lineReset);
    drawCurvedConnectLine(context, lineStayZero);
    drawCurvedConnectLine(context, lineStayOne);
    drawArrow(context, lineSet, arrowWidth, 1);
    drawArrow(context, lineReset, arrowWidth, 0);
    drawArrow(context, lineStayZero, arrowWidth, 1);
    drawArrow(context, lineStayOne, arrowWidth, 1);

    // Draw text on lines
    writeMultilineText2(context, textForLineSet,
        lineHeight, transitions.lineSet);
    writeMultilineText2(context, textForLineReset,
        lineHeight, transitions.lineReset);
    writeMultilineText2(context, textForLineStay0,
        lineHeight, transitions.lineStay0);
    writeMultilineText2(context, textForLineStay1,
        lineHeight, transitions.lineStay1);
}

function rightClickEvent(e) {
    e.preventDefault();
}