scopeFunctionDoubleJKDelay();

function scopeFunctionDoubleJKDelay() {
    let canvas = document.getElementById("doubleJKDelay");
    let context = canvas.getContext("2d");
    let firstJK = {
        simName: "Dvostruki JK-bistabil\nKašnjenje logičkih sklopova: 500 ms",
        J: 1,
        K: 1,
        S: 0,
        R: 1,
        CP: 0,
        Q: 1,
        nQ: 0,
        nS: 1,
        nR: 1,
        msDelay: 500,
        userInput: [1, 1, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };
    let stateSR = {
        S: 1,
        R: 0,
        nCP: 1,
        Q: 1,
        nQ: 0,
        nS: 0,
        nR: 1,
        msDelay: 500,
        animationOffset: firstJK.animationOffset,
        animationPattern: firstJK.animationPattern
    };

    let zoomHotsJK = {
        hots: [],
        min: 1.5,
        max: 1.7
    };

    let scaleFactor = 1.7;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);


    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                updateOther(canvas, context, true);
            }
        };
    }

    setStateProperties(firstJK, stateSR, simulationCallback);

    function setStateProperties(s0, s1, simulationCallback) {
        Object.defineProperties(s0, {
            "getJ": {
                get: function () {
                    return s0.J
                }
            },
            "setJ": {
                set: function (newVal) {
                    s0.J = newVal;
                    let setVal = !((s1.nQ === 1) && (s0.J === 1) && (s0.CP)) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NS", setVal), s0.msDelay);
                }
            },
            "getK": {
                get: function () {
                    return s0.K
                }
            },
            "setK": {
                set: function (newVal) {
                    s0.K = newVal;
                    let setVal = !((s1.Q === 1) && (s0.K === 1) && (s0.CP)) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NR", setVal), s0.msDelay);
                }
            },
            "getNS": {
                get: function () {
                    return s0.nS
                }
            },
            "setNS": {
                set: function (newVal) {
                    s0.nS = newVal;
                    let setValS = !((s1.nCP === 1) && (s0.Q === 1)) ? 1 : 0;
                    let setVal = !(((s0.nS === 1) && (s0.nQ === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "Q", setVal), s0.msDelay);
                    setTimeout(simulationCallback(s1, "S", setValS), s0.msDelay);
                }
            },
            "getQ": {
                get: function () {
                    return s0.Q
                }
            },
            "setQ": {
                set: function (newVal) {
                    s0.Q = newVal;
                    let setValNQ = !(((s0.nR === 1) && (s0.Q === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NQ", setValNQ), s0.msDelay);
                }
            },
            "getNR": {
                get: function () {
                    return s0.nR
                }
            },
            "setNR": {
                set: function (newVal) {
                    s0.nR = newVal;
                    let setValR = !((s0.nQ === 1) && (s1.nCP === 1)) ? 1 : 0;
                    let setVal = !(((s0.nR === 1) && (s0.Q === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NQ", setVal), s0.msDelay);
                    setTimeout(simulationCallback(s1, "R", setValR), s0.msDelay);
                }
            },
            "getNQ": {
                get: function () {
                    return s0.nQ
                }
            },
            "setNQ": {
                set: function (newVal) {
                    s0.nQ = newVal;
                    let setValQ = !(((s0.nS === 1) && (s0.nQ === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "Q", setValQ), s0.msDelay);
                }
            },
            "getCP": {
                get: function () {
                    return s0.CP
                }
            },
            "setCP": {
                set: function (newVal) {
                    s0.CP = newVal;
                    let setValNS = !((s1.nQ === 1) && (s0.J === 1)
                    && (s0.CP === 1)) ? 1 : 0;
                    let setValNR = !((s1.Q === 1) && (s0.K === 1)
                    && (s0.CP === 1)) ? 1 : 0;
                    let setValNCP = (s0.CP === 0) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NS", setValNS), s0.msDelay);
                    setTimeout(simulationCallback(s0, "NR", setValNR), s0.msDelay);
                    setTimeout(simulationCallback(s1, "NCP", setValNCP), s0.msDelay);
                }
            }
        });

        Object.defineProperties(s1, {
            "getS": {
                get: function () {
                    return s1.S
                }
            },
            "setS": {
                set: function (newVal) {
                    s1.S = newVal;
                    let setVal = !(((s1.S === 1) && (s1.nCP === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s1, "NS", setVal), s1.msDelay);

                }
            },
            "getR": {
                get: function () {
                    return s1.R
                }
            },
            "setR": {
                set: function (newVal) {
                    s1.R = newVal;
                    let setVal = !(((s1.R === 1) && (s1.nCP === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s1, "NR", setVal), s1.msDelay);
                }
            },
            "getNS": {
                get: function () {
                    return s1.nS
                }
            },
            "setNS": {
                set: function (newVal) {
                    s1.nS = newVal;
                    let setValQ = !(((s1.nS === 1) && (s1.nQ === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s1, "Q", setValQ), s1.msDelay);
                }
            },
            "getQ": {
                get: function () {
                    return s1.Q
                }
            },
            "setQ": {
                set: function (newVal) {
                    s1.Q = newVal;
                    let setValNR = !((s0.K === 1) &&
                    (s1.Q === 1) &&
                    (s0.CP === 1)) ? 1 : 0;
                    let setValNQ = !(((s1.nR === 1) && (s1.Q === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NR", setValNR), s1.msDelay);
                    setTimeout(simulationCallback(s1, "NQ", setValNQ), s1.msDelay);
                }
            },
            "getNR": {
                get: function () {
                    return s1.nR
                }
            },
            "setNR": {
                set: function (newVal) {
                    s1.nR = newVal;
                    let setValNQ = !(((s1.nR === 1) && (s1.Q === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s1, "NQ", setValNQ), s1.msDelay);
                }
            },
            "getNQ": {
                get: function () {
                    return s1.nQ
                }
            },
            "setNQ": {
                set: function (newVal) {
                    s1.nQ = newVal;
                    let setValNS = !((s0.J === 1) &&
                    (s1.nQ === 1) &&
                    (s0.CP === 1)) ? 1 : 0;
                    let setValQ = !(((s1.nS === 1) && (s1.nQ === 1))) ? 1 : 0;
                    setTimeout(simulationCallback(s0, "NS", setValNS), s1.msDelay);
                    setTimeout(simulationCallback(s1, "Q", setValQ), s1.msDelay);
                }
            },
            "getNCP": {
                get: function () {
                    return s1.nCP
                }
            },
            "setNCP": {
                set: function (newVal) {
                    s1.nCP = newVal;
                    let setValNS = !((s0.Q === 1) && (s1.nCP)) ? 1 : 0;
                    let setValNR = !((s0.nQ === 1) && (s1.nCP === 1)) ? 1 : 0;
                    setTimeout(simulationCallback(s1, "NS", setValNS), s1.msDelay);
                    setTimeout(simulationCallback(s1, "NR", setValNR), s1.msDelay);
                }
            }
        });

    }

    function clickEventHandler(canvas, context, state, prop) {
        return function (e) {
            clickHotEvent(e, canvas, state, prop);
            update(canvas, context, false, state, constJK);
        }
    }

    function update(canvas, context, clear = false, state, constants) {
        drawJK(canvas, context, clear, state, constants);
    }

    function updateOther(canvas, context, clear = false) {
        drawJK(canvas, context, clear, firstJK, constJK);
    }

    function processZoom(newScaleFactor, canvas, context) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        updateOther(canvas, context, true);
    }

    function startSimulationCallback(canvas, context, state) {
        return function () {
            startSimulation(canvas, context, state);
        }
    }

    function startSimulation(canvas, context, state) {
        state.animationOffset++;
        stateSR.animationOffset++;
        if (state.animationOffset > 2000) {
            state.animationOffset = 0;
        }
        if (stateSR.animationOffset > 2000) {
            stateSR.animationOffset = 0;
        }
        updateOther(canvas, context);
        setTimeout(startSimulationCallback(canvas, context, state), 40);
    }

    function wheelCollector(canvas, context) {
        return function (e) {
            MouseWheelEvent1(e, canvas, context);
        }
    }

    function MouseWheelEvent1(e, canvas, context) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas, context, state, prop) {
        canvas.addEventListener('mousedown',
            clickEventHandler(canvas, context, state, prop), false);
        canvas.addEventListener('mousewheel', wheelCollector(canvas, context), false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', wheelCollector(canvas, context), false);
    }

    function drawCPInvertor(context, circuits1, circuits2, constants) {
        let CP = (circuits1[0].state.CP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nCP = (circuits2[0].state.nCP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let yOff = 30 * constants.scaleFactor;
        let invLineIn = {
            startX: circuits1[1].startX - 1.5 * constants.inputLineLength,
            startY: circuits1[1].startY + circuits1[1].boxHeight + yOff
        };
        let yLinesOff = getInputLinesYOff(constants);

        invLineIn.endX = invLineIn.startX + 2 * constants.inputLineLength;
        invLineIn.endY = invLineIn.startY;

        let invCP = {
            startX: invLineIn.endX,
            h: 40,
            baseLength: 40,
            color: constants.passiveStateColor,
            lineWidth: constants.connectLinesWidth
        };
        invCP.startY = invLineIn.startY - invCP.baseLength / 2;

        let circleInv = {
            centerX: invCP.startX + invCP.h + constants.IEEEComplementRadius,
            centerY: invLineIn.startY,
            radius: constants.inverterComplementRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            lineWidth: constants.connectLinesWidth,
            fillStyle: "white"
        };

        passiveStyle(context);
        drawCircle(context, circleInv);
        drawTriangle(context, invCP);

        let invLineOut = {
            startX: invLineIn.endX + invCP.h + constants.IEEEComplementDiameter,
            startY: invLineIn.endY,
            endX: circuits2[0].startX - constants.inputLineLength,
            endY: invLineIn.endY
        };

        passiveStyle(context);
        drawConnectLine(context, invLineIn, constants.tubeLineWidth, true);
        drawConnectLine(context, invLineOut, constants.tubeLineWidth, true);
        if (CP === constants.greenLineActiveColor) {
            activeStyle(context, circuits1[0].state);
            invLineIn.color = constants.currentColor;
            drawConnectLine(context, invLineIn, constants.currentLineWidth);
        }
        if (nCP === constants.greenLineActiveColor) {
            activeStyle(context, circuits1[0].state);
            invLineOut.color = constants.currentColor;
            drawConnectLine(context, invLineOut, constants.currentLineWidth);
        }
        passiveStyle(context);

        // Make two lines for nice current flow
        let inputCPUp = {
            startX: invLineOut.endX,
            startY: invLineOut.endY,
            color: constants.passiveStateColor
        };

        inputCPUp.endX = inputCPUp.startX;
        inputCPUp.endY = circuits2[0].startY + constants.universalBoxHeight
            - yLinesOff;

        passiveStyle(context);
        drawConnectLine(context, inputCPUp, constants.tubeLineWidth, true);
        if (nCP === constants.greenLineActiveColor) {
            activeStyle(context, circuits1[0].state);
            inputCPUp.color = constants.currentColor;
            drawConnectLine(context, inputCPUp, constants.currentLineWidth,
                false, false);
        }
        passiveStyle(context);

        let connectDot =
            {
                centerX: circuits2[1].startX - constants.inputLineLength,
                centerY: circuits2[1].startY + yLinesOff,
                radius: constants.IEEEConnectDotRadius,
                startAngle: toRadians(constants.zeroAngle),
                endAngle: toRadians(constants.fullCircle),
                rotation: false,
                color: constants.passiveStateColor,
                fillStyle: constants.passiveStateColor,
                lineWidth: constants.connectDotLineWidth
            };
        drawCircle(context, connectDot);
    }

    function drawJK(canvas, context, clear = false, state, constants) {
        // Clear rect
        if (clear === true) {
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        context.save();

        firstJK.hots.length = 0;
        // init current values, passive and active states.

        let q = (stateSR.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (stateSR.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nS = (stateSR.nS === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nR = (stateSR.nR === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nCP = (stateSR.nCP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        let alignLeft = -50;
        // Center the sketch on canvas
        let xOff = (canvas.width / 2) + constants.universalBoxWidth +
            2 * constants.inputLineLength + constants.IEEEComplementDiameter -
            alignLeft;
        let yOff = (canvas.height - 3 * constants.universalBoxHeight) / 2;

        let yOffset = -30 * constants.scaleFactor;
        // Draw flip flop
        let NAND1 = {
            startX: xOff,
            startY: yOff + yOffset,
            boxWidth: constants.universalBoxWidth,
            boxHeight: constants.universalBoxHeight,
            color: constants.frameBlacked,
            fillStyle: constants.colors.andColor,
            textFont: constants.IECTextFont,
            complementList: [0, 0, 1],
            lineColorsIO: [nS, nQ, q],
            clickHotsIO: [false, false, false],
            state: stateSR,
        };

        let NANDYOff = 90;
        let NAND2 = {
            startX: NAND1.startX,
            startY: NAND1.startY + constants.universalBoxHeight + NANDYOff,
            boxWidth: constants.universalBoxWidth,
            boxHeight: constants.universalBoxHeight,
            color: constants.frameBlacked,
            fillStyle: constants.colors.andColor,
            textFont: constants.IECTextFont,
            complementList: [0, 0, 1],
            lineColorsIO: [q, nR, nQ],
            clickHotsIO: [false, false, false],
            state: stateSR
        };

        let firstQ = (state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let firstNQ = (state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let firstNS = (state.nS === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let firstNR = (state.nR === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        //let J = (state.userInput[0] === 1) ?
        //    constants.greenLineActiveColor : constants.passiveStateColor;
        // let K = (state.userInput[1] === 1) ?
        //     constants.greenLineActiveColor : constants.passiveStateColor;
        let CP = (state.CP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        /*let firstS = (state.S === 1) ?
         constants.greenLineActiveColor : constants.passiveStateColor;
         let firstR = (state.R === 1) ?
         constants.greenLineActiveColor : constants.passiveStateColor;*/

        // Add returning lines
        drawReturnConnectors(context, NAND1, NAND2, constants);
        context.restore();
        // Draw second logic
        let nextState = state;
        let nextSRState = stateSR;
        let colorsNSR2 = [[firstNS, firstNQ, firstQ], [firstQ, firstNR, firstNQ]];
        //let colorsSR2 = [[firstS, CP, firstNS], [CP, firstR, firstNR]];
        let colorsJK = [[nQ, CP, firstNS], [CP, q, firstNR]];
        let secondIO = [[firstQ, nCP, nS], [nCP, firstNQ, nR]];
        let NANDS = addNextLogicLevel([NAND1, NAND2],
            nextSRState, "NAND", "L", 1, secondIO, constants, 2, constants.colors.andColor);
        let firstJKNands = addNextLogicLevel(NANDS, nextState,
            "NAND", "L", 1, colorsNSR2, constants, 3.2, constants.colors.andColor);

        let actualDistance = 1.25 * 50;
        let retLineOff = 1.8 * 75;
        drawReturnConnectors(context, firstJKNands[0],
            firstJKNands[1], constants, actualDistance, retLineOff);

        let firstJKNands2 = addNextLogicLevel(firstJKNands, nextState,
            "NAND", "L", 1, colorsJK, constants, 2, constants.colors.andColor);

        let lineLength1 =
            [
                constants.inputLineLength,
                1.5 * constants.inputLineLength,
                constants.inputLineLength
            ];
        let lineLength2 =
            [
                1.5 * constants.inputLineLength,
                constants.inputLineLength,
                constants.inputLineLength
            ];

        drawIOLines(context, NAND1, constants);
        drawIOLines(context, NAND2, constants);
        drawIOLines(context, NANDS[0], constants);
        drawIOLines(context, NANDS[1], constants);
        drawIOLines(context, firstJKNands[0], constants);
        drawIOLines(context, firstJKNands[1], constants);
        drawIOLines(context, firstJKNands2[0], constants, lineLength1);
        drawIOLines(context, firstJKNands2[1], constants, lineLength2);

        // Draw boxes
        if (clear === true) {
            writeSimulationName(canvas, context, state.simName);
            setBackgroundFrame(context, canvas.width, canvas.height);
            // Add Q and Q' text
            drawOutputs2(context, [NAND1, NAND2], constants);
            let u = new usedUnicode;
            let underLinePosition = 1;
            let overLine = true;
            let shift = -constants.inputLineLength + 38 * constants.scaleFactor;
            let shiftX2 = -constants.inputLineLength + 40 * constants.scaleFactor;
            let shiftY = constants.constBoxHeight - 12 * constants.scaleFactor;
            let shiftY2 = constants.constBoxHeight * -1;
            genericDrawOutText(context, firstJKNands[0], "Q" + u.sub1 + " = ",
                firstJKNands[0].state.Q, constants, 0, shift, false, shiftY);
            genericDrawOutText(context, firstJKNands[1], "Q" + u.sub1 + " = ", firstJKNands[0].state.nQ,
                constants, underLinePosition, shiftX2, overLine, -shiftY2);

            let logicGates = new LogicGates();
            drawRectangularBox(context, NAND1, logicGates.ANDSIGN, constants);
            drawRectangularBox(context, NAND2, logicGates.ANDSIGN, constants);
            drawRectangularBox(context, NANDS[0], logicGates.ANDSIGN, constants);
            drawRectangularBox(context, NANDS[1], logicGates.ANDSIGN, constants);
            drawRectangularBox(context, firstJKNands[0],
                logicGates.ANDSIGN, constants);
            drawRectangularBox(context, firstJKNands[1],
                logicGates.ANDSIGN, constants);
            drawRectangularBox(context, firstJKNands2[0],
                logicGates.ANDSIGN, constants);
            drawRectangularBox(context, firstJKNands2[1],
                logicGates.ANDSIGN, constants);

        }
        drawCPConnectionsComplex(context, firstJKNands2, constants);
        drawUserBoxesThree(context, firstJKNands2, constants);
        drawReturnLines2(context, firstJKNands2, [NAND1, NAND2], constants, 60);
        drawCPInvertor(context, firstJKNands2, NANDS, constants);
    }

    function init(canvas, context, state, prop, constants) {
        let needClear = true;
        drawJK(canvas, context, needClear, state, constants);
        drawJK(canvas, context, needClear, state, constants);
        eventListeners(canvas, context, state, prop);

    }

    init(canvas, context, firstJK, propClickHotEvent, constJK);
    startSimulation(canvas, context, firstJK);
}
