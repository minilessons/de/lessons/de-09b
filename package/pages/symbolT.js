/**
 * Created by Komediruzecki on 13.12.2016..
 */


/**
 * Created by Komediruzecki on 7.12.2016..
 */

scopeFunctionSymbolT();

function scopeFunctionSymbolT() {

    let canvas = document.getElementById("symbolT");
    let context = canvas.getContext("2d");

    let stateT = {
        simName: "Bistabil T\nKašnjenje bistabila: 1000 ms",
        T: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        msDelay: 1000,
        userInput: [0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [20, 12]
    };

    let u = new usedUnicode();

    stateT.QnNext = (stateT.Q === 1) ? 0 : 1;
    stateT.currentRow = 1;

    stateT.elems = [
        ["T", "Q" + u.subN + u.subPlus + u.sub1],
        [0, "Q" + u.subN],
        [1, "Q" + u.subN + "'"]
    ];

    let zoomHotsJK = {
        hots: [],
        min: 1.5,
        max: 2
    };

    Object.defineProperties(stateT, {
        "setQ": {
            set: function (newVal) {
                stateT.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateT.nQ = newVal;
            }
        },
    });


    let scaleFactor = 1.75;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.T];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = state.Q;
        } else if (state.currentRow === 2) {
            state.QnNext = (state.Q === 1) ? 0 : 1;
        } else {
            // Not used
            state.QnNext = state.elems[state.currentRow][1];
        }
    }

    function startOscilation() {
        // set current
        setCurrentState(stateT);
        reInit(canvas, context);

        // Change state
        stateT.Q = (stateT.Q === 1) ? 0 : 1;
        stateT.nQ = (stateT.Q === 1) ? 0 : 1;

        // set next
        setTimeout(function () {
            setCurrentState(stateT);
            reInit(canvas, context);
        }, 3000);
    }


    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateT.hots.length; i < l; i++) {
            let h = stateT.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                let u = stateT.userInput[h.row];
                stateT.userInput[h.row] = (u === 0) ? 1 : 0;
                stateT.T = (h.row === 0) ? (stateT.T === 1) ? 0 : 1 : stateT.T;
                if (h.row === 1 && stateT.T === 1) {
                    stateT.CP = 1 - stateT.CP;
                    // CP changed, call the appropriate function
                    animateRaisingCP();

                } else {
                    setCurrentState(stateT);
                    reInit(canvas, context);
                }
                break;
            }
        }
    }

    function reInit(canvas, context) {
        drawT(canvas, context, stateT, constJK);
        drawDiagram(context, canvas, stateT, constJK);
        drawTable(context, stateT, constJK);
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);
    }

    function startSimulation() {
        stateT.animationOffset++;
        if (stateT.animationOffset > 2000) {
            stateT.animationOffset = 0;
        }
        setCurrentState(stateT);
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function eventListeners(canvas) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);
    }


    function drawT(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, state.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let T = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [T, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.flipT, clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 75 * constants.scaleFactor;
        let yOff = 85 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["T", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 3,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: state
        };

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);
    }

    function drawDiagram(context, canvas, state, constants) {
        let yOff = 10;
        let y = canvas.height - (canvas.height - 200) / 2 + yOff;
        let distanceFromCircles = 150 * constants.scaleFactor;
        let sketchLength = distanceFromCircles + constants.IEEEComplementDiameter;
        let x = (canvas.width - sketchLength) / 2;
        let circleRadius = constants.IEEEArcRadius;


        let transitions = {
            lineSet: "1",
            lineReset: "1",
            lineStay0: "0",
            lineStay1: "0"
        };
        drawDiagramGeneric002(context, state,
            x, y, distanceFromCircles, circleRadius, transitions, constants)
    }

    function drawTable(context, state, constants) {
        let x = 250 * 2;
        let y = 15 * 2;
        let rowHeight = 23 * 2;
        let columnWidth = 28 * 2;
        let fontSize = rowHeight / 2;
        drawTableGeneric2(context, x, y, rowHeight, columnWidth, fontSize, state, constants)
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                reInit(canvas, context);
            }
        };
    }


    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        if (stateT.CP === 1) {
            stateT.CPEdge = 1;
        } else {
            stateT.CPEdge = 0;
        }
        if (stateT.CPEdge === 1) {
            let newQ = (stateT.T === 1) ? (stateT.Q === 1) ? 0 : 1 : stateT.Q;
            let newNQ = (newQ === 1) ? 0 : 1;
            setTimeout(simulationCallback(stateT, "Q", newQ), stateT.msDelay);
            setTimeout(simulationCallback(stateT, "NQ", newNQ), stateT.msDelay);
        }
        stateT.CPEdge = 0;
    }


    function init() {
        drawT(canvas, context, stateT, constJK);
        drawDiagram(context, canvas, stateT, constJK);
        drawTable(context, stateT, constJK);
    }

    init();
    eventListeners(canvas);
    startSimulation();
}
