scopeFunctionExT();

function scopeFunctionExT() {

    let canvas = document.getElementById("flipFlopT");
    let context = canvas.getContext("2d");

    let stateT = {
        simName: "Bistabil T\nKašnjenje bistabila: 1000 ms",
        T: 0,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        msDelay: 1000,
        userInput: [0, 0, 0],
        hots: [],
        animationOffset: 0,
        animationPattern: [18, 10],
        impl: 0
    };

    let stateJK = {
        simName: "Implementacija bistabila T\nKašnjenje bistabila: 1000 ms",
        J: 1,
        K: 1,
        CP: 0,
        CPEdge: 0,
        Q: 1,
        nQ: 0,
        animationOffset: 0,
        animationPattern: [18, 10],
    };

    Object.defineProperties(stateT, {
        "setQ": {
            set: function (newVal) {
                stateT.Q = newVal;
            }
        },
        "setNQ": {
            set: function (newVal) {
                stateT.nQ = newVal;
            }
        },
    });


    let u = new usedUnicode();

    stateT.QnNext = (stateT.Q === 1) ? 0 : 1;
    stateT.currentRow = 1;

    stateT.elems = [
        ["T", "Q" + u.subN + u.subPlus + u.sub1],
        [0, "Q"],
        [1, "Q'"]
    ];

    let zoomHotsJK = {
        hots: [],
        min: 1.9,
        max: 2.5
    };

    let scaleFactor = 1.9;
    let constJK = new Constants(scaleFactor);
    saveClickHots(zoomHotsJK, 0, 0, canvas.width, canvas.height);

    function setCurrentState(state) {
        // One more skips the HEADER row in table, which is at row 0.
        let currentState = [state.T];
        state.currentRow = binaryToDecimal(currentState) + 1;
        if (state.currentRow === 1) {
            state.QnNext = (state.Q === 1) ? 0 : 1;
        } else if (state.currentRow === 2) {
            state.QnNext = (state.Q === 1) ? 0 : 1;
        } else {
            state.QnNext = state.elems[state.currentRow][1];
        }
    }

    function clickHotEventCustom(e) {
        let rect = canvas.getBoundingClientRect();
        let pos = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top
        };

        // Check if click was in area which we consider
        for (let i = 0, l = stateT.hots.length; i < l; i++) {
            let h = stateT.hots[i];
            if ((pos.x >= h.x) && (pos.x < (h.x + h.w))
                && (pos.y >= h.y) && (pos.y < (h.y + h.h))) {

                // We are inside click hot
                let u = stateT.userInput[h.row];
                stateT.userInput[h.row] = (u === 0) ? 1 : 0;
                stateT.T = (h.row === 0) ? (stateT.T === 1) ? 0 : 1 : stateT.T;

                switch (h.row) {
                    case 0:
                        setCurrentState(stateT);
                        reInit(canvas, context);
                        break;
                    case 1:
                        // CP changed, call the appropriate function
                        stateT.CP = 1 - stateT.CP;
                        animateRaisingCP();
                        break;
                    case 2:
                        // Right Click, see context event function
                        break;
                    default:
                        alert("Impossible click!");
                }
                break;
            }
        }
    }

    function setScaleFactor(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
    }

    function reInit(canvas, context) {
        if (stateT.impl === 0) {
            drawT(canvas, context, stateT, constJK);
        } else {
            drawInnerT(canvas, context, stateT, constJK);
        }
    }

    function processZoom(newScaleFactor) {
        scaleFactor = newScaleFactor;
        constJK = new Constants(scaleFactor);
        reInit(canvas, context);
    }

    function startSimulation() {
        stateT.animationOffset++;
        stateJK.animationOffset++;
        if (stateT.animationOffset > 2000) {
            stateT.animationOffset = 0;

        }
        if (stateJK.animationOffset > 2000) {
            stateJK.animationOffset = 0;
        }
        setCurrentState(stateT);
        reInit(canvas, context);
        setTimeout(startSimulation, 40);
    }

    function MouseWheelEvent1(e) {
        e.preventDefault();
        mouseZoomer(e, canvas, context, zoomHotsJK, scaleFactor, processZoom);
    }

    function rightClickEvent(canvas, state) {
        return function (e) {
            clickHotEvent2(e, canvas, state, propRightClick);
        }
    }

    function propRightClick(e, state, h) {
        if (h.row === 2) {
            state.impl = (state.impl === 1) ? 0 : 1;
            setScaleFactor(scaleFactor);
            e.preventDefault();
        }
    }

    function eventListeners(canvas, state) {
        canvas.addEventListener('mousedown', clickHotEventCustom, false);
        canvas.addEventListener('contextmenu',
            rightClickEvent(canvas, state), false);
        canvas.addEventListener('mousewheel', MouseWheelEvent1, false);

        // Firefox Support
        canvas.addEventListener('DOMMouseScroll', MouseWheelEvent1, false);
    }


    function drawT(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateT.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);
        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let T = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [T, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.flipT, clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 75 * scaleFactor;
        let boxHeight = 100 * scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["T", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [1, 1],
            textPosOut: [1, 1],
            flipFlopType: 3,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: colorsBox.box,
            state: state
        };

        // Save clickHots
        saveClickHots(stateT, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        passiveStyle(context);
    }

    /**
     * Returns a callback function which changes item to specified value
     * @param state state to which setter is applied
     * @param item item to change
     * @param value value to apply to given item
     * @returns {Function} function which will apply value to specified item and
     * reinitialise the current state on canvas
     */
    function simulationCallback(state, item, value) {
        return function () {
            let get = "get";
            let set = "set";
            let oldVal = state[get + item];
            if (oldVal != value) {
                state[set + item] = value;
                reInit(canvas, context);
            }
        };
    }


    /**
     * Animates flip flop which has raising edge behaviour.
     */
    function animateRaisingCP() {
        if (stateT.CP === 1) {
            stateT.CPEdge = 1;
        } else {
            stateT.CPEdge = 0;
        }
        if (stateT.CPEdge === 1) {
            let newQ = (stateT.T === 1) ? (stateT.Q === 1) ? 0 : 1 : stateT.Q;
            let newNQ = (newQ === 1) ? 0 : 1;
            setTimeout(simulationCallback(stateT, "Q", newQ), stateT.msDelay);
            setTimeout(simulationCallback(stateT, "NQ", newNQ), stateT.msDelay);
        }
        stateT.CPEdge = 0;
    }


    function drawInnerT(canvas, context, state, constants) {
        // Clear rect
        context.clearRect(0, 0, canvas.width, canvas.height);
        writeSimulationName(canvas, context, stateJK.simName);
        setBackgroundFrame(context, canvas.width, canvas.height);

        context.save();

        state.hots.length = 0;
        // init current values, passive and active states.

        let scaleFactorJK = constants.scaleFactor - 0.5;
        let constJK = new Constants(scaleFactorJK);
        let q = (state.Q === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let nQ = (state.nQ === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let CP = (state.userInput[1] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;
        let J = (state.userInput[0] === 1) ?
            constJK.greenLineActiveColor : constJK.passiveStateColor;

        let colorsIn = [J, CP];
        let colorsOut = [q, nQ];
        let colorsBox = {box: "#CB3A34", clickBox: "#746dc5"};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 2 * 75 * constants.scaleFactor;
        let boxHeight = 120 * constants.scaleFactor;
        let xOff = 1 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: (canvas.width - boxWidth) / 2 - xOff,
            startY: (canvas.height - boxHeight) / 2 - yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["T", "CP"],
            outText: ["Q", "Q'"],
            textPosIn: [0, 0],
            textPosOut: [0, 0],
            flipFlopType: 1,
            addClickBox: [true, true],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: colorsBox.clickBox,
            fillStyle: constants.colors.white,
            state: state
        };

        // Save clickHots
        saveClickHots(stateT, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);
        drawInnerJK(context, genericBox, stateJK, constJK);
        //drawOuterTextBoxes(context, genericBox, constants);
        passiveStyle(context);
    }

    function getFlipFlopYLinesOff(height, numberOfIn) {
        let h = height;
        let lineDistanceIn = (h / numberOfIn);
        return ((h - (numberOfIn - 1) * lineDistanceIn) / 2);
    }


    function drawInnerJK(context, outerBox, stateJK, constants) {
        // Clear rect
        context.save();

        // init current values, passive and active states.
        let J = (outerBox.state.T === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let K = (outerBox.state.T === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let q = (outerBox.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (outerBox.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        // Set inner CP
        stateJK.CP = stateT.CP;

        let colorsIn = [J, K];
        let colorsOut = [q, nQ];
        let colorsBox = {box: constants.colors.emerald};

        // Description: boxXOffset, boxYOffset, boxWidth, boxHeight, lineWidth,
        // lineLength, inputLineDistance, clickBoxWidth, clickBoxHeight, colors...
        let boxWidth = 70 * constants.scaleFactor;
        let boxHeight = 90 * constants.scaleFactor;
        let xOff = 30 * constants.scaleFactor;
        let yOff = 1 * constants.scaleFactor;
        let genericBox = {
            startX: outerBox.startX + (outerBox.boxWidth - boxWidth) / 2 + xOff,
            startY: outerBox.startY + (outerBox.boxHeight - boxHeight) / 2 + yOff,
            inputLineLength: constants.inputLineLength,
            boxWidth: boxWidth,
            boxHeight: boxHeight,
            clickBoxWidth: constants.clickBoxWidth,
            clickBoxHeight: constants.clickBoxHeight,
            complementListIn: [0, 0, 0],
            complementListOut: [0, 0],
            clickIds: [0, 1],
            textIn: ["J", "K"],
            outText: ["Q", "Q'"],
            flipFlopType: 3,
            addClickBox: [false, false],
            boxLineWidth: constants.boxLineWidth,
            lineColorsIn: colorsIn,
            lineColorsOut: colorsOut,
            clickBoxColor: constants.colors.white,
            fillStyle: colorsBox.box,
            state: stateJK
        };

        // Save clickHots
        saveClickHots(stateT, genericBox.startX, genericBox.startY,
            genericBox.boxWidth, genericBox.boxHeight, 2);

        // Draw flip flop. Draw IO lines
        passiveStyle(context);
        drawFlipFlopBox(context, genericBox, constants);
        drawLinesNew(context, genericBox, colorsIn.length, colorsOut.length, constants);

        drawCPBelow(context, genericBox, constants, 3);
        drawInnerLines(context, outerBox, genericBox, constants);
        passiveStyle(context);
    }

    function drawInnerLines(context, box, innerBox, constants) {
        // Active and passive colours
        let J = (box.state.T === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let K = (box.state.T === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let CP = (box.state.CP === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let Q = (box.state.Q === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;
        let nQ = (box.state.nQ === 1) ?
            constants.greenLineActiveColor : constants.passiveStateColor;

        let numberOfIn = 2;
        let h = box.boxHeight;
        let hInner = innerBox.boxHeight;
        let lineDistanceIn = (h / numberOfIn);
        let yLinesOff = (h - (numberOfIn - 1) * lineDistanceIn) / 2;

        let lineDistanceInInner = (hInner / numberOfIn);
        let yLinesOffInner = (hInner - (numberOfIn - 1) * lineDistanceInInner) / 2;
        let xIn = 20 * constants.scaleFactor;
        let yDown = (innerBox.startY + yLinesOffInner) - (box.startY + yLinesOff);

        // Line from outer to inner
        // Line to S
        let lineOutToInS = {
            startX: box.startX,
            startY: box.startY + yLinesOff,
            color: constants.passiveStateColor
        };
        lineOutToInS.endX = lineOutToInS.startX + xIn;
        lineOutToInS.endY = lineOutToInS.startY;
        let lineToS = {
            startX: lineOutToInS.endX,
            startY: lineOutToInS.endY,
            color: constants.passiveStateColor
        };
        lineToS.endX = lineToS.startX;
        lineToS.endY = lineToS.startY + yDown;

        let lineToSLeft = {
            startX: lineToS.endX,
            startY: lineToS.endY,
            color: constants.passiveStateColor
        };
        lineToSLeft.endX = innerBox.startX;
        lineToSLeft.endY = lineToSLeft.startY;


        passiveStyle(context);
        drawConnectLine(context, lineOutToInS, constants.tubeLineWidth);
        drawConnectLine(context, lineToS, constants.tubeLineWidth);
        drawConnectLine(context, lineToSLeft, constants.tubeLineWidth);
        if (J === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineOutToInS.color = constants.greenLineActiveColor;
            lineToS.color = constants.greenLineActiveColor;
            lineToSLeft.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineOutToInS, constants.currentLineWidth);
            drawConnectLine(context, lineToS, constants.currentLineWidth);
            drawConnectLine(context, lineToSLeft, constants.currentLineWidth);
        }
        passiveStyle(context);

        // Line from S to inv
        // Line to R

        let lineIn = {
            startX: lineToS.endX,
            startY: lineToS.endY,
            color: constants.passiveStateColor
        };
        lineIn.endX = lineIn.startX;
        lineIn.endY = innerBox.startY + innerBox.boxHeight - yLinesOffInner;

        let xInvOff = 20 * constants.scaleFactor;
        let lineInLeft = {
            startX: lineIn.endX,
            startY: lineIn.endY,
            color: constants.passiveStateColor
        };
        lineInLeft.endX = lineInLeft.startX + xInvOff;
        lineInLeft.endY = lineInLeft.startY;

        passiveStyle(context);
        drawConnectLine(context, lineIn, constants.tubeLineWidth);
        drawConnectLine(context, lineInLeft, constants.tubeLineWidth);
        if (J === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineIn.color = constants.greenLineActiveColor;
            lineInLeft.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineIn, constants.currentLineWidth);
            drawConnectLine(context, lineInLeft, constants.currentLineWidth);
        }
        passiveStyle(context);

        let circleInv = {
            centerX: lineInLeft.startX,
            centerY: lineInLeft.startY,
            radius: constants.inverterComplementRadius,
            startAngle: toRadians(constants.zeroAngle),
            endAngle: toRadians(constants.fullCircle),
            rotation: false,
            color: constants.passiveStateColor,
            lineWidth: constants.connectLinesWidth,
            fillStyle: "white"
        };

        passiveStyle(context);
        let lineOut = {
            startX: circleInv.centerX + constants.IEEEComplementRadius,
            startY: circleInv.centerY,
            color: constants.passiveStateColor
        };

        lineOut.endX = innerBox.startX;
        lineOut.endY = lineOut.startY;

        passiveStyle(context);
        drawConnectLine(context, lineOut, constants.tubeLineWidth);
        if (K === constants.greenLineActiveColor) {
            activeStyle(context, box.state);
            lineOut.color = constants.greenLineActiveColor;
            drawConnectLine(context, lineOut, constants.currentLineWidth);
        }
        passiveStyle(context);

        let connectDot =
            {
                centerX: lineToS.endX,
                centerY: lineToS.endY,
                radius: constants.IEEEConnectDotRadius,
                startAngle: toRadians(constants.zeroAngle),
                endAngle: toRadians(constants.fullCircle),
                rotation: false,
                color: constants.passiveStateColor,
                fillStyle: constants.passiveStateColor,
                lineWidth: constants.connectDotLineWidth
            };
        drawCircle(context, connectDot);

        let lineCPIn = getLineParams(
            box.startX,
            box.startY + box.boxHeight - yLinesOff,
            box.startX + xIn,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let downCP = 25 * constants.scaleFactor;
        let lineCPDown = getLineParams(
            lineCPIn.endX,
            lineCPIn.endY,
            lineCPIn.endX,
            lineCPIn.endY + downCP,
            constants.passiveStateColor
        );

        let rightCP = getLineParams(
            lineCPDown.endX,
            lineCPDown.endY,
            innerBox.startX + (innerBox.boxWidth / 2),
            lineCPDown.endY,
            constants.passiveStateColor
        );

        let lineCPUp = getLineParams(
            rightCP.endX,
            rightCP.endY,
            rightCP.endX,
            innerBox.startY + innerBox.boxHeight,
            constants.passiveStateColor
        );
        drawComplexLine(context, lineCPIn, CP, box.state, constants);
        drawComplexLine(context, lineCPDown, CP, box.state, constants);
        drawComplexLine(context, rightCP, CP, box.state, constants);
        drawComplexLine(context, lineCPUp, CP, box.state, constants, false, false);


        let lineQOut = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            box.startY + yLinesOff,
            constants.passiveStateColor
        );
        let lineNQOut = getLineParams(
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            innerBox.startY + innerBox.boxHeight - yLinesOffInner,
            innerBox.startX + innerBox.boxWidth + constants.inputLineLengthNew,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let lineNQRight = getLineParams(
            lineNQOut.endX,
            lineNQOut.endY,
            box.startX + box.boxWidth,
            box.startY + box.boxHeight - yLinesOff,
            constants.passiveStateColor
        );

        let lineQRight = getLineParams(
            lineQOut.endX,
            lineQOut.endY,
            box.startX + box.boxWidth,
            box.startY + yLinesOff,
            constants.passiveStateColor
        );
        drawComplexLine(context, lineQOut, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQOut, nQ, box.state, constants, false, false);
        drawComplexLine(context, lineQRight, Q, box.state, constants, false, false);
        drawComplexLine(context, lineNQRight, nQ, box.state, constants, false, false);
    }

    function drawComplexLine(context, line, cond, state,
                             constants, clearCanvas = false, direction = true) {
        passiveStyle(context);
        drawConnectLine(context, line,
            constants.tubeLineWidth, clearCanvas, direction);
        if (cond === constants.greenLineActiveColor) {
            activeStyle(context, state);
            line.color = constants.greenLineActiveColor;
            drawConnectLine(context, line,
                constants.currentLineWidth, clearCanvas, direction);
        }
        passiveStyle(context);
    }

    function getLineParams(x1, y1, x2, y2, c) {
        return {
            startX: x1,
            startY: y1,
            endX: x2,
            endY: y2,
            color: c
        };
    }

    function init() {
        if (stateT.impl === 0) {
            drawT(canvas, context, stateT, constJK);
        } else {
            drawInnerT(canvas, context, stateT, constJK);
        }
    }

    init();
    eventListeners(canvas, stateT);
    startSimulation();
}
