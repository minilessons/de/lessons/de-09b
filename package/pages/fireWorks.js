let t = 0;
let c = document.getElementById("fireWorks");
let context = c.getContext('2d');
/*canvas.width = canvas.width;
 canvas.height = canvas.height;*/
context.fillStyle = 'hsla(0,0%,0%,1)';
/*

 window.addEventListener('resize', function () {
 c.width = c.width;
 c.height = c.height;
 }, false);
 */

function draw() {
    context.globalCompositeOperation = 'source-over';
    context.fillStyle = 'hsla(0,0%,0%,.1)';
    context.fillRect(0, 0, c.width, c.height);
    let foo, i, j, r;
    foo = Math.sin(t) * 2 * Math.PI;
    for (i = 0; i < 400; ++i) {
        r = 400 * Math.sin(i * foo);
        context.globalCompositeOperation = '';
        context.fillStyle = 'hsla(' + i + 12 + ',100%, 60%,1)';
        context.beginPath();
        context.arc(Math.sin(i) * r + (c.width / 2),
            Math.cos(i) * r + (c.height / 2),
            1.5, 0, Math.PI * 2);
        context.fill();

    }
    t += 0.000005;
    return t %= 2 * Math.PI;

}

function run() {
    window.requestAnimationFrame(run);
    draw();
}
run();

